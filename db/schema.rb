# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180913160100) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "payment_commisions", force: :cascade do |t|
    t.string   "code"
    t.integer  "admin_id"
    t.integer  "user_id"
    t.date     "payment_at"
    t.date     "payment_end"
    t.float    "price",         default: 0.0
    t.float    "fee_publisher", default: 0.0
    t.float    "fee_am",        default: 0.0
    t.float    "total",         default: 0.0
    t.string   "from_acc"
    t.string   "to_acc"
    t.string   "message"
    t.string   "status",        default: "pending"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "payment_snapshots", force: :cascade do |t|
    t.string   "code"
    t.integer  "admin_id"
    t.integer  "user_id"
    t.float    "price",         default: 0.0
    t.float    "fee_am",        default: 0.0
    t.float    "fee_company",   default: 0.0
    t.float    "total",         default: 0.0
    t.string   "message"
    t.string   "status",        default: "pending"
    t.date     "payment_at"
    t.date     "payment_end"
    t.string   "category"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "from_acc"
    t.string   "to_acc"
    t.float    "fee_publisher", default: 0.0
    t.index ["admin_id"], name: "index_payment_snapshots_on_admin_id", using: :btree
    t.index ["user_id"], name: "index_payment_snapshots_on_user_id", using: :btree
  end

  create_table "profile_account_managers", force: :cascade do |t|
    t.integer  "account_manager_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "country"
    t.string   "province"
    t.string   "city"
    t.string   "address"
    t.string   "choice_payment_type"
    t.string   "acc_paypal"
    t.string   "acc_payoneer"
    t.string   "acc_bank"
    t.string   "longitude"
    t.string   "latitude"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "zip_code"
    t.string   "bank_name"
    t.string   "acc_name"
    t.index ["account_manager_id"], name: "index_profile_account_managers_on_account_manager_id", using: :btree
  end

  create_table "profiles", force: :cascade do |t|
    t.integer  "publisher_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "country"
    t.string   "province"
    t.string   "city"
    t.string   "address"
    t.string   "acc_paypal"
    t.string   "acc_payoneer"
    t.string   "acc_bank"
    t.string   "longitude"
    t.string   "latitude"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "choice_payment_type"
    t.string   "zip_code"
    t.string   "bank_name"
    t.string   "acc_name"
    t.index ["publisher_id"], name: "index_profiles_on_publisher_id", using: :btree
  end

  create_table "regions", force: :cascade do |t|
    t.string   "name"
    t.string   "ancestry"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                            default: "",    null: false
    t.string   "encrypted_password",               default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                    default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",                  default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "username"
    t.string   "provider"
    t.string   "uid"
    t.string   "slug"
    t.boolean  "verified",                         default: false
    t.string   "type"
    t.string   "facebook_uid"
    t.string   "facebook_oauth_token"
    t.string   "facebook_oauth_token_expires"
    t.datetime "facebook_oauth_token_register_at"
    t.integer  "facebook_friends_count"
    t.string   "facebook_avatar"
    t.string   "google_uid"
    t.string   "google_oauth_token"
    t.string   "google_oauth_token_expires"
    t.datetime "google_oauth_token_register_at"
    t.integer  "google_followers_count"
    t.string   "google_avatar"
    t.string   "phone"
    t.string   "skype"
    t.string   "avatar"
    t.integer  "parent_id"
    t.string   "code"
    t.string   "referral"
    t.integer  "role_id",                          default: 0
    t.integer  "account_manager_id"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.float    "balance",                          default: 0.0
    t.float    "balance_next",                     default: 0.0
    t.string   "phone_code"
    t.integer  "admin_id"
    t.boolean  "suspended",                        default: false
    t.string   "suspended_message"
    t.string   "facebook_url"
    t.string   "referral_publisher"
    t.string   "referral_token"
    t.string   "referral_status"
    t.string   "id_card"
    t.string   "foto_id_card"
    t.string   "selfi_id_card"
    t.index ["account_manager_id"], name: "index_users_on_account_manager_id", using: :btree
    t.index ["admin_id"], name: "index_users_on_admin_id", using: :btree
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["parent_id"], name: "index_users_on_parent_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

end
