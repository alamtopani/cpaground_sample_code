module SeedUser
  def self.seed
    User.find_or_create_by!(email: "superadmin@gmail.com") do |admin|
      admin.username = "SuperAdmin"
      admin.skype = "SuperAdminskype"
      admin.password = "superadmin"
      admin.password_confirmation = "superadmin"
      admin.type = "admin"
      admin.role_id = 1
      admin.verified = true
      admin.confirmation_token = nil
      admin.confirmed_at= Time.now
      admin.phone_code = '+62'
      admin.phone = '85777956729'
    end
  end
end