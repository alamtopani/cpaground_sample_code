module SeedWebSetting
  def self.seed
    WebSetting.find_or_create_by({
      header_tags: "Cpaground",
      footer_tags: "© 2017 PT. Ultiface. All Rights Reserved.",
      contact: '+6211+1111+1111',
      email: 'hi@cpaground.com',
      skype: 'hi@cpaground.com',
      facebook: 'http://www.facebook.com/',
      twitter: 'http://www.twitter.com/',
      title: "Cpaground",
      keywords: "Cpaground Indonesia, Cpaground",
      description: 'Cpaground adalah sebuah affiliate marketing.',
      author: "@cpaground",
    })
  end
end
