class AddAccToPaymentSnapshots < ActiveRecord::Migration[5.0]
  def change
    add_column :payment_snapshots, :from_acc, :string
    add_column :payment_snapshots, :to_acc, :string
    add_column :payment_snapshots, :to_acc_am, :string
  end
end
