class AddSuspendedToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :suspended, :boolean, default: false
    add_column :users, :suspended_message, :string
  end
end
