class AddFeePublisherInPaymentSnapshot < ActiveRecord::Migration[5.0]
  def change
    add_column :payment_snapshots, :fee_publisher, :float, default: 0
  end
end
