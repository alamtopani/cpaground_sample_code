class ChangeColumnFromPaymentSnapshot < ActiveRecord::Migration[5.0]
  def change
    remove_column :payment_snapshots, :to_acc_am, :string
    remove_column :payment_snapshots, :account_manager_id, :integer
    rename_column :payment_snapshots, :publisher_id, :user_id
  end
end
