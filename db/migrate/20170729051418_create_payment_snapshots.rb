class CreatePaymentSnapshots < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_snapshots do |t|
      t.string :code
      t.integer :admin_id
      t.integer :publisher_id
      t.integer :account_manager_id
      t.float :price, default: 0
      t.float :fee_am, default: 0
      t.float :fee_company, default: 0
      t.float :total, default: 0
      t.string :message
      t.string :status, default: "pending"
      t.date :payment_at
      t.date :payment_end
      t.string :category

      t.timestamps
    end
    add_index :payment_snapshots, :admin_id
    add_index :payment_snapshots, :publisher_id
    add_index :payment_snapshots, :account_manager_id
  end
end
