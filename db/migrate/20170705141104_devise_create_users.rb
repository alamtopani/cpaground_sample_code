class DeviseCreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      t.string   :unlock_token # Only if unlock strategy is :email or :both
      t.datetime :locked_at

      t.string   :username
      t.string   :provider
      t.string   :uid
      t.string   :slug
      t.boolean  :verified, default: false
      t.string   :type
      t.string   :facebook_uid
      t.string   :facebook_oauth_token
      t.string   :facebook_oauth_token_expires
      t.datetime :facebook_oauth_token_register_at
      t.integer  :facebook_friends_count
      t.string   :facebook_avatar
      t.string   :google_uid
      t.string   :google_oauth_token
      t.string   :google_oauth_token_expires
      t.datetime :google_oauth_token_register_at
      t.integer  :google_followers_count
      t.string   :google_avatar
      t.string   :phone
      t.string   :skype
      t.string   :avatar
      t.integer  :parent_id

      t.string  :code
      t.string  :referral
      t.integer :role_id, default: 0
      t.integer :account_manager_id

      t.timestamps null: false
    end

    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
    add_index :users, :confirmation_token,   unique: true
    add_index :users, :unlock_token,         unique: true
    add_index :users, :parent_id
    add_index :users, :account_manager_id
  end
end
