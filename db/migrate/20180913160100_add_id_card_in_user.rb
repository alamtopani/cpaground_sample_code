class AddIdCardInUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :id_card, :string
    add_column :users, :foto_id_card, :string
    add_column :users, :selfi_id_card, :string
  end
end
