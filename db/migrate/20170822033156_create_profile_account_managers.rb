class CreateProfileAccountManagers < ActiveRecord::Migration[5.0]
  def change
    create_table :profile_account_managers do |t|
      t.integer :account_manager_id
      t.string :first_name
      t.string :last_name
      t.string :country
      t.string :province
      t.string :city
      t.string :address
      t.string :choice_payment_type
      t.string :acc_paypal
      t.string :acc_payoneer
      t.string :acc_bank
      t.string :longitude
      t.string :latitude

      t.timestamps
    end
    
    add_index :profile_account_managers, :account_manager_id
  end
end
