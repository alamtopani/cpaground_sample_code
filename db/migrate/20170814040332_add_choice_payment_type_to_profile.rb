class AddChoicePaymentTypeToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :choice_payment_type, :string
  end
end
