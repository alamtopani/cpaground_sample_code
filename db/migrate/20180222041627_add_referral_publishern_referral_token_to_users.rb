class AddReferralPublishernReferralTokenToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :referral_publisher, :string
    add_column :users, :referral_token, :string
  end
end
