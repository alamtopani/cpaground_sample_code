class ChangeColumnDefaultPaymentCommisions < ActiveRecord::Migration[5.0]
  def change
    change_column_default(:payment_commisions, :status, "pending")
  end
end
