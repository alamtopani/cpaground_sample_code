class AddBankNameToProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :bank_name, :string
    add_column :profiles, :acc_name, :string
    add_column :profile_account_managers, :bank_name, :string
    add_column :profile_account_managers, :acc_name, :string
  end
end
