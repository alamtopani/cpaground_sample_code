class CreatePaymentCommisions < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_commisions do |t|
      t.string :code
      t.integer :admin_id
      t.integer :user_id
      t.date :payment_at
      t.date :payment_end
      t.float :price, default: 0
      t.float :fee_publisher, default: 0
      t.float :fee_am, default: 0
      t.float :total, default: 0
      t.string :from_acc
      t.string :to_acc
      t.string :message
      t.string :status

      t.timestamps
    end
  end
end
