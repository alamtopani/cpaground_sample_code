class AddZipCodeToProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :profiles, :zip_code, :string
    add_column :profile_account_managers, :zip_code, :string
  end
end
