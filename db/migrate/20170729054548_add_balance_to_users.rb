class AddBalanceToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :balance, :float, default: 0
    add_column :users, :balance_next, :float, default: 0
  end
end
