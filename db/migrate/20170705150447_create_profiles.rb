class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.integer :publisher_id
      t.string :first_name
      t.string :last_name
      t.string :country
      t.string :province
      t.string :city
      t.string :address
      t.string :acc_paypal
      t.string :acc_payoneer
      t.string :acc_bank
      t.string :longitude
      t.string :latitude

      t.timestamps
    end

    add_index :profiles, :publisher_id
  end
end
