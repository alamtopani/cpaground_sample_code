class AddPhoneCodeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :phone_code, :string
  end
end
