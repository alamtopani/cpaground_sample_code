source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.0.2'
gem 'sqlite3'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails', '4.3.1'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'

gem "pg"
gem 'mongoid'
gem 'carrierwave-mongoid', :require => 'carrierwave/mongoid'
gem 'kaminari-mongoid'
gem 'kaminari'
gem "haml-rails"
gem "bower-rails", "~> 0.10.0"
gem "bootstrap-sass", "~> 3.3", ">= 3.3.6"
gem "devise", :github => 'plataformatec/devise', :branch => 'master'
gem 'omniauth'
gem 'omniauth-facebook'
gem "omniauth-google-oauth2"
gem 'letter_opener'
gem 'friendly_id'
gem 'ckeditor'
gem 'ckeditor-imgur'
gem 'impressionist'
gem 'cocoon'
gem 'htmlcompressor'
gem 'ancestry'
gem 'better_errors'
gem "paranoia", "~> 2.0"
gem 'ratyrate'
gem 'adminlte2-rails'
gem "breadcrumbs_on_rails"
gem 'enumerate_it'
gem 'chartkick'
gem 'mini_magick'
gem 'carrierwave'
gem 'carrierwave-aws'
gem "binding_of_caller"
gem 'gmaps4rails'
# gem 'acts-as-taggable-on', '~> 4.0'
gem 'select2-rails'
gem 'bootstrap-datepicker-rails'
gem 'groupdate'
gem "rest-client"
gem 'activerecord-session_store', github: 'rails/activerecord-session_store'
gem 'clipboard-rails'

# Utility
gem 'whenever', :require => false
gem 'annotate'
gem 'faker', '1.6.3'
# gem 'factory_girl_rails', '~> 4.0'
gem 'countries'
gem 'country_select'
gem 'city-state'
gem 'money-rails'
gem 'geocoder'
gem 'browser'
gem 'recaptcha', require: 'recaptcha/rails'
gem 'flag-icons-rails'
# gem 'cloudfront-signer'
gem 'google_currency'
gem 'eu_central_bank'
gem 'premailer-rails'
gem 'exception_notification'
gem 'lograge'
gem 'rubyzip', '~> 1.1.0'
gem 'gibbon'
gem 'validates_email_format_of'
gem 'silencer'
gem 'sidekiq'
gem 'redis-rails'
gem 'chartist-rails'
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'jquery-infinite-pages'
gem "recaptcha", require: "recaptcha/rails"
gem 'mongoid-slug'
gem 'roo'
gem 'bitly'
gem 'jquery-datatables'
gem 'device_detector'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
end

group :development do
  gem 'mina'
  gem 'mina-puma', require: false
  gem 'mina-sidekiq'
  gem 'mina-whenever'
  gem 'rubocop', require: false
  gem 'brakeman', require: false

  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'spring-commands-rspec'
  gem 'spring-commands-spinach'
  gem 'spring-commands-rubocop'
  gem 'rack-mini-profiler', require: false
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
