# See http://www.robotstxt.org/robotstxt.html for documentation on how to use the robots.txt file
#
# To ban all spiders from the entire site uncomment the next two lines:
# User-agent: *
# Disallow: /

User-agent: *
Disallow: /app/views/admins/

User-agent:Mediapartners-Google*
Allow:/
User-agent:Googlebot-Image
Allow:/public/uploads/
User-agent:Adsbot-Google
Allow:/
User-agent:Googlebot-Mobile
Allow:/