require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require "action_cable/engine"
require 'sprockets/railtie'
# require "rails/test_unit/railtie"
require 'silencer/logger'

Bundler.require(*Rails.groups)

module Cpaground
  class Application < Rails::Application
    config.active_job.queue_adapter = :sidekiq
    config.time_zone = 'Amsterdam'
    config.cache_store = :redis_store, "redis://#{Rails.application.secrets.redis_host}:6379/0/cache"

    config.active_record.raise_in_transactional_callbacks = true
    config.assets.enabled = true
    config.assets.paths << Rails.root.join("app", "assets", "fonts", "yml")
    config.autoload_paths += %W(#{config.root}/lib)
    config.assets.paths << Rails.root.join("vendor", "assets", "fonts")
    config.assets.paths << Rails.root.join("vendor", "assets", "images")
    config.assets.paths << Rails.root.join("vendor", "assets", "stylesheets")
    config.assets.paths << Rails.root.join("vendor", "assets", "javascripts")
    config.exceptions_app = self.routes
    config.assets.initialize_on_precompile = true
    config.action_dispatch.ignore_accept_header = true
    config.assets.precompile += %w(.svg .eot .woff .ttf)  
    config.quiet_assets = true
    config.action_dispatch.default_headers = {
        'X-Frame-Options' => 'ALLOWALL',
        'Vary' => 'Accept-Encoding'
    }
    config.middleware.use HtmlCompressor::Rack, {preserve_line_breaks: false,javascript_compressor: :ugglifier, css_compressor: :sass, remove_intertag_spaces: true, remove_quotes: false}
    config.autoload_paths += %w(#{config.root}/app/models/ckeditor)
  end
end
