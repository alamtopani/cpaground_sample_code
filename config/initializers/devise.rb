Devise.setup do |config|
  config.mailer_sender = 'noreply@cpaground.com'

  require 'devise/orm/active_record'
  require "omniauth-facebook"
  require 'omniauth-google-oauth2'

  config.omniauth :facebook, Rails.application.secrets.app_id_facebook, Rails.application.secrets.app_secret_facebook, {scope: 'public_profile'}
  config.omniauth :google_oauth2, Rails.application.secrets.app_id_google, Rails.application.secrets.app_secret_google, {skip_jwt: true}

  config.authentication_keys = [:username]
  config.case_insensitive_keys = [:username]
  config.strip_whitespace_keys = [:username]

  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 11
  config.reconfirmable = true
  config.expire_all_remember_me_on_sign_out = true
  config.password_length = 6..128
  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/
  config.reset_password_within = 6.hours
  config.sign_out_via = :delete
end