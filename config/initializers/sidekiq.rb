Sidekiq.configure_server do |config|
  config.redis = { url: "redis://#{Rails.application.secrets.redis_host}:6379/0" }
end

Sidekiq.configure_client do |config|
  config.redis = { url: "redis://#{Rails.application.secrets.redis_host}:6379/0" }
end
