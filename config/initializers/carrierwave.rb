CarrierWave.configure do |config|
  if Rails.env.production? || Rails.env.staging?
    config.storage = :aws
    config.aws_bucket = Rails.application.secrets.aws_aws_bucket
    config.aws_acl = 'public-read'

    config.aws_attributes = {
      expires: 1.week.from_now.httpdate,
      cache_control: 'max-age=604800'
    }

    config.aws_credentials = {
      access_key_id: Rails.application.secrets.aws_access_key_id,
      secret_access_key: Rails.application.secrets.aws_secret_access_key,
      region: Rails.application.secrets.aws_region,
    }
  elsif Rails.env.test?
    config.storage = :file
    config.enable_processing = false
  elsif Rails.env.development?
    config.storage = :file
  end
end