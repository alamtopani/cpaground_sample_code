Rails.application.routes.draw do
  devise_for :users,
    controllers: {
      omniauth_callbacks: 'omniauth_callbacks',
      registrations: 'registrations',
      sessions: 'sessions',
      confirmations: 'confirmations'
    },
    path_names: {
      sign_in:  'login',
      sign_out: 'logout',
      sign_up:  'register'
    }

  mount Ckeditor::Engine => '/ckeditor'

  root "home#index"
  get 'contact', to: "home#index#contact"
  get 'health-check', to: 'home#health_check'
  get 'about-us', to: 'about_us#index'
  get 'faqs', to: 'faqs#index'
  get 'features', to: 'features#index'
  get 'how-it-works', to: 'how_it_works#index'
  get 'contact', to: 'contacts#index'
  get 'privacy-policy', to: 'privacy_policy#index'
  get 'gdpr-cpaground', to: 'gdpr#index'
  get 'thank_you/:id', to: 'home#thank_you', as: "thank_you"
  get 'waiting_verified/:id', to: 'home#waiting_verified', as: "waiting_verified"
  get 'under-maintenance', to: 'home#under_maintenance', as: 'under_maintenance'
  get '404-not-found', to: 'home#not_found_page', as: 'not_found_page'
  get 'lombok', to: 'home#lombok', as: 'lombok'
  resources :contacts
  resources :subscribers, only: [:create] do 
    collection do
      get :subscribe
      get :unsubscribe
    end
  end

  resources :xhrs do
    collection do
      get :cities
      get :get_box_report
      get :get_balances
      get :get_revenues
      get :get_commisions
      get :get_notifications
      get :get_campaign
      get :get_campaign_date_form
      get :get_offer_cr
      get :get_payment_owners
      get :get_convertion_report
      get :change_convertion_report
    end
  end

  namespace :admins do
    resources :dashboards
    resources :owners
    resources :invoice_owners do
      member do
        put :change_status
      end
    end
    resources :admins
    resources :account_managers do
      member do
        put :change_status
        get :detail_report
      end
      collection do
        get :multiple_action
      end
    end
    resources :publishers do
      member do
        put :change_status
        get :detail_report
      end
      collection do
        get :multiple_action
        get :list_email
      end
    end
    resources :campaigns do
      collection do
        get :multiple_action
        post :import
      end
      member do
        put :change_status
        get :click_notif
      end
    end
    resources :category_campaigns do
      collection do
        get :multiple_action
        get :multiple_action_targeting
        get :multiple_action_banner_image
        post :import
        post :import_per_offer
      end
      member do
        put :change_status
        delete :delete_targeting
        delete :delete_banner_image
      end
    end
    resources :report_campaign_snapshots do
      member do
        put :change_convertion_report
        delete :delete_convertion_report
      end
    end
    resources :contacts
    resources :web_settings
    resources :payment_snapshots do
      collection do
        get :multiple_action
      end
      member do
        put :change_status
      end
    end
    resources :layout_themes do
      collection do
        get :multiple_action
      end
      member do
        put :change_status
      end
    end
    resources :request_landing_pages do
      collection do
        get :multiple_action
      end
      member do
        put :change_status
        get :click_notif
      end
    end
    resources :subscribers, only: [:index, :show, :destroy] do
      member do
        put :change_status
      end
    end
    resources :blogs do
      collection do
        get :multiple_action
      end
      member do
        put :change_status
      end
    end
    resources :forums do
      collection do
        get :multiple_action
      end
      member do
        put :change_status
        delete :delete_comment
      end
    end
    resources :feeds do
      collection do
        post :comment_create
      end
    end
    resources :invoices do
      member do
        put :change_status
      end
    end
    resources :groups do
      collection do
        get :multiple_action
      end
      member do
        put :change_status
      end
    end
    resources :access_offers do
      collection do
        get :multiple_action
      end
    end
    resources :reports do
      collection do
        get :input
      end
      member do
        delete :delete_report_item
      end
      collection do
        get :multiple_action
      end
    end
    resources :informations do
      member do
        put :change_status
      end
      collection do
        get :multiple_action
      end
    end
    resources :permission_referral_publishers
    resources :commisions
    resources :payment_commisions do
      collection do
        get :multiple_action
      end
      member do
        put :change_status
      end
    end
    resources :messages
  end

  namespace :account_managers do
    resources :dashboards do
      collection do
        resources :rankings
      end
    end
    resources :campaigns do
      member do
        get :click_notif
        put :approvement
      end
    end
    resources :offers
    resources :profiles
    resources :activities
    resources :forums do
      member do
        delete :delete_comment
      end
    end
    resources :blogs do
      collection do
        post :comment_create
      end
    end
    resources :referrals
    resources :payments do
      member do
        get :click_notif
      end
    end
    resources :publishers do
      member do
        get :click_message
        get :detail_report
        put :change_status
        put :suspended
      end
    end
    resources :messages do
      collection do
        post :blast_message
      end
    end
    resources :feeds do
      collection do
        post :comment_create
        post :like_create
      end
    end
    resources :request_landing_pages do
      member do
        get :click_notif
      end
    end
    resources :notifications
    resources :groups do
      member do
        # put :change_status
        put :change_status_group_coverage
        get :click_notif
      end
    end
    resources :offers do
      member do
        get :click_notif
        put :checked
        put :change_status
      end
    end
    resources :reports
    resources :permission_referral_publishers
    resources :commisions
  end

  namespace :publishers do
    # get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :dashboards
    resources :campaigns do
      member do
        get :click_notif
      end
    end
    resources :offers do
      member do
        get :click_notif
        put :checked
        put :change_status
      end
    end
    resources :profiles
    resources :activities
    resources :forums
    resources :blogs do
      collection do
        post :comment_create
      end
    end
    resources :referrals
    resources :payments do
      member do
        get :click_notif
      end
    end
    resources :managers
    resources :messages
    resources :feeds do
      collection do
        post :comment_create
        post :like_create
      end
    end
    resources :request_landing_pages do
      member do
        get :click_notif
        put :change_status
      end
      collection do
        get :download_lp
      end
    end
    resources :notifications
    resources :groups do
      collection do
        post :join
      end
      member do
        get :click_notif
      end
    end
    resources :access_offers
    resources :reports
    resources :referral_publishers
    resources :commisions
    resources :payment_commisions do
      member do
        get :click_notif
      end
    end
    resources :offer_commisions
  end

  namespace :api do
    resources :lp do
      collection do
        get :track_visit
        get :postback
        get :get_campaigns
      end
    end
  end
  get 'postback', to: 'api/lp#postback', as: "postback"
end
