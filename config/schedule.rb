set :environment, "staging"
set :output, "/home/cpaground/stagingcpaground.com/shared/log/cron_log.log"
set :bundle_command, "/home/cpaground/.rbenv/shims/bundle exec"

every 3.days do
  rake "log:clear"
end

# every 5.hours do
#   rake "cron_job:create_payment"
# end

# every 1.day do
#   rake "cron_job:check_publisher_verified"
# end

# every 5.minutes do
#   rake "cron_job:check_job"
# end

# every 5.minutes do
#   rake "cron_job:delete_convertion_report"
# end