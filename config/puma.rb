threads_count = ENV.fetch("RAILS_MAX_THREADS") { 5 }.to_i
threads 1, 16

port        ENV.fetch("PORT") { 3000 }
plugin :tmp_restart


if ENV['RAILS_ENV'] == 'productiontrack' || ENV['RAILS_ENV'] == 'production' || ENV['RAILS_ENV'] == 'staging'
  ########### FOR DEPLOY 
  # Dir

  if ENV['RAILS_ENV'] == 'production'
    shared_dir = '/home/cpagroundpro/cpaground.com'
  elsif ENV['RAILS_ENV'] == 'productiontrack'
    shared_dir = '/home/cpagroundtrack/track.cpaground.com'
  else
    shared_dir = '/home/cpaground/stagingcpaground.com'
  end

  # Set up socket location
  bind "unix://#{shared_dir}/shared/tmp/sockets/puma.sock"

  # Logging
  # stdout_redirect "#{shared_dir}/shared/log/puma.stdout.log", "#{shared_dir}/shared/log/puma.stderr.log", true

  # Set master PID and state locations
  pidfile "#{shared_dir}/shared/tmp/pids/puma.pid"
  state_path "#{shared_dir}/shared/tmp/pids/puma.state"
  activate_control_app 'unix://#{shared_dir}/shared/tmp/sockets/pumactl.sock'

  workers 3
  threads 1, 16

  daemonize true
  prune_bundler
end