namespace :cron_job do
  desc "TODO"
  task check_job: :environment do
    CategoryCampaign.where(action: 'CPS').where(status: "active", :expires.lte => Date.today).each do |resource|
      resource.status = CategoryCampaign::INACTIVE
      resource.inactive_campaign
      resource.save
    end
  end

  task check_publisher_verified: :environment do
    # Publisher.confirmed.pending.no_suspended.where("created_at < ?", 3.days.ago).each do |resource|
    Publisher.where(account_manager_id: nil).confirmed.pending.no_suspended.each do |resource|
      resource.account_manager_id = AccountManager.verified.where.not(username: ['adam','alam','inactive','inactive2','yazid','roadshow']).pluck(:id).sample if resource.account_manager_id.blank?
      resource.admin_id = Admin.find_by(slug: 'winda').id if resource.admin_id.blank?
      # resource.verified = true
      if resource.save
        verification_snapshot = VerificationSnapshot.new
        verification_snapshot.publisher_id = resource.id
        verification_snapshot.admin_id = resource.admin_id
        verification_snapshot.account_manager_id = resource.account_manager_id
        verification_snapshot.status = VerificationSnapshot::ACTIVE
        verification_snapshot.message = "(Automatic Verified Publisher) has change verification status to #{verification_snapshot.status}."
        verification_snapshot.save
        Activity.create(user_id: resource.admin_id, name: "verification publisher", description: "(Automatic Verified Publisher) has successfully verification publisher #{resource.username}", activitiable_id: resource.id, activitiable_type: resource.class.name)
        UserMailer.publisher_approved(resource).deliver_now! if resource.is_verified?
      end
    end
  end

  task delete_convertion_report: :environment do
    date = Time.zone.now.to_date
    date = date.beginning_of_month - 2.months
    date = date.end_of_month

    puts "Start execute deleted convertion reports"
    ConvertionReport.where(:convertion_date.lte => date, status: "visited").delete_all
    puts "Success execute deleted convertion reports"
  end

  task create_payment: :environment do
    finance = Admin.where(role_id: User::FINANCE_ROLE).first

    date_zone = Time.zone.now.to_date
    last_month = (date_zone.beginning_of_month - 1.month)
    beginning_of_month = date_zone.beginning_of_month
    half_end_of_month = date_zone.beginning_of_month + 14.days
    beginning_half_of_month = half_end_of_month + 1.days

    payment_period_one = beginning_of_month + 5.days
    payment_period_two = beginning_half_of_month + 5.days
    
    if payment_period_one == date_zone
      payment_start = last_month.beginning_of_month + 15.days
      payment_end = last_month.end_of_month
    elsif payment_period_two == date_zone
      payment_start = beginning_of_month
      payment_end = half_end_of_month
    end

    if payment_period_one == date_zone || payment_period_two == date_zone
      report_campaign_snapshots = ReportCampaignSnapshot.where(:campaign_date.gte => payment_start, :campaign_date.lte => payment_end).have_sales_only
      
      Publisher.verified.no_suspended.where(id: report_campaign_snapshots.pluck(:publisher_id).uniq).each do |resource|
        last_payment = resource.payment_snapshots.last
        unless last_payment.payment_at == payment_start && last_payment.payment_end == payment_end
          @payment_snapshot = PaymentSnapshot.new
          @payment_snapshot.admin_id = finance.id
          @payment_snapshot.user_id = resource.id
          @payment_snapshot.payment_at = payment_start
          @payment_snapshot.payment_end = payment_end
          @payment_snapshot.status = PaymentSnapshot::PENDING
          @payment_snapshot.from_acc = "CPAGROUND"
          @payment_snapshot.to_acc = resource.profile.payment_to
          @payment_snapshot.message = "Invoice payment #{payment_start} - #{payment_end}."

          reports = report_campaign_snapshots.where(publisher_id: resource.id)
          @payment_snapshot.fee_am = '%.4f' %  reports.sum(:fee_am)
          @payment_snapshot.fee_publisher = '%.4f' %  reports.sum(:fee_publisher)
          @payment_snapshot.fee_company = '%.4f' %  reports.sum(:fee_company)
          @payment_snapshot.total = '%.4f' % reports.sum(:revenue)
          @payment_snapshot.price = @payment_snapshot.fee_publisher
          @payment_snapshot.save
          User.prepare_activity(@payment_snapshot, finance.id, "new payment snapshot", "successfully created automatically payment snapshot period #{@payment_snapshot.payment_at} - #{@payment_snapshot.payment_end}")
        end
      end

      AccountManager.verified.where(id: report_campaign_snapshots.pluck(:account_manager_id).uniq).each do |resource|
        last_payment = resource.payment_snapshots.last
        unless last_payment.payment_at == payment_start && last_payment.payment_end == payment_end
          @payment_snapshot = PaymentSnapshot.new
          @payment_snapshot.admin_id = finance.id
          @payment_snapshot.user_id = resource.id
          @payment_snapshot.payment_at = payment_start
          @payment_snapshot.payment_end = payment_end
          @payment_snapshot.status = PaymentSnapshot::PENDING
          @payment_snapshot.from_acc = "CPAGROUND"
          @payment_snapshot.to_acc = resource.profile_account_manager.payment_to
          @payment_snapshot.message = "Invoice payment #{payment_start} - #{payment_end}."

          reports = report_campaign_snapshots.where(account_manager_id: resource.id)
          @payment_snapshot.fee_am = '%.4f' %  reports.sum(:fee_am)
          @payment_snapshot.fee_publisher = '%.4f' %  reports.sum(:fee_publisher)
          @payment_snapshot.fee_company = '%.4f' %  reports.sum(:fee_company)
          @payment_snapshot.total = '%.4f' % reports.sum(:revenue)
          @payment_snapshot.price = @payment_snapshot.fee_am
          @payment_snapshot.save
          User.prepare_activity(@payment_snapshot, finance.id, "new payment snapshot", "successfully created automatically payment snapshot period #{@payment_snapshot.payment_at} - #{@payment_snapshot.payment_end}")
        end
      end

      commissions = Commision.where(:campaign_date.gte => payment_start, :campaign_date.lte => payment_end, status: "approved")

      Publisher.verified.no_suspended.where(id: commissions.pluck(:publisher_id).uniq).each do |resource|
        last_payment = resource.payment_commisions.last
        unless last_payment.payment_at == payment_start && last_payment.payment_end == payment_end
          @payment_commision = PaymentCommision.new
          @payment_commision.admin_id = finance.id
          @payment_commision.user_id = resource.id
          @payment_commision.payment_at = payment_start
          @payment_commision.payment_end = payment_end
          @payment_commision.status = PaymentCommision::PENDING
          @payment_commision.from_acc = "CPAGROUND"
          @payment_commision.to_acc = resource.profile.payment_to
          @payment_commision.message = "Invoice commission #{payment_start} - #{payment_end}."

          reports = commissions.where(publisher_id: resource.id)
          @payment_commision.fee_am = '%.4f' %  reports.sum(:fee_am)
          @payment_commision.fee_publisher = '%.4f' %  reports.sum(:fee_publisher)
          @payment_commision.total = '%.4f' % reports.sum(:revenue)
          @payment_commision.price = @payment_commision.fee_publisher
          @payment_commision.save
          User.prepare_activity(@payment_commision, finance.id, "new payment commission", "successfully created automatically payment commission period #{@payment_commision.payment_at} - #{@payment_commision.payment_end}")
        end
      end
    end
  end
end