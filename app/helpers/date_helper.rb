module DateHelper
  def get_range_date_chart(start_at, end_at)
    if start_at.present? && end_at.present?
      if start_at == end_at
        return start_at.to_datetime.strftime(" %d %B %Y")
      else
        return [start_at.to_datetime.strftime(" %d %B %Y"), end_at.to_datetime.strftime(" %d %B %Y")].join(' - ')
      end
    elsif start_at.present? && end_at.blank?
      return start_at.to_datetime.strftime(" %d %B %Y")
    elsif start_at.blank? && end_at.present?
      return end_at.to_datetime.strftime(" %d %B %Y")
    else
      return Time.zone.now.strftime(" %d %B %Y")
    end    
  end
  
  def get_time(datetime)
    datetime.strftime("%I:%M")
  end

  def get_datetime(datetime)
    datetime.strftime(" %B %d, %Y - %I:%M ")
  end

  def date_sort_time(datetime)
    datetime.strftime(" %b %d, %Y")
  end

  def get_date_now(start_at)
    if start_at.present?
      start_at.to_datetime
    else
      Time.zone.now
    end
  end

  def time_to_ago(time)
    a = (Time.zone.now - time).to_i

    case a
      when 0 then 'just now'
      when 1 then 'a second ago'
      when 2..59 then a.to_s+' seconds ago' 
      when 60..119 then 'a minute ago' #120 = 2 minutes
      when 120..3540 then (a/60).to_i.to_s+' minutes ago'
      when 3541..7100 then 'an hour ago' # 3600 = 1 hour
      when 7101..82800 then ((a+99)/3600).to_i.to_s+' hours ago' 
      when 82801..172000 then 'a day ago' # 86400 = 1 day
      when 172001..518400 then ((a+800)/(60*60*24)).to_i.to_s+' days ago'
      when 518400..1036800 then 'a week ago'
      else ((a+180000)/(60*60*24*7)).to_i.to_s+' weeks ago'
    end
  end
end