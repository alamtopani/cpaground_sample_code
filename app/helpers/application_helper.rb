module ApplicationHelper
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", errors: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    if flash.present?
      content_tag(:div, class: 'section-alert') do
        flash.each do |msg_type, message|
          concat(content_tag(:div, message, class: "alert-publisher alert #{bootstrap_class_for(msg_type)} alert-dismissible", role: 'alert') do
            concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
              concat content_tag(:span, '&times;'.html_safe, 'aria-hidden' => true)
              concat content_tag(:span, 'Close', class: 'sr-only')
            end)
            if flash[:errors].present?
              message.each do |word|
                concat "<div class='alert-publisher alert alert-success alert-dismissible margin-bottom5'>#{word}</div>".html_safe
              end
            else
              concat "#{message}".html_safe
            end
          end)
        end
        nil
      end
    end
  end

  def active_sidebar?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'active'
    end if controller_name == controller.to_s
  end

  def active_sidebar_params?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'active'
    end if request.fullpath == controller.to_s
  end

  def active_sidebar_group?(controller, group_id)
    if params[:id] == group_id
      return 'active'
    end if controller_name == controller.to_s
  end

  def get_currency(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "$", :separator => ",", :delimiter => ".", precision: 2)
  end

  def get_currency_full(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "$", :separator => ",", :delimiter => ".", precision: 4)
  end

  def get_currency_int(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "$", :separator => ",", :delimiter => ".", precision: 0)
  end

  def get_percent(number)
    number = 0 if number.blank?
    [number, "%"].select(&:'present?').join(' ')
  end

  def get_currency_rp(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "Rp. ", :separator => ",", :delimiter => ".", precision: 2)
  end

  def get_format_number(number)
    number = 0 if number.blank?
    number_to_currency(number, :unit => "", :separator => ",", :delimiter => ".", precision: 0)
  end

  def web_setting
    web_setting = WebSetting.first
  end

  def message_has_readed_am(message_status)
    if message_status == "read"
      return "<li style='background: none;'>".html_safe
    else
      return "<li>".html_safe
    end
  end

  def message_status(status)
    if status == "unread"
      return "<label class='label label-success'>New</label>".html_safe
    end
  end

  # def link_shorter(link)
  #   bitly = Bitly.new("login", "api_key")
  #   return bitly.shorten('http://www.google.com').short_url
  # end
end