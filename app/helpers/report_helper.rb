module ReportHelper
  def prepare_date
    @date = Time.zone.now.to_date
    @_last_month = (@date.beginning_of_month - 1.month)
    @_beginning_of_month = @date.beginning_of_month
    @_half_end_of_month = @date.beginning_of_month + 14.days
    @_beginning_half_of_month = @_half_end_of_month + 1.days
    @_end_of_month = @date.end_of_month
  end

  def query_last_next_payment(user, reports)
    if @_beginning_of_month <= @date && @_half_end_of_month >= @date
      reports = reports.where(:campaign_date.gte => @_beginning_of_month, :campaign_date.lte => @_half_end_of_month)
      @_next_payment = reports
      @_next_payment = user.present? ? @_next_payment.total(user.type) : @_next_payment.sum(:revenue)
      @_next_payment_all = reports.sum(:revenue)
      @_next_payment_pub = reports.total(User::PUBLISHER)
      @_next_payment_am = reports.total(User::ACCOUNTMANAGER)
      @_next_payment_admin = reports.total(User::ADMIN)
      @_start_date_payment = @_beginning_of_month

      @_last_payment_start_at = @_last_month.beginning_of_month + 15.days
      @_last_payment_end_at = @_last_month.end_of_month
    elsif @_beginning_half_of_month <= @date && @_end_of_month >= @date
      reports = reports.where(:campaign_date.gte => @_beginning_half_of_month, :campaign_date.lte => @_end_of_month)
      @_next_payment = reports
      @_next_payment = user.present? ? @_next_payment.total(user.type) : @_next_payment.sum(:revenue)
      @_next_payment_all = reports.sum(:revenue)
      @_next_payment_pub = reports.total(User::PUBLISHER)
      @_next_payment_am = reports.total(User::ACCOUNTMANAGER)
      @_next_payment_admin = reports.total(User::ADMIN)
      @_start_date_payment = @_beginning_half_of_month

      @_last_payment_start_at = @date.beginning_of_month
      @_last_payment_end_at = @_last_payment_start_at + 14.days
    end

    reports = reports.where(:campaign_date.gte => @_last_payment_start_at, :campaign_date.lte => @_last_payment_end_at)
    @_last_payment = reports
    @_last_payment = user.present? ? @_last_payment.total(user.type) : @_last_payment.sum(:revenue)

    @_last_payment_all = reports.sum(:revenue)
    @_last_payment_pub = reports.total(User::PUBLISHER)
    @_last_payment_am = reports.total(User::ACCOUNTMANAGER)
    @_last_payment_admin = reports.total(User::ADMIN)
  end

  def get_payment(type, user)
    prepare_date
    reports = user.type == User::PUBLISHER ? ReportCampaignSnapshot.where(:campaign_date.gte => @_last_month).where(publisher_id: user.id).have_sales_only : ReportCampaignSnapshot.where(:campaign_date.gte => @_last_month).where(account_manager_id: user.id).have_sales_only
    query_last_next_payment(user, reports)

    if type == 'last_payment'
      return @_last_payment
    elsif type == 'ongoing_payment'
      return @_next_payment
    end
  end

  def ongoing_payment(user)
    prepare_date
    reports = user.is_publisher? ? ReportCampaignSnapshot.where(:campaign_date.gte => @_last_month).where(publisher_id: user.id).have_sales_only : ReportCampaignSnapshot.where(:campaign_date.gte => @_last_month).where(account_manager_id: user.id).have_sales_only
    next_last_payment(user, reports)
    @invoice_payments = user.payment_snapshots.latest.limit(4)
  end 

  def next_last_payment_all(reports)
    prepare_date
    query_last_next_payment(nil, reports)
  end

  def next_last_payment(user, reports)
    if user.present? && reports.present?
      query_last_next_payment(user, reports)
      
      if user.is_account_manager?
        @_last_payment_publishers = reports.where(:campaign_date.gte => @_last_payment_start_at, :campaign_date.lte => @_last_payment_end_at).total(User::PUBLISHER)
      end
    end
  end

  def get_analytic_report(user, report_for)
    @start_date = params[:start_at]
    @end_date = params[:end_at]
    prepare_date

    unless user.type == User::ADMIN
      if report_for == 'dashbord_publisher' || report_for == 'dashbord_admin_publisher' || report_for == 'dashboard_am_publisher'
        reports = ReportCampaignSnapshot.where(publisher_id: @publisher.id).where(:campaign_date.gte => @_last_month)
        next_last_payment(user, reports)
        @earning_today_so_far = reports.have_sales_only.where(campaign_date: @date).total(user.type) rescue 0
        @earning_yesterday = reports.have_sales_only.where(campaign_date: @date.yesterday).total(user.type) rescue 0
        @earning_last_7_month = reports.have_sales_only.where(:campaign_date.gte => @date - 6.days, :campaign_date.lte => @date).total(user.type) rescue 0
        @earning_this_month = reports.have_sales_only.where(:campaign_date.gte => @date.beginning_of_month, :campaign_date.lte => @date.end_of_month).total(user.type) rescue 0
        @_last_payment = reports.have_sales_only.where(:campaign_date.gte => @_last_payment_start_at, :campaign_date.lte => @_last_payment_end_at).total(user.type) rescue 0
      elsif report_for == 'dashbord_publisher_campaign' || report_for == 'dashboard_am_campaign' || report_for == 'dashbord_admin_campaign'
        reports = ReportCampaignSnapshot.where(campaign_id: @campaign.id).where(:campaign_date.gte => @_last_month)
        next_last_payment(user, reports)
        @earning_today_so_far = reports.have_sales_only.where(campaign_date: @date).total(user.type) rescue 0
        @earning_yesterday = reports.have_sales_only.where(campaign_date: @date.yesterday).total(user.type) rescue 0
        @earning_last_7_month = reports.have_sales_only.where(:campaign_date.gte => @date - 6.days, :campaign_date.lte => @date).total(user.type) rescue 0
        @earning_this_month = reports.have_sales_only.where(:campaign_date.gte => @date.beginning_of_month, :campaign_date.lte => @date.end_of_month).total(user.type) rescue 0
        @_last_payment = reports.have_sales_only.where(:campaign_date.gte => @_last_payment_start_at, :campaign_date.lte => @_last_payment_end_at).total(user.type) rescue 0
      elsif report_for == 'dashboard_am' || report_for == 'dashbord_admin_am'
        reports = ReportCampaignSnapshot.where(account_manager_id: @account_manager.id).where(:campaign_date.gte => @_last_month)
        next_last_payment(user, reports)
        @earning_today_so_far = reports.have_sales_only.where(campaign_date: @date).total(user.type) rescue 0
        @earning_yesterday = reports.have_sales_only.where(campaign_date: @date.yesterday).total(user.type) rescue 0
        @earning_last_7_month = reports.have_sales_only.where(:campaign_date.gte => @date - 6.days, :campaign_date.lte => @date).total(user.type) rescue 0
        @earning_this_month = reports.have_sales_only.where(:campaign_date.gte => @date.beginning_of_month, :campaign_date.lte => @date.end_of_month).total(user.type) rescue 0
        @_last_payment = reports.have_sales_only.where(:campaign_date.gte => @_last_payment_start_at, :campaign_date.lte => @_last_payment_end_at).total(user.type) rescue 0
      end
    end

    if report_for == 'dashboard_am'
      @earning_today_so_far_publisher = reports.have_sales_only.where(campaign_date: @date).total(User::PUBLISHER) rescue 0
      @earning_yesterday_publisher = reports.have_sales_only.where(campaign_date: @date.yesterday).total(User::PUBLISHER) rescue 0
      @earning_last_7_month_publisher = reports.have_sales_only.where(:campaign_date.gte => @date - 6.days, :campaign_date.lte => @date).total(User::PUBLISHER) rescue 0
      @earning_this_month_publisher = reports.have_sales_only.where(:campaign_date.gte => @date.beginning_of_month, :campaign_date.lte => @date.end_of_month).total(User::PUBLISHER) rescue 0
    elsif report_for == 'dashboard_am_publisher' || report_for == 'dashboard_am_campaign'
      @earning_today_so_far_am = reports.have_sales_only.where(campaign_date: @date).total(User::ACCOUNTMANAGER) rescue 0
      @earning_yesterday_am = reports.have_sales_only.where(campaign_date: @date.yesterday).total(User::ACCOUNTMANAGER) rescue 0
      @earning_last_7_month_am = reports.have_sales_only.where(:campaign_date.gte => @date - 6.days, :campaign_date.lte => @date).total(User::ACCOUNTMANAGER) rescue 0
      @earning_this_month_am = reports.have_sales_only.where(:campaign_date.gte => @date.beginning_of_month, :campaign_date.lte => @date.end_of_month).total(User::ACCOUNTMANAGER) rescue 0
      @_last_payment_am = reports.have_sales_only.where(:campaign_date.gte => @_last_payment_start_at, :campaign_date.lte => @_last_payment_end_at).total(User::ACCOUNTMANAGER) rescue 0
    end

    if @start_date.blank? && @end_date.blank?
      if report_for == 'dashbord_publisher' || report_for == 'dashbord_admin_publisher'
        @reports = ReportCampaignSnapshot.where(publisher_id: @publisher.id).where(campaign_date: @date) rescue nil
        @convertion_reports = ConvertionReport.where(publisher_id: @publisher.id).have_sales_only.where(convertion_date: @date) rescue nil
      elsif report_for == 'dashbord_publisher_campaign' || report_for == 'dashboard_am_campaign' || report_for == 'dashbord_admin_campaign'
        @reports = ReportCampaignSnapshot.where(campaign_id: @campaign.id).where(campaign_date: @date) rescue nil
        @convertion_reports = ConvertionReport.where(campaign_id: @campaign.id).have_sales_only.where(convertion_date: @date) rescue nil
      elsif report_for == 'dashboard_am' || report_for == 'dashbord_admin_am'
        @reports = ReportCampaignSnapshot.where(account_manager_id: @account_manager.id).where(campaign_date: @date) rescue nil
        @convertion_reports = ConvertionReport.where(account_manager_id: @account_manager.id).have_sales_only.where(convertion_date: @date) rescue nil
      elsif report_for == 'dashboard_am_publisher'
        @reports = ReportCampaignSnapshot.where(publisher_id: @publisher.id).where(account_manager_id: current_user.id).where(campaign_date: @date) rescue nil
        @convertion_reports = ConvertionReport.where(publisher_id: @publisher.id).where(account_manager_id: current_user.id).have_sales_only.where(convertion_date: @date) rescue nil
      elsif report_for == 'dashbord_admin'
        @reports = ReportCampaignSnapshot.where(campaign_date: @date) rescue nil
        @reports = @reports.where(admin_id: current_user.id) if current_user.is_staff?

        # if current_user.is_staff?
        #   @convertion_reports = ConvertionReport.have_sales_only.where(admin_id: current_user.id).where(campaign_date: @date) rescue nil
        # else
        #   @convertion_reports = ConvertionReport.have_sales_only.where(campaign_date: @date) rescue nil
        # end
      end
    else
      if report_for == 'dashbord_publisher' || report_for == 'dashbord_admin_publisher'
        @reports = ReportCampaignSnapshot.where(publisher_id: @publisher.id).search_by_date(params) rescue nil
        @convertion_reports = ConvertionReport.where(publisher_id: @publisher.id).have_sales_only.search_by_date(params) rescue nil
      elsif report_for == 'dashbord_publisher_campaign' || report_for == 'dashboard_am_campaign' || report_for == 'dashbord_admin_campaign'
        @reports = ReportCampaignSnapshot.where(campaign_id: @campaign.id).search_by_date(params) rescue nil
        @convertion_reports = ConvertionReport.where(campaign_id: @campaign.id).have_sales_only.search_by_date(params) rescue nil
      elsif report_for == 'dashboard_am' || report_for == 'dashbord_admin_am'
        @reports = ReportCampaignSnapshot.where(account_manager_id: @account_manager.id).search_by_date(params) rescue nil
        @convertion_reports = ConvertionReport.where(account_manager_id: @account_manager.id).have_sales_only.search_by_date(params) rescue nil
      elsif report_for == 'dashboard_am_publisher'
        @reports = ReportCampaignSnapshot.where(publisher_id: @publisher.id).where(account_manager_id: current_user.id).search_by_date(params) rescue nil
        @convertion_reports = ConvertionReport.where(publisher_id: @publisher.id).where(account_manager_id: current_user.id).have_sales_only.search_by_date(params) rescue nil
      elsif report_for == 'dashbord_admin'
        @reports = ReportCampaignSnapshot.search_by_date(params) rescue nil
        @reports = @reports.where(admin_id: current_user.id) if current_user.is_staff?

        # if current_user.is_staff?
        #   @convertion_reports = ConvertionReport.where(admin_id: current_user.id).have_sales_only.search_by_date(params) rescue nil
        # else
        #   @convertion_reports = ConvertionReport.have_sales_only.search_by_date(params) rescue nil
        # end
      end
    end

    @reports = nil if @reports.blank?

    @convertion_reports = @convertion_reports rescue nil unless report_for == 'dashboard_am'
    @report_campaign_snapshots = @reports.latest.page(params[:page_report_campaign_snapshots]).per(per_page) rescue nil

    if user.is_account_manager?
      @reports_collection = @reports.group_by {|r| r.publisher_id} rescue nil
    else
      @reports_collection = @reports.group_by {|r| r.campaign_id} rescue nil
    end

    if @reports.present?
      @daily_reports_group = @reports.group_by {|d| d.campaign_date}
      @monthly_reports_group = @reports.group_by {|d| d.campaign_date.strftime("%B %Y")}
      @yearly_reports_group = @reports.group_by {|d| d.campaign_date.strftime("%Y")}

      if report_for == 'dashbord_admin_campaign' || report_for == 'dashbord_publisher_campaign' || report_for == 'dashboard_am_campaign'
        @daily_reports_array = set_collection_report(@daily_reports_group, report_for, params)
        @monthly_reports_array = set_collection_report(@monthly_reports_group, report_for, params)
        @yearly_reports_array = set_collection_report(@yearly_reports_group, report_for, params)
      else
        @daily_reports_array = set_collection_report(@reports_collection, report_for, params)
        @monthly_reports_array = set_collection_report(@reports_collection, report_for, params)
        @yearly_reports_array = set_collection_report(@reports_collection, report_for, params)
      end

      @daily_reports_collection = Kaminari.paginate_array(@daily_reports_array).page(params[:day]).per(per_page)
      @monthly_reports_collection = Kaminari.paginate_array(@monthly_reports_array).page(params[:month]).per(per_page)
      @yearly_reports_collection = Kaminari.paginate_array(@yearly_reports_array).page(params[:year]).per(per_page)

      if user.type == User::ADMIN || user.type == User::OWNER
        @reports_total_revenue = @reports.have_sales_only.sum(:revenue)
      else
        @reports_total_revenue = @reports.have_sales_only.total(user.type)
      end
    end
  end

  def set_collection_report(reports, report_for, params)
    if report_for == 'dashbord_admin'
      reports = reports.values.map{|w| [w.map(&:campaign_id).last, w.map(&:publisher_id).last, 0, w.map(&:account_manager_id).last.to_i, w.map(&:impressions).reduce(:+), w.map(&:clicks).reduce(:+), w.map(&:visitors).reduce(:+), w.map(&:unique_visitors).reduce(:+), w.map(&:sales).reduce(:+), w.map(&:revenue).reduce(:+), w.map(&:created_at).last] }
    elsif report_for == 'dashbord_admin_publisher' || report_for == 'dashboard_am_publisher' || report_for == 'dashbord_publisher'
      reports = reports.values.map{|w| [w.map(&:campaign_id).last, '-', 0, w.map(&:admin_id).last.to_i, w.map(&:impressions).reduce(:+), w.map(&:clicks).reduce(:+), w.map(&:visitors).reduce(:+), w.map(&:unique_visitors).reduce(:+), w.map(&:sales).reduce(:+), w.map(&:fee_publisher).reduce(:+), w.map(&:created_at).last] }
    elsif report_for == 'dashbord_admin_am' || report_for == 'dashboard_am'
      reports = reports.values.map{|w| [w.map(&:publisher_id).last, '-', 0, w.map(&:admin_id).last.to_i, w.map(&:impressions).reduce(:+), w.map(&:clicks).reduce(:+), w.map(&:visitors).reduce(:+), w.map(&:unique_visitors).reduce(:+), w.map(&:sales).reduce(:+), w.map(&:fee_am).reduce(:+), w.map(&:created_at).last] }
    elsif report_for == 'dashbord_admin_campaign'
      reports = reports.values.map{|w| [w.map(&:campaign_date).last, '-', 0, w.map(&:admin_id).last.to_i, w.map(&:impressions).reduce(:+), w.map(&:clicks).reduce(:+), w.map(&:visitors).reduce(:+), w.map(&:unique_visitors).reduce(:+), w.map(&:sales).reduce(:+), w.map(&:revenue).reduce(:+), w.map(&:created_at).last] }
    elsif  report_for == 'dashboard_am_campaign' || report_for == 'dashbord_publisher_campaign'
      reports = reports.values.map{|w| [w.map(&:campaign_date).last, '-', 0, w.map(&:admin_id).last.to_i, w.map(&:impressions).reduce(:+), w.map(&:clicks).reduce(:+), w.map(&:visitors).reduce(:+), w.map(&:unique_visitors).reduce(:+), w.map(&:sales).reduce(:+), w.map(&:fee_publisher).reduce(:+), w.map(&:created_at).last] }
    end

    if params[:sort_by].present?
      if params[:sort_by] == 'sort_by_revenue'
        reports = reports.sort_by {|v| v[9]}.reverse
      elsif params[:sort_by] == 'sort_by_sales'
        reports = reports.sort_by {|v| v[8]}.reverse
      elsif params[:sort_by] == 'sort_by_unique_visitors'
        reports = reports.sort_by {|v| v[7]}.reverse
      elsif params[:sort_by] == 'sort_by_visitors'
        reports = reports.sort_by {|v| v[6]}.reverse
      elsif params[:sort_by] == 'sort_by_clicks'
        reports = reports.sort_by {|v| v[5]}.reverse
      elsif params[:sort_by] == 'sort_by_impressions'
        reports = reports.sort_by {|v| v[4]}.reverse
      end
    else
      reports = reports.sort_by {|v| v[8]}.reverse
    end
  end

  def set_collection_convertion_report(group_by, convertion_reports, report_for)
    if group_by.present?
      if params[:group_by] == 'Publisher'
        convertion_report_group_by = convertion_reports.group_by{|r| r.publisher_id}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.publisher_id, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, 'publisher') : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, 'am') : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, 'company') : 0}.reduce(:+)] }
      elsif params[:group_by] == 'Campaign'
        convertion_report_group_by = convertion_reports.group_by{|r| r.campaign_id}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.campaign_id, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      elsif params[:group_by] == 'Offer'
        convertion_report_group_by = convertion_reports.group_by{|r| r.category_campaign_id}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.category_campaign_id, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      elsif group_by == 'IP Address'
        convertion_report_group_by = convertion_reports.group_by{|r| r.ip}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.ip, r.last.country_code, r.count, r.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      elsif group_by == 'Country'
        convertion_report_group_by = convertion_reports.group_by{|r| r.country_code}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.country_code, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      elsif group_by == 'Referrer'
        convertion_report_group_by = convertion_reports.group_by{|r| r.referrer}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.referrer, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      elsif group_by == 'Date'
        convertion_report_group_by = convertion_reports.group_by{|r| r.convertion_date}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.convertion_date, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      elsif group_by == 'Device'
        convertion_report_group_by = convertion_reports.group_by{|r| r.device}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.device, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      elsif group_by == 'Browser'
        convertion_report_group_by = convertion_reports.group_by{|r| r.browser}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.browser, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      elsif group_by == 'Source'
        convertion_report_group_by = convertion_reports.group_by{|r| r.source}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.source, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      elsif group_by == 'Sub1'
        convertion_report_group_by = convertion_reports.group_by{|r| r.sub1}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.sub1, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      elsif group_by == 'Sub2'
        convertion_report_group_by = convertion_reports.group_by{|r| r.sub2}
        @convertion_report_group_by = convertion_report_group_by.values.map{ |r| [r.last.sub2, r.count, r.count, r.group_by{|r| r.ip}.count, r.map{|s| s.status == 'approved' ? s.sales : 0}.reduce(:+), r.map{|s| s.status == 'approved' ? condition_revenue(s, report_for) : 0}.reduce(:+)] }
      end

      if params[:sort_by].present?
        if params[:sort_by] == 'sort_by_revenue'
          @convertion_report_group_by = @convertion_report_group_by.sort_by {|v| v[5]}.reverse
        elsif params[:sort_by] == 'sort_by_sales'
          @convertion_report_group_by = @convertion_report_group_by.sort_by {|v| v[4]}.reverse
        elsif params[:sort_by] == 'sort_by_unique_visitors'
          @convertion_report_group_by = @convertion_report_group_by.sort_by {|v| v[3]}.reverse
        elsif params[:sort_by] == 'sort_by_visitors'
          @convertion_report_group_by = @convertion_report_group_by.sort_by {|v| v[2]}.reverse
        elsif params[:sort_by] == 'sort_by_clicks'
          @convertion_report_group_by = @convertion_report_group_by.sort_by {|v| v[1]}.reverse
        end
      end
      @convertion_reports = Kaminari.paginate_array(@convertion_report_group_by).page(params[:page_report_convertion]).per(25)
    else
      # unless params[:status] == "visited"
      #   @total_convertion_reports = convertion_reports.have_sales_only.latest
      # end
      @convertion_reports = convertion_reports.latest.page(params[:page_report_convertion]).per(25)
    end
    # @convertion_reports_list = convertion_reports.have_sales_only
  end

  def multipe_chart(collection)
    collection = collection.have_sales_only if params[:status].blank?
    @offer_chart = collection.group_by{|r| r.category_campaign_id}.values.map{|r| [CategoryCampaign.find(r.last.category_campaign_id).name, r.count]}
    @geo_chart = collection.group_by{|r| r.country_name}.values.map{|r| [r.last.country_name, r.count]}
    # @device_chart = collection.group_by{|r| r.device}.values.map{|r| [r.last.device, r.count]}
    # @browser_chart = collection.group_by{|r| r.browser}.values.map{|r| [r.last.browser, r.count]}
  end

  def condition_revenue(s, report_for)
    if report_for == "publisher"
      return s.fee_publisher
    elsif report_for == "am"
      return s.fee_am
    elsif report_for == "company"
      return s.fee_company
    else
      return s.revenue
    end
  end

  def query_last_next_payment_commisions(commisions)
    prepare_date
    if @_beginning_of_month <= @date && @_half_end_of_month >= @date
      commisions = commisions.where(:campaign_date.gte => @_beginning_of_month, :campaign_date.lte => @_half_end_of_month, status: "approved")
      @next_payment_commision = commisions.sum(:fee_publisher)
      @next_payment_commision_am = commisions.sum(:fee_am)
      @next_payment_commision_all = commisions.sum(:revenue)
      @start_date_payment_commision = @_beginning_of_month

      @last_payment_commision_start_at = @_last_month.beginning_of_month + 15.days
      @last_payment_commision_end_at = @_last_month.end_of_month
    elsif @_beginning_half_of_month <= @date && @_end_of_month >= @date
      commisions = commisions.where(:campaign_date.gte => @_beginning_half_of_month, :campaign_date.lte => @_end_of_month, status: "approved")
      @next_payment_commision = commisions.sum(:fee_publisher)
      @next_payment_commision_am = commisions.sum(:fee_am)
      @next_payment_commision_all = commisions.sum(:revenue)
      @start_date_payment_commision = @_beginning_half_of_month

      @last_payment_commision_start_at = @date.beginning_of_month
      @last_payment_commision_end_at = @last_payment_commision_start_at + 14.days
    end

    commisions = commisions.where(:campaign_date.gte => @last_payment_commision_start_at, :campaign_date.lte => @last_payment_commision_end_at, status: "approved")
    @last_payment_commision = commisions.sum(:fee_publisher) rescue 0
    @last_payment_commision_am = commisions.sum(:fee_am) rescue 0
    @last_payment_commision_all = commisions.sum(:revenue) rescue 0
  end
end









