module TrackHelper
  def track_visit
    headers['Access-Control-Allow-Origin'] = '*'
    # Allow GET and OPTIONS requests from any origin
    headers['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'Authorization'
    
    if params.present? && params['record'].present?
      today = Time.zone.now
      record = Base64.decode64(params['record'])
      # record = record.split('&').map{|r| r.split('=')[1]}
      # pub_id = record[0].split('-')[1]
      # campaign_id = record[1]
      # click_id = record[2]
      # link_referrer = record[3]
      record = record.split('&')
      pub_id = record.select{|r| r.include?('s1=')}[0].split('-')[1]
      campaign_id = record.select{|r| r.include?('s2=')}[0].split('s2=')[1]
      click_id = record.select{|r| r.include?('s3=')}[0].split('s3=')[1]
      link_referrer = record.select{|r| r.include?('s4=')}[0].split('s4=')[1]
      campaign = Campaign.active.find(campaign_id)
      referrer = campaign.tracking_code.include?('/?') ? campaign.tracking_code : campaign.tracking_code.gsub('?', '/?')

      params[:source] = params[:source].present? ? params[:source] : nil
      params[:sub1] = params[:sub1].present? ? params[:sub1] : nil
      params[:sub2] = params[:sub2].present? ? params[:sub2] : nil

      if campaign.present? && campaign.publisher_id == pub_id.to_i
        client = DeviceDetector.new(request.user_agent)
        @convertion = ConvertionReport.new(
                        click_id: click_id,
                        category_campaign_id: campaign.category_campaign_id,
                        campaign_id: campaign_id,
                        publisher_id: pub_id,
                        account_manager_id: campaign.account_manager_id,
                        admin_id: campaign.admin_id,
                        ip: request.ip.present? ? request.ip : '',
                        country_code: request.location.present? ? request.location.country_code : '',
                        country_name: request.location.present? ? request.location.country : '',
                        region_code: '',
                        region_name: '',
                        city: request.location.present? ? request.location.city : 'not-detected',
                        latitude: request.location.present? ? request.location.latitude : 'not-detected',
                        longitude: request.location.present? ? request.location.longitude : 'not-detected',
                        referrer: link_referrer,
                        sales: 0,
                        revenue: 0,
                        convertion_date: today.to_date,
                        convertion_time: today,
                        device: client.device_type,
                        browser: client.name,
                        provider: '',
                        os: "#{client.os_name} #{client.os_full_version}",
                        user_agent: request.location.present? ? request.user_agent : 'not-detected',
                        source: params[:source],
                        sub1: params[:sub1],
                        sub2: params[:sub2],
                        status: ConvertionReport::VISITED
                      )
  
        if @convertion.present?
          today_report = ReportCampaignSnapshot.find_by(campaign_id: campaign_id, :campaign_date.gte => today.to_date, publisher_id: pub_id) rescue nil
          today_report = today_report.blank? ? ReportCampaignSnapshot.new : today_report
          @convertion.report_campaign_snapshot_id = today_report.id
          if @convertion.save
            today_report.campaign_id = campaign_id
            today_report.admin_id = campaign.admin_id
            today_report.account_manager_id = campaign.account_manager_id
            today_report.publisher_id = pub_id
            today_report.campaign_date = today.to_date
            today_report.impressions = 0
            today_report.clicks = today_report.clicks + 1
            today_report.visitors = today_report.visitors + 1
            today_report.unique_visitors = today_report.convertion_reports.group_by{|r| r.ip}.size rescue 0
            today_report.save
          end
        end
      end
    end
  end
end