module UsersHelper
  def check_profile
    if user_signed_in?
      if current_user.is_publisher?
        unless @publisher.profile.first_name.present?
          # redirect_to edit_publishers_profile_path(current_user), alert: 'Please complete your profile first!'
          flash.now[:alert] = 'Please complete your profile first!'
        end
      end
    else
      redirect_to root_path, alert: 'Your session has expired!'
    end
  end
end