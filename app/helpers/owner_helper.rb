module OwnerHelper
  def prepare_date_owner
    @date = Time.zone.now.to_date
    @_last_month = (@date.beginning_of_month - 1.month)
    @_last_month_end = @_last_month.end_of_month

    @_beginning_of_month = @date.beginning_of_month
    @_end_of_month = @date.end_of_month
  end

  def query_last_next_payment_owner(user, reports)
    @_next_payment = reports.where(:campaign_date.gte => @_beginning_of_month, :campaign_date.lte => @_end_of_month)
    @_next_payment = @_next_payment.sum(:fee_company) * user.fee

    @_last_payment = reports.where(:campaign_date.gte => @_last_month, :campaign_date.lte => @_last_month_end)
    @_last_payment = @_last_payment.sum(:fee_company) * user.fee
  end

  def get_payment_owner(type, user)
    reports = user.report_campaign_snapshots
    prepare_date_owner
    query_last_next_payment_owner(user, reports)

    if type == 'last_payment'
      return @_last_payment
    elsif type == 'ongoing_payment'
      return @_next_payment
    end
  end

  def next_last_payment_revenue(user, reports)
    if user.present? && reports.present?
      query_last_next_payment_owner(user, reports)
    end
  end

  def get_analytic_report_owner(reports, params, user, report_for)
    @start_date = params[:start_at]
    @end_date = params[:end_at]
    prepare_date_owner

    if reports.present?
      next_last_payment_revenue(user, reports)
      @earning_today_so_far = reports.where(campaign_date: @date).sum(:fee_company) * user.fee
      @earning_yesterday = reports.where(campaign_date: @date.yesterday).sum(:fee_company) * user.fee
      @earning_last_7_month = reports.where(:campaign_date.gte => @date - 6.days, :campaign_date.lte => @date).sum(:fee_company) * user.fee
      @earning_this_month = reports.where(:campaign_date.gte => @date.beginning_of_month, :campaign_date.lte => @date.end_of_month).sum(:fee_company) * user.fee

      if @start_date.present? && @end_date.present?
        @reports = reports.where(:campaign_date.gte => @start_date, :campaign_date.lte => @end_date)
      elsif @start_date.present? && @end_date.blank?
        @reports = reports.where(campaign_date: @start_date)
      elsif @start_date.blank? && @end_date.present?
        @reports = reports.where(campaign_date: @end_date)
      elsif @start_date.blank? && @end_date.blank?
        @reports = reports.where(campaign_date: @date)
      else
        @reports = reports.where(campaign_date: @date)
      end

      @report_campaign_snapshots = @reports.latest.page(params[:page_report_campaign_snapshots]).per(per_page)
      @reports_collection = @reports.group_by {|r| r.publisher_id}

      @daily_reports_group = @reports.order_by(created_at: 'asc').group_by {|d| d.campaign_date} if @reports.present?
      @monthly_reports_group = @reports.order_by(created_at: 'asc').group_by {|d| d.campaign_date.strftime("%B %Y")} if @reports.present?
      @yearly_reports_group = @reports.order_by(created_at: 'asc').group_by {|d| d.campaign_date.strftime("%Y")} if @reports.present?

      @reports_total_revenue = @reports.sum(:fee_company) * user.fee
    end
  end
end