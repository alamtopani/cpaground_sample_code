class SubscribersController < ApplicationController
  before_action :class_name
  
  def create
    @subscriber = Subscriber.where(email: params[:subscriber][:email]).first
    if @subscriber.blank?
      @subscriber = Subscriber.new params_permit
      if @subscriber.save
        UserMailer.after_subscribe(@subscriber).deliver_now!
        create_activity(@subscriber, nil, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} by #{@subscriber.email}")
        redirect_to root_path, notice: "Congratulations you have registered your email as our subscriber, we will send the latest news every day to your email!"
      else
        redirect_to root_path, alert: "Unsuccessfully created #{@resource_name}, #{@subscriber.errors[:name].first}"
      end
    else
      if @subscriber.is_active?
        redirect_to root_path, alert: "Unsuccessfully created #{@resource_name}, email has already used."
      else
        @subscriber.status = Subscriber::ACTIVE
        @subscriber.save
        UserMailer.after_subscribe(@subscriber).deliver_now!
        create_activity(@subscriber, nil, "Active #{@resource_name.downcase}", "successfully activated #{@resource_name.downcase} by #{@subscriber.email}")
        redirect_to root_path, notice: "Successfully created #{@resource_name}"
      end
    end
  end

  def subscribe
    @subscriber = Subscriber.where(email: params[:email]).first
    @subscriber = Subscriber.new if @subscriber.blank?
    @subscriber.user_id = current_user.id
    @subscriber.email = current_user.email
    @subscriber.subscriber_type = current_user.type
    @subscriber.status = Subscriber::ACTIVE
    if @subscriber.save
      UserMailer.after_subscribe(@subscriber).deliver_now!
      create_activity(@subscriber, @subscriber.user_id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} by #{@subscriber.email}")
      redirect_to :back, notice: "Successfully created #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully created #{@resource_name}, #{@subscriber.errors[:name].first}"
    end
  end

  def unsubscribe
    @subscriber = Subscriber.where(email: params[:email]).first
    @subscriber.status = Subscriber::INACTIVE
    if @subscriber.save
      UserMailer.after_unsubscribe(@subscriber).deliver_now!
      create_activity(@subscriber, @subscriber.user_id, "Unsubscribe", "successfully Unsubscribe by #{@subscriber.email}")
      redirect_back(fallback_location: root_path, notice: "Successfully Unsubscribe.")
    else
      redirect_back(fallback_location: root_path, alert: "Unsuccessfully Unsubscribe, #{@subscriber.errors[:name].first}")
    end
  end

  private

    def params_permit
      params.require(:subscriber).permit(:email)
    end

    def class_name
      @resource_name = 'Subscriber'
      @collection_name = 'Subscribers'
    end
end