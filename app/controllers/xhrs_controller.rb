class XhrsController < ApplicationController
  def get_offer_cr
    reports = ReportCampaignSnapshot.where(:sales.gte => 1).where(:campaign_id.in => Campaign.where(:status.in => [Campaign::ACTIVE, Campaign::COMPLETE]).pluck(:id))
    @list_offer_count = set_collection_offer(reports.group_by {|r| r.category_campaign.id}) if reports.present?

    render layout: false
  end

  def get_campaign_date_form
    @convertion = ConvertionReport.find_by(click_id: params[:id])
    revenue_targeting = @convertion.category_campaign.targetings.where(country_code: @convertion.country_code).last
    revenue_targeting = @convertion.category_campaign.targetings.where(country_code: "ALL COUNTRY").last if revenue_targeting.blank?
    if revenue_targeting.present?
      @convertion.revenue = revenue_targeting.payout_price if @convertion.revenue <= 0
    end
    render layout: false
  end

  def get_convertion_report
    @convertion = ConvertionReport.find_by(click_id: params[:id])
    revenue_targeting = @convertion.category_campaign.targetings.where(country_code: @convertion.country_code).last
    revenue_targeting = @convertion.category_campaign.targetings.where(country_code: "ALL COUNTRY").last if revenue_targeting.blank?
    if revenue_targeting.present?
      @convertion.revenue = revenue_targeting.payout_price if @convertion.revenue <= 0
    end
    render layout: false
  end

  def change_convertion_report
    @convertion = ConvertionReport.find_by(click_id: params[:id])
    revenue_targeting = @convertion.category_campaign.targetings.where(country_code: @convertion.country_code).last
    revenue_targeting = @convertion.category_campaign.targetings.where(country_code: "ALL COUNTRY").last if revenue_targeting.blank?
    if revenue_targeting.present?
      @convertion.revenue = revenue_targeting.payout_price if @convertion.revenue <= 0
    end
    render layout: false
  end

  def get_campaign
    @publisher = Publisher.find(current_user.id)
    category_campaign_ids = CategoryCampaign.where(category_id: params[:id]).pluck(:id)
    # active_request_lp_ids = @publisher.request_landing_pages.where(status: { :$in => [RequestLandingPage::ACTIVE, RequestLandingPage::INACTIVE] }).pluck(:campaign_id)

    @campaigns_data = @publisher.campaigns.where(category_campaign_id: { :$in => category_campaign_ids }).active.latest if category_campaign_ids.present?

    if @campaigns_data.present?
      @campaigns_data = @campaigns_data #.where(:id.nin => active_request_lp_ids)
    end
    
    @campaign_id = params[:node] if params[:node].present?
    @layouts = LayoutTheme.active.where(category_id: params[:id]).oldest
    render layout: false
  end

  def cities
    province = Region.find_by(name: params[:id])
    @cities = province ? province.children.pluck(:name) : Region.none
    render layout: false
  end

  def get_box_report
    @campaign = Campaign.find_by(id: params[:id])
    render layout: false
  end

  def get_balances
    publisher = Publisher.find params[:id]
    @balance = publisher.balance
    render layout: false
  end

  def get_revenues
    node = params[:node].split(" ")
    user_id = node[0].to_i
    payment_at = node[1]
    payment_end = params[:id]
    user = User.find user_id
    if user.is_publisher?
      reports = ReportCampaignSnapshot.where(publisher_id: user_id, :campaign_date.gte => payment_at, :campaign_date.lte => payment_end).have_sales_only
      @to_acc = Publisher.find(user_id).profile.payment_to
    elsif user.is_account_manager?
      reports = ReportCampaignSnapshot.where(account_manager_id: user_id, :campaign_date.gte => payment_at, :campaign_date.lte => payment_end).have_sales_only
      @to_acc = AccountManager.find(user_id).profile_account_manager.payment_to
    end
    @status_account = User.find(user_id).status?
    @total_revenue = '%.4f' % reports.sum(:revenue)
    @revenue_publisher = '%.4f' %  reports.sum(:fee_publisher)
    @fee_am = '%.4f' %  reports.sum(:fee_am)
    @fee_company = '%.4f' %  reports.sum(:fee_company)

    render layout: false
  end

  def get_commisions
    node = params[:node].split(" ")
    user_id = node[0].to_i
    payment_at = node[1]
    payment_end = params[:id]
    user = User.find user_id
    reports = Commision.where(publisher_id: user_id, :campaign_date.gte => payment_at, :campaign_date.lte => payment_end, status: "approved")
    @to_acc = Publisher.find(user_id).profile.payment_to
    @total_revenue = '%.4f' % reports.sum(:revenue)
    @revenue_publisher = '%.4f' %  reports.sum(:fee_publisher)
    @fee_am = '%.4f' %  reports.sum(:fee_am)

    render layout: false
  end

  def get_payment_owners
    node = params[:node].split(" ")
    owner_id = node[0].to_i
    payment_at = node[1]
    payment_end = params[:id]

    owner = Owner.find owner_id
    reports = owner.report_campaign_snapshots.where(:campaign_date.gte => payment_at, :campaign_date.lte => payment_end)
    
    @total_fee_company = reports.sum(:fee_company)
    @price = '%.4f' % (@total_fee_company.to_f * owner.fee)

    render layout: false
  end

  private
    def set_collection_offer(offers)
      offers.values.map{|o| [o.map(&:campaign_id).last,
                            o.map(&:sales).reduce(:+),
                            o.map(&:visitors).reduce(:+), 
                            o.map(&:unique_visitors).reduce(:+),
                            o.map(&:revenue).reduce(:+)] 
                          }.sort_by { |o| o[1] }.reverse
    end

end
