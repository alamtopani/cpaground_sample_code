class HomeController < ApplicationController
	# layout "track", only: :index
	layout "track", only: [:under_maintenance, :not_found_page]

	def health_check
		render plain: "I'm healthy!"
	end
	
	def index
		@slider_home = ['slider.jpg', 'slider2.jpg', 'slider3.jpg', 'slider5.jpg'].shuffle.first

		# if params[:c].present?
		# 	c = Base64.decode64(params[:c]).gsub(/\s+/, "")
		# 	c = c.split('&').map{|r| r.split('=')[1]}
		# 	offer = CategoryCampaign.find c[0] if c[0].present?
		# 	if offer.present? && c[0].present? && c[1].present? && c[2].present?
		# 		random = generate_code(30)
		# 		referer = request.referer || 'not-detected'
		# 		record = "s1="+c[1]+"&s2="+c[2]+"&s3="+random+"&s4="+referer
		# 		var1 = c[1]+'-'+c[2]
		# 		affiliate_url = offer.category_id == '6' ? offer.link_offer+'?s1='+var1+'&s4='+random : offer.link_offer+'?s3='+var1+'&s4='+random
		# 		params[:record] = Base64.encode64(record)
		# 		helpers.track_visit
		# 		redirect_to affiliate_url
		# 	else
		# 		redirect_to "https://www.cpaground.com"
		# 	end
		# else
		# 	redirect_to "https://www.cpaground.com"
		# end

    # if params[:c].present?
    #   random = generate_code(30)
    #   referer = request.referer || 'not-detected'
    #   c = Base64.decode64(params[:c]).gsub(/\s+/, "").split('&').map{|r| r.split('=')[1]}
    #   offer = CategoryCampaign.find c[0] if c[0].present?
    #   if offer.present? && c[0].present? && c[1].present? && c[2].present?
    #     var1 = c[1]+'-'+c[2]
    #     record = "s1="+c[1]+"&s2="+c[2]+"&s3="+random+"&s4="+referer
    #     @record = Base64.encode64(record).gsub(/\s+/, "")
    #     @affiliate_url = offer.category_id == '6' ? offer.link_offer+'?s1='+var1+'&s4='+random : offer.link_offer+'?s3='+var1+'&s4='+random
    #     @affiliate_url = @affiliate_url.gsub(/\s+/, "")

    #     @visit_url = "#{CategoryCampaign::DOMAIN_LINK_BASIC}/api/lp/track_visit.json/?record=#{@record}"
    #     if params[:source].present?
    #       @visit_url = @visit_url+"&source=#{params[:source]}"
    #     end
    #     if params[:sub1].present?
    #       @visit_url = @visit_url+"&sub1=#{params[:sub1]}"
    #     end
    #     if params[:sub2].present?
    #       @visit_url = @visit_url+"&sub2=#{params[:sub2]}"
    #     end
        
    #   else
    #     redirect_to not_found_page_url
    #   end
    # else
    #   redirect_to not_found_page_url
    # end
	end

	def thank_you
		
	end

	def waiting_verified
		
	end

	def under_maintenance
		
	end

  def not_found_page
    
  end

  def lombok
    render layout: "donation" 
  end

	private
		def generate_code(number)
			charset = Array('0'..'9') + Array('a'..'z')
			Array.new(number) { charset.sample }.join
		end
end