class ConfirmationsController < Devise::ConfirmationsController
  private
  def after_confirmation_path_for(resource_name, resource)
    # root_path
    waiting_verified_path(resource)
  end
end