class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include DeviseHelper
  include UsersHelper
  include ReportHelper
  include DataHelper
  include TrackHelper
  include UserModule::DeviseFilter
  before_action :prepare_setting

  private
    def prepare_setting
      @web_setting = WebSetting.first
    end

    def launching_v2
      @launching_v2 = '2017/12/10'.to_date  
    end

    def per_page
      params[:per_page] ||= 25
    end

    def page
      params[:page] ||= 1
    end

    def create_activity(resource, user_id, name, description)
      Activity.create(user_id: user_id, name: name, description: description, activitiable_id: resource.id, activitiable_type: resource.class.name)
      # create_activity(@campaign, current_user.id, "new campaign", "successfully save new campaign")
    end

    def create_activity_report(resource, user_id, name, description, report)
      Activity.create(user_id: user_id, name: name, description: description, activitiable_id: resource.id, activitiable_type: report.class.name)
    end

    def create_notification(resource, user_id, title, message)
      Notification.create(user_id: user_id, title: title, message: message, notification_id: resource.id, notification_type: resource.class.name)
    end
end
