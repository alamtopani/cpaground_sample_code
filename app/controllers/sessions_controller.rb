class SessionsController < Devise::SessionsController
  def create
    if current_user.present? && current_user.is_publisher? && current_user.verified == false
      reset_session
      redirect_to root_path, alert: "Your account will be activated after the verification process of our team, please wait for further information from us!"
    else
      self.resource = warden.authenticate!(auth_options)
      set_flash_message(:notice, :signed_in) if is_flashing_format?
      sign_in(resource_name, resource)
      yield resource if block_given?
      if request.xhr?
        render :json => {:success => true}
      else
        respond_with resource, location: after_sign_in_path_for(resource)
      end
    end
  end
end
