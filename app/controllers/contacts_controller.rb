class ContactsController < ApplicationController
  before_action :class_name
  def index
    
  end
  def create
    @contact = Contact.new params_permit
    if verify_recaptcha(model: @contact) && @contact.save
      UserMailer.after_send_contact(@contact).deliver_now!
      create_activity(@contact, @contact.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} by #{@contact.name}")
      redirect_to root_path, notice: "Your message has been successfully sent, we will immediately reply by your email, thank you!"
    else
      redirect_to root_path, alert: "#{@contact.errors.full_messages.join("<br>")}"
    end
  end

  private

    def params_permit
      params.require(:contact).permit(:name, :email, :phone_code, :phone, :website, :message, :status)
    end

    def class_name
      @resource_name = 'Contact'
      @collection_name = 'Contacts'
    end
end