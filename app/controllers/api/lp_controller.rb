class Api::LpController < ApplicationController
	def show
		# @lp = RequestLandingPage.find_by(code: params[:id])
		data_hash = JSON.parse(File.read("#{Rails.root}/public/get_lp.json"))
		@lp = (data_hash.find { |u| u['id'] == params[:id] })
	end

  def get_campaigns
    @landing_pages = RequestLandingPage.all
  end

	def track_visit
    helpers.track_visit
	end

	def postback
		@convertion = ConvertionReport.find_by(click_id: params[:click_id])
    if @convertion.category_campaign.action == 'CPS'
    	if params[:click_id].present? && @convertion.present? && params[:payout].present?
    		revenue = params[:payout].to_f
    		target = @convertion.category_campaign.targetings.latest.last
				if @convertion.revenue <= 0 && @convertion.sales <= 0 && @convertion.status == ConvertionReport::VISITED && revenue >= 0
					@convertion.sales = 0
					@convertion.status = ConvertionReport::PENDING
					@convertion.revenue = revenue
	        @convertion.fee_publisher = (target.fee_publisher.to_f / 100) * revenue
	        @convertion.fee_am = (target.fee_am.to_f / 100) * revenue
	        @convertion.fee_company = (target.fee_company.to_f / 100) * revenue
	        update_commision(@convertion)
					@convertion.save
				end
			end
    else
    	revenue = @convertion.category_campaign.targetings.where(country_code: params[:country_code]).last
    	revenue = @convertion.category_campaign.targetings.where(country_code: "ALL COUNTRY").last if revenue.blank?
			if params[:click_id].present? && params[:country_code].present? && @convertion.present? && revenue.present?
				if @convertion.revenue <= 0 && @convertion.sales <= 0 && @convertion.status == ConvertionReport::VISITED
					@convertion.sales = 1
					@convertion.status = ConvertionReport::APPROVED
					@convertion.revenue = revenue.payout_price
	        @convertion.fee_publisher = (revenue.fee_publisher.to_f / 100) * @convertion.revenue
	        @convertion.fee_am = (revenue.fee_am.to_f / 100) * @convertion.revenue
	        @convertion.fee_company = (revenue.fee_company.to_f / 100) * @convertion.revenue
	        update_commision(@convertion)
					if @convertion.save
						today_report = @convertion.report_campaign_snapshot
						today_report.sales = today_report.sales + 1
	          today_report.revenue = today_report.revenue + @convertion.revenue
	          today_report.fee_publisher = today_report.fee_publisher + @convertion.fee_publisher
	          today_report.fee_am = today_report.fee_am + @convertion.fee_am
						today_report.fee_company = today_report.fee_company + @convertion.fee_company
						today_report.save
						update_offer_sales(@convertion.category_campaign)
					end
				end
			end
		end
	end

	private
		def update_offer_sales(offer)
      offer = offer
      offer.sales = offer.sales + 1
      offer.save
    end

    def update_commision(convertion)
    	@convertion = convertion
    	referral_publisher_top = @convertion.publisher.referral_publisher_top
			if referral_publisher_top.present?
				commision_percent = referral_publisher_top.permission_referral_publisher.commision
				commision = Commision.find_by(convertion_report_id: @convertion.id)
				commision = commision.blank? ? Commision.new : commision
				commision.convertion_report_id = @convertion.id
				commision.account_manager_id = @convertion.account_manager_id
				commision.publisher_id = referral_publisher_top.id
				commision.referral_publisher_id = @convertion.publisher_id
				commision.report_campaign_snapshot_id = @convertion.report_campaign_snapshot_id
				commision.category_campaign_id = @convertion.category_campaign_id
				commision.campaign_id = @convertion.campaign_id
				commision.campaign_date = @convertion.report_campaign_snapshot.campaign_date
				commision.revenue = @convertion.fee_am
				commision.fee_publisher = (commision_percent.to_f / 100) * commision.revenue
				commision.fee_am = commision.revenue - commision.fee_publisher
				commision.status = @convertion.status
				commision.save
				@convertion.fee_am = commision.fee_am
				@convertion.save
			end
    	
    end
end