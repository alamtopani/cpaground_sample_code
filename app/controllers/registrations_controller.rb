class RegistrationsController < Devise::RegistrationsController
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, 
        keys: [
          :uid,
          :username,
          :email,
          :password,
          :password_confirmation,
          :type,
          :slug,
          :phone_code,
          :phone,
          :skype,
          :facebook_url,
          :referral,
          :id_card, 
          :foto_id_card, 
          :selfi_id_card
        ]
      )

      devise_parameter_sanitizer.permit(:sign_in, 
        keys: [
          :login, :username, :email, :password, :remember_me, :type, :slug
        ]
      )
    end

    def after_inactive_sign_up_path_for(resource)
      publisher = User.find resource
      publisher.type = User::PUBLISHER
      if cookies[:referral].present?
        publisher_token = Publisher.where(referral_token: cookies[:referral], referral_status: "active").last
        if publisher_token.present?
          account_manager = publisher_token.account_manager
          publisher.referral = "referral #{publisher_token.username}"
          publisher.referral_publisher = cookies[:referral]
        else
          account_manager = AccountManager.where(username: cookies[:referral]).last
          publisher.referral = account_manager.present? ? cookies[:referral] : "public"
        end
        publisher.account_manager_id = account_manager.id if account_manager.present?
      else
        publisher.referral = "public"
      end
      publisher.save

      profile = Profile.new
      profile.publisher_id = publisher.id
      profile.save

      cookies.delete :referral
      thank_you_path(resource)
    end
end
