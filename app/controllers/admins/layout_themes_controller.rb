class Admins::LayoutThemesController < Admins::ApplicationController
  before_action :class_name
  before_action :resource, only: [:show, :edit, :update, :destroy]

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "layout themes", :admins_layout_themes_path
  
  def index
    @layout_themes = collection.page(page).per(per_page)
    @collection_size = "(#{@layout_themes.total_count})"

    respond_to do |format|
      format.html
      format.xls
      format.js
    end 
  end

  def show
    add_breadcrumb "#{params[:action]}"
    @campaigns = @layout_theme.campaigns.latest.search_by(params).page(params[:list_campaigns]).per(per_page)
  end

  def new
    @layout_theme = LayoutTheme.new
  end

  def create
    @layout_theme = LayoutTheme.new params_permit
    unless @layout_theme.link_preview.include? "//"
      @layout_theme.link_preview = "http://"+@layout_theme.link_preview
    end
    if @layout_theme.save
      @layout_theme.blast_email("Available new landing page #{@layout_theme.title} for category offer #{@layout_theme.category_name}", "Available new landing page #{@layout_theme.title} for category offer #{@layout_theme.category_name}, you can see detail landing page below:", "Please check in request landing pages.") if @layout_theme.status == "active"
      redirect_to admins_layout_theme_path(@layout_theme), notice: "Successfully created #{@resource_name}"
    else
      redirect_to admins_layout_themes_path, alert: "Unsuccessfully created #{@resource_name}, #{@layout_theme.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
  end

  def update
    if @layout_theme.update params_permit
      unless @layout_theme.link_preview.include? "//"
        @layout_theme.link_preview = "http://"+@layout_theme.link_preview
        @layout_theme.save
      end
      redirect_to admins_layout_theme_path(@layout_theme), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@layout_theme.errors.full_messages.join(',')}"
    end
  end

  def destroy
    if @layout_theme.destroy
      redirect_to admins_layout_themes_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to admins_layout_themes_path, alert: "Unsuccessfully delete #{@resource_name}, #{@layout_theme.errors.full_messages.join(',')}"
    end
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      if params[:commit] == LayoutTheme::ACTIVE
        LayoutTheme.where(:id.in => collect).each do |resource|
          resource.status = LayoutTheme::ACTIVE
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
        end
      elsif params[:commit] == LayoutTheme::INACTIVE
        LayoutTheme.where(:id.in => collect).each do |resource|
          resource.status = LayoutTheme::INACTIVE
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
        end
      elsif params[:commit] == LayoutTheme::PROMOTE
        LayoutTheme.where(:id.in => collect).each do |resource|
          resource.blast_email("Promote landing page #{resource.title} for category offer #{resource.category_name}", "We promote landing page #{resource.title} for category offer #{resource.category_name}, you can see detail landing page below:", "Please check in request landing pages.") if resource.status == "active"
          create_activity(resource, current_user.id, "promote landing page", "successfully promoted landing page #{resource.title}.")
        end
      end
      redirect_to admins_layout_themes_path, notice: 'Status updated!'
    else
      redirect_to admins_layout_themes_path, alert: 'Nothing checked!'
    end 
  end

  def change_status
    resource
    @layout_theme.status = params[:layout_theme][:status]
    if @layout_theme.save
      create_activity(@layout_theme, current_user.id, "change status #{@resource_name.downcase} to #{@layout_theme.status}", "successfully changed #{@resource_name.downcase} #{@layout_theme.title}")
      redirect_to admins_layout_theme_path(@layout_theme), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@layout_theme.errors.full_messages.join(',')}"
    end
  end

  private
    def resource
      @layout_theme = LayoutTheme.find params[:id]
    end

    def collection
      @layout_themes = LayoutTheme.latest.search_by(params)
    end

    def params_permit
      params.require(:layout_theme).permit(:category_id, :image, :title, :link_preview, :status)
    end

    def class_name
      @resource_name = 'Layout Theme'
      @collection_name = 'Layout Themes'
    end
  
end