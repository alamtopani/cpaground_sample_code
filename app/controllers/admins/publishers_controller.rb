class Admins::PublishersController < Admins::ApplicationController
	before_action :class_name
	prepend_before_filter :draw_password, only: :update

	add_breadcrumb "dashboard", :admins_dashboards_path
	add_breadcrumb "publishers", :admins_publishers_path

	def index
		collection
		@total_publishers = @publishers if params[:export] == 'true'
		@publishers = @publishers.page(page).per(per_page)
		@collection_size = "(#{@publishers.total_count})"

		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

	def show
		add_breadcrumb "#{params[:action]}"
		resource
		@profile = @publisher.profile
		@verification_snapshots = @publisher.verification_snapshots.latest.search_by(params).page(params[:page_verification_snapshots]).per(per_page)
		@campaigns = @publisher.campaigns.latest.search_by(params).page(params[:page_campaigns]).per(per_page)
		@activities = @publisher.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
		@payment_snapshots = @publisher.payment_snapshots.latest.search_by(params).page(params[:page_payment_snapshots]).per(per_page)
		@request_landing_pages = @publisher.request_landing_pages.latest.search_by(params).page(params[:page_landing_pages]).per(per_page)
		@referral_publishers = @publisher.referral_publishers.latest.search_by(params).page(params[:page_referral_publishers]).per(per_page)
		@commisions = @publisher.commisions.latest.search_by(params).page(params[:page_commisions]).per(per_page)
		@payment_commisions = @publisher.payment_commisions.latest.search_by(params).page(params[:page_payment_commisions]).per(per_page)

		reports = ReportCampaignSnapshot.where(publisher_id: @publisher.id)
		next_last_payment_all(reports)

		query_last_next_payment_commisions(@publisher.commisions)

		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

	def new
		add_breadcrumb "#{params[:action]}"
		@publisher = Publisher.new
	end

	def create
		@publisher = Publisher.new params_permit
		if @publisher.save
			create_activity(@publisher, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@publisher.username}")
			redirect_to admins_publisher_path(@publisher), notice: "Successfully created #{@resource_name}"
		else
			redirect_to admins_publishers_path, alert: "Unsuccessfully created #{@resource_name}, #{@publisher.errors.full_messages.join(',')}"
		end
	end

	def edit
		add_breadcrumb "#{params[:action]}"
		resource
	end

	def update
		resource
		if @publisher.update params_permit 
			create_activity(@publisher, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} #{@publisher.username}")
			redirect_to admins_publisher_path(@publisher), notice: "Successfully updated #{@resource_name}"
		else
			redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@publisher.errors.full_messages.join(',')}"
		end
	end

	def destroy
		resource

		if @publisher.destroy
			redirect_to admins_publishers_path, notice: "Successfully deleted #{@resource_name}"
		else
			redirect_to admins_publishers_path, alert: "Unsuccessfully deleted #{@resource_name}, #{@publisher.errors.full_messages.join(',')}"
		end
	end

	def change_status
		resource
		check_verified = @publisher.verified
		check_account_manager = @publisher.account_manager
		@publisher.verified = params[:publisher][:verified]
		@publisher.account_manager_id = params[:publisher][:account_manager_id]
		@publisher.admin_id = params[:publisher][:admin_id] if params[:publisher][:admin_id].present?
		if @publisher.save
			if @publisher.verified == true && check_account_manager.present? && check_account_manager.id != @publisher.account_manager_id
				UserMailer.replacing_manager(@publisher, check_account_manager).deliver_now!
				send_message(@publisher, "Hi i'm #{@publisher.account_manager.profile_account_manager.full_name} replace #{check_account_manager.profile_account_manager.full_name} as your manager.")
				create_activity(@publisher, current_user.id, "replace manager", "#{current_user.username} has replace manager from #{check_account_manager.profile_account_manager.full_name} to #{@publisher.account_manager.profile_account_manager.full_name}.")
				create_activity(@publisher, @publisher.id, "replace manager", "#{current_user.username} has replace manager from #{check_account_manager.profile_account_manager.full_name} to #{@publisher.account_manager.profile_account_manager.full_name}.")
				update_manager_in_data(@publisher)
			end
			verification_snapshot(@publisher)
			UserMailer.publisher_approved(@publisher).deliver_now! if @publisher.verified == true && check_verified != true
			redirect_to admins_publisher_path(@publisher), notice: "Successfully updated #{@resource_name}"
		else
			redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@publisher.errors.full_messages.join(',')}"
		end
	end

	def detail_report
		add_breadcrumb "reports"
		resource
		get_analytic_report(@publisher, 'dashbord_admin_publisher')
		if @convertion_reports.present?
			set_collection_convertion_report(params[:group_by], @convertion_reports, "publisher")
			# multipe_chart(@convertion_reports_list)
		end
		
		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

	def list_email
		add_breadcrumb "list_email_publishers"
		@publishers = Publisher.latest.search_by(params)
	end

	private
		def resource
			if current_user.is_staff?
				@publisher = Publisher.where(admin_id: current_user.id).find_by(slug: params[:id])
			else
				@publisher = Publisher.find(params[:id])
			end
		end

		def collection
			if current_user.is_staff?
				@publishers = Publisher.where(admin_id: current_user.id).latest.search_by(params)
			else
				@publishers = Publisher.latest.search_by(params)
			end
		end

		def params_permit
			params.require(:publisher).permit(:username, :email, :skype, :avatar, :phone_code, :phone, :verified, :password, :password_confirmation, :account_manager_id, :admin_id, :facebook_url, :id_card, :foto_id_card, :selfi_id_card,
				profile_attributes:[:publisher_id, :first_name, :last_name, :country, :province, :city, :address, :zip_code, :choice_payment_type, :acc_paypal, :acc_payoneer, :acc_bank, :longitude, :latitude, :bank_name, :acc_name])
		end

		def class_name
			@resource_name = 'Publisher'
			@collection_name = 'Publishers'
		end

		def draw_password
			%w(password password_confirmation).each do |attr|
				params[:publisher].delete(attr)
			end if params[:publisher] && params[:publisher][:password].blank?
		end

		def verification_snapshot(publisher)
			verification_snapshot = VerificationSnapshot.new
			verification_snapshot.publisher_id = publisher.id
			verification_snapshot.admin_id = current_user.id
			verification_snapshot.account_manager_id = publisher.account_manager_id
			if publisher.is_verified?
				verification_snapshot.status = VerificationSnapshot::ACTIVE
			else
				verification_snapshot.status = VerificationSnapshot::INACTIVE
			end
			verification_snapshot.message = "#{current_user.username}(Admin) has change verification status to #{verification_snapshot.status}."
			verification_snapshot.save
		end

		def send_message(publisher, message)
			group_message_id = GroupMessage.where(account_manager_id: publisher.account_manager_id, publisher_id: publisher.id).first
			if group_message_id.blank?
				group_message_id = GroupMessage.create(account_manager_id: publisher.account_manager_id, publisher_id: publisher.id, status: 'active')
			end
			@message = Message.new
			@message.group_message_id = group_message_id
			@message.from = publisher.account_manager_id
			@message.to = publisher.id
			@message.message = message
			@message.save
		end

		def update_manager_in_data(publisher)
			publisher.access_offers.each do |resource|
				resource.account_manager_id = publisher.account_manager_id
				resource.save
			end

			publisher.campaigns.each do |resource|
				resource.account_manager_id = publisher.account_manager_id
				resource.save
			end

			publisher.request_landing_pages.each do |resource|
				resource.account_manager_id = publisher.account_manager_id
				resource.save
			end
		end
end