class Admins::RequestLandingPagesController < Admins::ApplicationController
  before_action :class_name
  before_action :resource, only: [:show, :edit, :update, :destroy]

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "request landing pages", :admins_request_landing_pages_path
  
  def index
    collection
    @request_landing_pages_size = @request_landing_pages if params[:export] == 'true'
    @request_landing_pages = @request_landing_pages.page(page).per(per_page)
    @collection_size = "(#{@request_landing_pages.total_count})"

    respond_to do |format|
      format.html
      format.xls
      format.js
    end 
  end

  def show
    add_breadcrumb "#{params[:action]}"
    @activities = @request_landing_page.activities.latest.search_by(params).page(page).per(per_page)
  end

  def new
    @request_landing_page = RequestLandingPage.new
  end

  def create
    @request_landing_page = RequestLandingPage.new params_permit
    if @request_landing_page.save
      redirect_to admins_request_landing_page_path(@request_landing_page), notice: "Successfully created #{@resource_name}"
    else
      redirect_to admins_request_landing_pages_path, alert: "Unsuccessfully created #{@resource_name}, #{@request_landing_page.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
  end

  def update
    if @request_landing_page.update params_permit
      redirect_to admins_request_landing_page_path(@request_landing_page), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@request_landing_page.errors.full_messages.join(',')}"
    end
  end

  def destroy
    if @request_landing_page.destroy
      redirect_to admins_request_landing_pages_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to admins_request_landing_pages_path, alert: "Unsuccessfully delete #{@resource_name}, #{@request_landing_page.errors.full_messages.join(',')}"
    end
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      if params[:commit] == RequestLandingPage::ACTIVE
        RequestLandingPage.where(:id.in => collect).each do |resource|
          resource.status = RequestLandingPage::ACTIVE
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.code}")
        end
      elsif params[:commit] == RequestLandingPage::INACTIVE
        RequestLandingPage.where(:id.in => collect).each do |resource|
          resource.status = RequestLandingPage::INACTIVE
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.code}")
        end
      elsif params[:commit] == RequestLandingPage::REJECT
        RequestLandingPage.where(:id.in => collect).each do |resource|
          resource.status = RequestLandingPage::REJECT
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.code}")
        end
      end
      redirect_to admins_request_landing_pages_path, notice: 'Status updated!'
    else
      redirect_to admins_request_landing_pages_path, alert: 'Nothing checked!'
    end 
  end

  def change_status
    resource
    @request_landing_page.status = params[:request_landing_page][:status]
    @request_landing_page.domain = params[:request_landing_page][:domain]
    @request_landing_page.track_script = params[:request_landing_page][:track_script]
    if @request_landing_page.save
      # UserMailer.after_review_request_lp(@request_landing_page).deliver_now!
      create_notification(@request_landing_page, @request_landing_page.publisher_id, "Your #{@resource_name.downcase} for #{@request_landing_page.campaign.title}", "Your #{@resource_name.downcase} for #{@request_landing_page.campaign.title} has #{@request_landing_page.status}.")
      create_activity(@request_landing_page, current_user.id, "change status #{@resource_name.downcase} to #{@request_landing_page.status}", "successfully changed #{@resource_name.downcase} #{@request_landing_page.code}")
      redirect_to admins_request_landing_page_path(@request_landing_page), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@request_landing_page.errors.full_messages.join(',')}"
    end
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to admins_request_landing_page_path(params[:id])
  end

  private
    def resource
      if current_user.is_staff?
        @request_landing_page = RequestLandingPage.where(admin_id: current_user.id).find params[:id]
      else
        @request_landing_page = RequestLandingPage.find params[:id]
      end
    end

    def collection
      if current_user.is_staff?
        @request_landing_pages = RequestLandingPage.where(admin_id: current_user.id).latest.search_by(params)
      else
        @request_landing_pages = RequestLandingPage.latest.search_by(params)
      end
    end

    def params_permit
      params.require(:request_landing_page).permit(:category_id, :campaign_id, :publisher_id, :layout_theme_id, :domain, :track_script, :status)
    end

    def class_name
      @resource_name = 'Request Landing Page'
      @collection_name = 'Request Landing Pages'
    end
  
end