class Admins::WebSettingsController < Admins::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "web settings", :admins_web_settings_path

  def index
    @web_settings = collection.page(page).per(per_page)
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    @activities = @web_setting.activities.latest.page(page).per(per_page)
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource
    if @web_setting.update params_permit
      create_activity(@web_setting, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} #{@web_setting.title}")
      redirect_to :back, notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@web_setting.errors.full_messages.join(',')}"
    end
  end

  private
    def resource
      @web_setting = WebSetting.find(params[:id])
    end

    def collection
      @web_settings = WebSetting.latest
    end

     def params_permit
      params.require(:web_setting).permit(
                                          :title, 
                                          :description, 
                                          :keywords, 
                                          :header_tags, 
                                          :footer_tags, 
                                          :contact, 
                                          :email, 
                                          :skype, 
                                          :favicon, 
                                          :logo, 
                                          :facebook, 
                                          :twitter, 
                                          :author, 
                                          :longitude, 
                                          :latitude, 
                                          :address,
                                          galleries_attributes: [
                                            :id,
                                            :title,
                                            :description,
                                            :image,
                                            :position,
                                            :featured,
                                            :_destroy,
                                            :deleted_at
                                          ])
    end

    def class_name
      @resource_name = 'Web Setting'
      @collection_name = 'Web Settings'
    end
end