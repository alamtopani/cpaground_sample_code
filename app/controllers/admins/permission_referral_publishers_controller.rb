class Admins::PermissionReferralPublishersController < Admins::ApplicationController
  before_action :check_profile
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "permission referral publishers", :admins_permission_referral_publishers_path

  def index
    @permission_referral_publishers = collection.page(page).per(per_page)
    @collection_size = "(#{@permission_referral_publishers.total_count})"
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  private
    def collection
      @permission_referral_publishers = PermissionReferralPublisher.latest.search_by(params)
    end

    def class_name
      @resource_name = 'Permission Referral Publisher'
      @collection_name = 'Permission Referral Publishers'
    end

end