class Admins::ApplicationController < ApplicationController
  layout 'application_admin'
  before_action :authenticate_user!, :filter_for_access!

  def authenticate_user!
    unless user_signed_in? && (current_user.is_admin? || current_user.is_staff? || current_user.is_finance? || current_user.is_owner?)
      redirect_to root_path, alert: "Can not access this page!"
    else
      if params[:controller] == 'admins/dashboards'
        # @request_publisher_query = Publisher.where(verified: false).latest
        # @request_publishers = @request_publisher_query

        # @request_campaigns_query = Campaign.latest
        # @request_campaigns = @request_campaigns_query

        # @request_contacts_query = Contact.where(status: Contact::UNREAD).latest
        # @request_contacts = @request_contacts_query

        # @notifications_query = Notification.all_of({user_id: current_user.id}, {status: Notification::UNREAD}).latest
        # @notifications = @notifications_query
      end
    end
  end

  def filter_for_access!
    if user_signed_in? && current_user.is_staff?
      if params[:controller] == 'admins/web_settings' || 
         params[:controller] == 'admins/contacts' || 
         params[:controller] == 'admins/admins' && params[:action] == 'index' ||
         params[:controller] == 'admins/category_campaigns' ||
         params[:controller] == 'admins/payment_snapshots' ||
         params[:controller] == 'admins/account_managers' ||
         params[:controller] == 'admins/layout_themes' ||
         params[:controller] == 'admins/subscribers' ||
         params[:controller] == 'admins/invoices' ||
         params[:controller] == 'admins/blogs' ||
         params[:controller] == 'admins/forums' ||
         params[:controller] == 'admins/owners' ||
         params[:controller] == 'admins/invoice_owners' ||
         params[:controller] == 'admins/groups' ||
         params[:controller] == 'admins/access_offers' ||
         params[:controller] == 'admins/permission_referral_publishers' ||
         params[:controller] == 'admins/commisions' ||
         params[:controller] == 'admins/payment_commisions'

        redirect_to admins_dashboards_path, alert: "Can not access this page!"
      end
    elsif user_signed_in? && current_user.is_finance?
      if params[:controller] == 'admins/web_settings' || 
         params[:controller] == 'admins/contacts' || 
         params[:controller] == 'admins/admins' && params[:action] == 'index' ||
         params[:controller] == 'admins/category_campaigns' ||
         params[:controller] == 'admins/layout_themes' ||
         params[:controller] == 'admins/subscribers' ||
         params[:controller] == 'admins/blogs' ||
         params[:controller] == 'admins/forums' ||
         params[:controller] == 'admins/owners' ||
         params[:controller] == 'admins/invoice_owners' ||
         params[:controller] == 'admins/groups' ||
         params[:controller] == 'admins/access_offers'

        redirect_to admins_dashboards_path, alert: "Can not access this page!"
      end
    end
  end

end