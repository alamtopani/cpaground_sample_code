class Admins::AdminsController < Admins::ApplicationController 
  before_action :class_name
  prepend_before_filter :draw_password, only: :update

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "admins", :admins_admins_path

  def index
    @admins = collection.where.not(id: current_user.id).page(page).per(per_page)
    @collection_size = "(#{@admins.total_count})"
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    @payment_snapshots = @admin.payment_snapshots.latest.search_by(params).page(params[:page_payment_snapshots]).per(per_page)
    @campaigns = @admin.campaigns.latest.search_by(params).page(params[:page_campaigns]).per(per_page)
    @report_campaign_snapshots = @admin.report_campaign_snapshots.latest.search_by(params).page(params[:page_report_campaign_snapshots]).per(per_page)
    @verification_snapshots = @admin.verification_snapshots.latest.search_by(params).page(params[:page_verification_snapshots]).per(per_page)
    @activities = @admin.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @admin = Admin.new
  end

  def create
    @admin = Admin.new(params_permit)
    @admin.confirmation_token = nil
    @admin.confirmed_at = Time.now
    if @admin.save
      create_activity(@admin, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@admin.username}")
      redirect_to admins_admins_path, notice: "Successfully created #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully created #{@resource_name}, #{@admin.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
    check_filter_for_access
  end

  def update
    resource

    if @admin.update(params_permit)
      create_activity(@admin, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} #{@admin.username}") if current_user.id != @admin.id
      redirect_to :back, notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@admin.errors.full_messages.join(',')}"
    end
  end

  def destroy
    resource

    if @admin.destroy
      redirect_to :back, alert: "Successfully deleted #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully deleted #{@resource_name}, #{@admin.errors.full_messages.join(',')}"
    end
  end

  private
    def check_filter_for_access
      unless current_user.id == @admin.id || current_user.is_admin?
        redirect_to admins_dashboards_path, alert: "Can not access this page!"
      end
    end

    def resource
      @admin = Admin.find(params[:id])
    end

    def collection
      @admins = Admin.latest.search_by(params)
    end

    def params_permit
      params.require(:admin).permit(:role_id, :username, :email, :skype, :avatar, :phone_code, :phone, :verified, :password, :password_confirmation, :confirmation_token, :confirmed_at)
    end

    def class_name
      @resource_name = 'Admin'
      @collection_name = 'Admins'
    end

    def draw_password
      %w(password password_confirmation).each do |attr|
        params[:admin].delete(attr)
      end if params[:admin] && params[:admin][:password].blank?
    end
end
