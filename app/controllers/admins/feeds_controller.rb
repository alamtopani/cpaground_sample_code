class Admins::FeedsController < Admins::ApplicationController  
  def create
    @feed = Feed.new(params_permit)
    @feed.user_id = current_user.id
    if @feed.save
      @feeds = Feed.activated.latest.page(page).per(per_page)
      redirect_to publishers_forums_path, notice: 'Your feed successfully share to everyone'
    end
  end

  def comment_create
    @feed_comment = FeedComment.create(user_id: current_user.id, message: params[:message], feedable_id: params[:feedable_id], feedable_type: params[:feedable_type])
    
    respond_to do |format|
      format.js { render json: { status: 'Success', user_username: @feed_comment.user.username, user_avatar: @feed_comment.user.avatar.url(:thumb), data: @feed_comment}, status: :ok }
    end
  end

  def destroy
    @comment = FeedComment.find_by(id: params[:id])
    if @comment.destroy
      redirect_to :back, notice: "Successfully delete feed comment"
    else
      redirect_to :back, alert: "Unsuccessfully delete feed comment, #{@comment.errors.full_messages.join(',')}"
    end
  end

  private
    def params_permit
      params.require(:feed).permit(:user_id, :message, :attachment_image)
    end
end