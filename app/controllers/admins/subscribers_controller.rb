class Admins::SubscribersController < Admins::ApplicationController
  before_action :class_name
  before_action :resource, only: [:show, :destroy]

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "subscribers", :admins_subscribers_path
  
  def index
    collection
    @subscribers_size = @subscribers if params[:export] == 'true'
    @subscribers = @subscribers.page(page).per(per_page)
    @collection_size = "(#{@subscribers.total_count})"

    respond_to do |format|
      format.html
      format.xls
      format.js
    end 
  end

  def show
    add_breadcrumb "#{params[:action]}"
    @activities = @subscriber.activities.latest.search_by(params).page(page).per(per_page)
  end

  def destroy
    if @subscriber.destroy
      redirect_to admins_subscribers_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to admins_subscribers_path, alert: "Unsuccessfully delete #{@resource_name}, #{@subscriber.errors.full_messages.join(',')}"
    end
  end

  def change_status
    resource
    @subscriber.status = params[:subscriber][:status]
    if @subscriber.save
      create_notification(@subscriber, @subscriber.user_id, "#{@resource_name.downcase} for #{@subscriber.email}", "#{@resource_name.downcase} for #{@subscriber.email} has #{@subscriber.status}.") if @subscriber.user_id.present?
      create_activity(@subscriber, current_user.id, "change status #{@resource_name.downcase} to #{@subscriber.status}", "successfully changed status #{@resource_name.downcase} #{@subscriber.email}.")
      redirect_to admins_subscriber_path(@subscriber), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@subscriber.errors.full_messages.join(',')}"
    end
  end

  private
    def resource
      @subscriber = Subscriber.find params[:id]
    end

    def collection
      @subscribers = Subscriber.latest.search_by(params)
    end

    def class_name
      @resource_name = 'Subscriber'
      @collection_name = 'Subscribers'
    end
  
end