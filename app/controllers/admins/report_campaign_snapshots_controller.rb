class Admins::ReportCampaignSnapshotsController < Admins::ApplicationController
	before_action :class_name

	add_breadcrumb "dashboard", :admins_dashboards_path
	add_breadcrumb "report campaigns", :admins_report_campaign_snapshots_path

	def index
		collection
		@total_revenue = '%.2f' % @report_campaign_snapshots.sum(:revenue)
		@publisher_revenue = '%.2f' % @report_campaign_snapshots.total(User::PUBLISHER)
		@fee_am = '%.2f' % @report_campaign_snapshots.total(User::ACCOUNTMANAGER)
		@fee_company = '%.2f' % @report_campaign_snapshots.total(User::ADMIN)
		@report_campaign_snapshots = @report_campaign_snapshots.page(page).per(per_page)
		@collection_size = "(#{@report_campaign_snapshots.total_count})"

		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

	def show
		add_breadcrumb "#{params[:action]}"
		resource
		@activities = @report_campaign_snapshot.activities.latest.page(params[:page_activities]).per(per_page)
		@convertion_reports = @report_campaign_snapshot.convertion_reports.search_by(params)
		if @convertion_reports.present?
			set_collection_convertion_report(params[:group_by], @convertion_reports, "all")
		end
	end

	def new
		add_breadcrumb "#{params[:action]}"
		@report_campaign_snapshot = ReportCampaignSnapshot.new
	end

	def create
		today = Time.zone.now
		@convertion = ConvertionReport.find_by(click_id: params[:convertion_report][:click_id].strip)
		revenue = @convertion.category_campaign.targetings.where(country_code: params[:convertion_report][:country_code].present? ? params[:convertion_report][:country_code] : @convertion.country_code).last
		revenue = @convertion.category_campaign.targetings.where(country_code: "ALL COUNTRY").last if revenue.blank?

		if @convertion.category_campaign.action == 'CPS'
			revenue = @convertion.category_campaign.targetings.latest.last
		end

		old_revenue = @convertion.revenue
		old_fee_publisher = @convertion.fee_publisher
		old_fee_am = @convertion.fee_am
		old_fee_company = @convertion.fee_company
		old_sales = @convertion.sales
		old_status = @convertion.status
		
		if @convertion.present? && @convertion.campaign_id.to_s == params[:convertion_report][:campaign_id] && revenue.present?
			@convertion.country_code = params[:convertion_report][:country_code]
			@convertion.revenue = params[:convertion_report][:revenue]
			@convertion.fee_publisher = (revenue.fee_publisher.to_f / 100) * @convertion.revenue
			@convertion.fee_am = (revenue.fee_am.to_f / 100) * @convertion.revenue
			@convertion.fee_company = (revenue.fee_company.to_f / 100) * @convertion.revenue
			if @convertion.category_campaign.action == 'CPS'
				@convertion.status = params[:convertion_report][:status]
				@convertion.sales = params[:convertion_report][:status] == ConvertionReport::APPROVED ? 1 : 0
			else
				@convertion.status = ConvertionReport::APPROVED
				@convertion.sales = 1
			end

			referral_publisher_top = @convertion.publisher.referral_publisher_top
			if referral_publisher_top.present?
				commision_percent = referral_publisher_top.permission_referral_publisher.commision
				commision = Commision.find_by(convertion_report_id: @convertion.id) rescue nil
				commision = commision.blank? ? Commision.new : commision
				commision.convertion_report_id = @convertion.id
				commision.account_manager_id = @convertion.account_manager_id
				commision.publisher_id = referral_publisher_top.id
				commision.referral_publisher_id = @convertion.publisher_id
				commision.report_campaign_snapshot_id = @convertion.report_campaign_snapshot_id
				commision.category_campaign_id = @convertion.category_campaign_id
				commision.campaign_date = @convertion.report_campaign_snapshot.campaign_date
				commision.campaign_id = @convertion.campaign_id
				commision.revenue = @convertion.fee_am
				commision.fee_publisher = (commision_percent.to_f / 100) * commision.revenue
				commision.fee_am = commision.revenue - commision.fee_publisher
				commision.status = @convertion.status
				commision.save
				@convertion.fee_am = commision.fee_am
			end 

			if @convertion.save
				today_report = @convertion.report_campaign_snapshot

				convertion_reports_approved = today_report.convertion_reports.approved
				today_report.sales = convertion_reports_approved.sum(:sales)
				today_report.revenue = convertion_reports_approved.sum(:revenue)
				today_report.fee_publisher = convertion_reports_approved.sum(:fee_publisher)
				today_report.fee_am = convertion_reports_approved.sum(:fee_am)
				today_report.fee_company = convertion_reports_approved.sum(:fee_company)

				# if old_revenue > 0 && old_sales > 0 && old_status == ConvertionReport::APPROVED
				#   today_report.revenue = (today_report.revenue - old_revenue) + @convertion.revenue
				#   today_report.fee_publisher = (today_report.fee_publisher - old_fee_publisher) + @convertion.fee_publisher
				#   today_report.fee_am = (today_report.fee_am - old_fee_am) + @convertion.fee_am
				#   today_report.fee_company = (today_report.fee_company - old_fee_company) + @convertion.fee_company
				# else
				#   today_report.sales = today_report.sales + 1
				#   today_report.revenue = today_report.revenue + @convertion.revenue
				#   today_report.fee_publisher = today_report.fee_publisher + @convertion.fee_publisher
				#   today_report.fee_am = today_report.fee_am + @convertion.fee_am
				#   today_report.fee_company = today_report.fee_company + @convertion.fee_company
				# end

				respond_to do |format|
					if today_report.save             
						if old_status == ConvertionReport::VISITED
							update_offer_sales(@convertion.category_campaign)
							create_activity_report(today_report.campaign, current_user.id, "Add new report campaign #{today_report.campaign.code}", "Successfully added revenue #{helpers.get_currency(params[:convertion_report][:revenue])} campaign report for #{today_report.campaign_date} - with detail: impressions: #{today_report.impressions}, clicks: #{today_report.clicks}, visitors: #{today_report.visitors}, unique_visitors: #{today_report.unique_visitors}, sales: #{today_report.sales}, total revenue: #{helpers.get_currency(today_report.revenue)}", today_report)
							create_activity(today_report, current_user.id, "Add new report campaign #{today_report.campaign.code}", "Successfully added revenue #{helpers.get_currency(params[:convertion_report][:revenue])} campaign report for #{today_report.campaign_date} - with detail: impressions: #{today_report.impressions}, clicks: #{today_report.clicks}, visitors: #{today_report.visitors}, unique_visitors: #{today_report.unique_visitors}, sales: #{today_report.sales}, total revenue: #{helpers.get_currency(today_report.revenue)}")
						else
							create_activity_report(today_report.campaign, current_user.id, "Update report campaign #{today_report.campaign.code}", "Successfully updated #{@convertion.status} revenue #{helpers.get_currency(old_revenue)} to #{helpers.get_currency(params[:convertion_report][:revenue])} campaign report for #{today_report.campaign_date} - with detail: impressions: #{today_report.impressions}, clicks: #{today_report.clicks}, visitors: #{today_report.visitors}, unique_visitors: #{today_report.unique_visitors}, sales: #{today_report.sales}, total revenue: #{helpers.get_currency(today_report.revenue)}", today_report)
							create_activity(today_report, current_user.id, "Update report campaign #{today_report.campaign.code}", "Successfully updated #{@convertion.status} revenue #{helpers.get_currency(old_revenue)} to #{helpers.get_currency(params[:convertion_report][:revenue])} campaign report for #{today_report.campaign_date} - with detail: impressions: #{today_report.impressions}, clicks: #{today_report.clicks}, visitors: #{today_report.visitors}, unique_visitors: #{today_report.unique_visitors}, sales: #{today_report.sales}, total revenue: #{helpers.get_currency(today_report.revenue)}")
						end

						format.html { redirect_to admins_campaign_path(today_report.campaign), notice: "Successfully added the latest campaign report" }
						format.js { render json: { status: 'Success', message: "Successfully added the latest campaign report #{today_report.code}", data: today_report}, status: :ok }
					else
						redirect_to :back, alert: "Failed to created #{@resource_name}"
					end
				end
			end
		else
			respond_to do |format|
				redirect_to :back, alert: "Failed to created #{@resource_name}"
			end
		end
	end

	def edit
		add_breadcrumb "#{params[:action]}"
		resource
		campaign
	end

	def update
		resource
		if @report_campaign_snapshot.update params_permit
			create_activity_report(@report_campaign_snapshot.campaign, current_user.id, "Updated report campaign #{@report_campaign_snapshot.campaign.code}", "Successfully updated report campaign for #{@report_campaign_snapshot.campaign_date} - has change to impressions: #{@report_campaign_snapshot.impressions}, clicks: #{@report_campaign_snapshot.clicks}, visitors: #{@report_campaign_snapshot.visitors}, unique_visitors: #{@report_campaign_snapshot.unique_visitors}, sales: #{@report_campaign_snapshot.sales}, revenue: #{helpers.get_currency(@report_campaign_snapshot.revenue)}", @report_campaign_snapshot)
			create_activity(@report_campaign_snapshot, current_user.id, "Updated report campaign #{@report_campaign_snapshot.campaign.code}", "Successfully updated report campaign for #{@report_campaign_snapshot.campaign_date} - has change to impressions: #{@report_campaign_snapshot.impressions}, clicks: #{@report_campaign_snapshot.clicks}, visitors: #{@report_campaign_snapshot.visitors}, unique_visitors: #{@report_campaign_snapshot.unique_visitors}, sales: #{@report_campaign_snapshot.sales}, revenue: #{helpers.get_currency(@report_campaign_snapshot.revenue)}")
			redirect_to admins_report_campaign_snapshot_path(@report_campaign_snapshot), notice: "Successfully updated #{@resource_name}"
		else
			redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@report_campaign_snapshot.errors.full_messages.join(',')}"
		end
	end

	def destroy
		resource
		if @report_campaign_snapshot.destroy
			redirect_to admins_campaign_path(@report_campaign_snapshot.campaign), notice: "Successfully delete #{@resource_name}"
		else
			redirect_to admins_campaign_path(@report_campaign_snapshot.campaign), alert: "Unsuccessfully delete #{@resource_name}, #{@report_campaign_snapshot.errors.full_messages.join(',')}"
		end
	end

	def delete_convertion_report
		today = Time.zone.now
		convertion = ConvertionReport.find params[:id]
		report_campaign_snapshot = ReportCampaignSnapshot.find convertion.report_campaign_snapshot_id
		if convertion.destroy
			convertion_reports = report_campaign_snapshot.convertion_reports
			convertion_reports_approved = convertion_reports.approved
			report_campaign_snapshot.clicks = convertion_reports.count
			report_campaign_snapshot.visitors = convertion_reports.count
			report_campaign_snapshot.unique_visitors = convertion_reports.group_by{|r| r.ip}.count
			report_campaign_snapshot.sales = convertion_reports_approved.sum(:sales)
			report_campaign_snapshot.revenue = convertion_reports_approved.sum(:revenue)
			report_campaign_snapshot.fee_publisher = convertion_reports_approved.sum(:fee_publisher)
			report_campaign_snapshot.fee_am = convertion_reports_approved.sum(:fee_am)
			report_campaign_snapshot.fee_company = convertion_reports_approved.sum(:fee_company)
			
			if report_campaign_snapshot.save
				update_offer_not_sales(convertion.category_campaign)
				create_activity_report(report_campaign_snapshot.campaign, current_user.id, "deleted convertion report (#{convertion.click_id})", "Successfully deleted convertion report for click id (#{convertion.click_id}) click date (#{convertion.convertion_date}) - report campaign has change to impressions: #{report_campaign_snapshot.impressions}, clicks: #{report_campaign_snapshot.clicks}, visitors: #{report_campaign_snapshot.visitors}, unique_visitors: #{report_campaign_snapshot.unique_visitors}, sales: #{report_campaign_snapshot.sales}, revenue: #{helpers.get_currency(report_campaign_snapshot.revenue)}", report_campaign_snapshot)
				create_activity(report_campaign_snapshot, current_user.id, "deleted convertion report (#{convertion.click_id})", "Successfully deleted convertion report for click id (#{convertion.click_id}) click date (#{convertion.convertion_date}) - report campaign has change to impressions: #{report_campaign_snapshot.impressions}, clicks: #{report_campaign_snapshot.clicks}, visitors: #{report_campaign_snapshot.visitors}, unique_visitors: #{report_campaign_snapshot.unique_visitors}, sales: #{report_campaign_snapshot.sales}, revenue: #{helpers.get_currency(report_campaign_snapshot.revenue)}")
				redirect_to :back, notice: "Successfully delete convertion"
			end
		else
			redirect_to :back, alert: "Unsuccessfully delete convertion"
		end
	end

	def change_convertion_report
		today = Time.zone.now
		convertion = ConvertionReport.find params[:id]
		convertion.status = params[:commit] == 'reject' ? ConvertionReport::REJECT : ConvertionReport::APPROVED
		convertion.sales = params[:commit] == 'reject' ? 0 : 1
		report_campaign_snapshot = ReportCampaignSnapshot.find convertion.report_campaign_snapshot_id
		if convertion.save
			convertion_reports = report_campaign_snapshot.convertion_reports
			convertion_reports_approved = convertion_reports.approved
			report_campaign_snapshot.sales = convertion_reports_approved.sum(:sales)
			report_campaign_snapshot.revenue = convertion_reports_approved.sum(:revenue)
			report_campaign_snapshot.fee_publisher = convertion_reports_approved.sum(:fee_publisher)
			report_campaign_snapshot.fee_am = convertion_reports_approved.sum(:fee_am)
			report_campaign_snapshot.fee_company = convertion_reports_approved.sum(:fee_company)
			
			if report_campaign_snapshot.save
				if convertion.status == ConvertionReport::REJECT
					update_offer_not_sales(convertion.category_campaign)
					create_activity_report(report_campaign_snapshot.campaign, current_user.id, "rejected convertion report (#{convertion.click_id})", "Successfully rejected convertion report for click id (#{convertion.click_id}) click date (#{convertion.convertion_date}) - report campaign has change to impressions: #{report_campaign_snapshot.impressions}, clicks: #{report_campaign_snapshot.clicks}, visitors: #{report_campaign_snapshot.visitors}, unique_visitors: #{report_campaign_snapshot.unique_visitors}, sales: #{report_campaign_snapshot.sales}, revenue: #{helpers.get_currency(report_campaign_snapshot.revenue)}", report_campaign_snapshot)
					create_activity(report_campaign_snapshot, current_user.id, "rejected convertion report (#{convertion.click_id})", "Successfully rejected convertion report for click id (#{convertion.click_id}) click date (#{convertion.convertion_date}) - report campaign has change to impressions: #{report_campaign_snapshot.impressions}, clicks: #{report_campaign_snapshot.clicks}, visitors: #{report_campaign_snapshot.visitors}, unique_visitors: #{report_campaign_snapshot.unique_visitors}, sales: #{report_campaign_snapshot.sales}, revenue: #{helpers.get_currency(report_campaign_snapshot.revenue)}")
					redirect_to :back, notice: "Successfully rejected convertion #{convertion.click_id}"
				elsif convertion.status == ConvertionReport::APPROVED
					update_offer_sales(convertion.category_campaign)
					create_activity_report(report_campaign_snapshot.campaign, current_user.id, "approved convertion report (#{convertion.click_id})", "Successfully rejected convertion report for click id (#{convertion.click_id}) click date (#{convertion.convertion_date}) - report campaign has change to impressions: #{report_campaign_snapshot.impressions}, clicks: #{report_campaign_snapshot.clicks}, visitors: #{report_campaign_snapshot.visitors}, unique_visitors: #{report_campaign_snapshot.unique_visitors}, sales: #{report_campaign_snapshot.sales}, revenue: #{helpers.get_currency(report_campaign_snapshot.revenue)}", report_campaign_snapshot)
					create_activity(report_campaign_snapshot, current_user.id, "approved convertion report (#{convertion.click_id})", "Successfully approved convertion report for click id (#{convertion.click_id}) click date (#{convertion.convertion_date}) - report campaign has change to impressions: #{report_campaign_snapshot.impressions}, clicks: #{report_campaign_snapshot.clicks}, visitors: #{report_campaign_snapshot.visitors}, unique_visitors: #{report_campaign_snapshot.unique_visitors}, sales: #{report_campaign_snapshot.sales}, revenue: #{helpers.get_currency(report_campaign_snapshot.revenue)}")
					redirect_to :back, notice: "Successfully approved convertion #{convertion.click_id}"
				end

				if convertion.commision.present?
					commision = convertion.commision
					commision.status = convertion.status
					commision.save
				end
			end
		else
			redirect_to :back, alert: "Unsuccessfully rejected convertion"
		end
	end

	private

		def update_offer_sales(offer)
			offer = offer
			offer.sales = offer.sales + 1
			offer.save
		end

		def update_offer_not_sales(offer)
			offer = offer
			offer.sales = offer.sales - 1
			offer.save
		end

		def resource
			if current_user.is_staff?
				@report_campaign_snapshot = ReportCampaignSnapshot.where(admin_id: current_user.id).find(params[:id])
			else
				@report_campaign_snapshot = ReportCampaignSnapshot.find(params[:id])
			end
		end

		def campaign
			@campaign = Campaign.find(@report_campaign_snapshot.campaign_id)
		end

		def collection
			if current_user.is_staff?
				@report_campaign_snapshots = ReportCampaignSnapshot.where(admin_id: current_user.id).latest.search_by(params)
			else
				@report_campaign_snapshots = ReportCampaignSnapshot.latest.search_by(params)
			end
		end

		def params_permit
			params.require(:report_campaign_snapshot).permit(:campaign_id, :campaign_date, :admin_id, :account_manager_id, :impressions, :clicks, :visitors, :unique_visitors, :sales, :country, :revenue, :fee_publisher, :fee_am, :fee_company, :created_at, :updated_at)
		end

		def class_name
			@resource_name = 'Report Campaign Snapshot'
			@collection_name = 'Report Campaign Snapshots'
		end
end
