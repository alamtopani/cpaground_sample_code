class Admins::ForumsController < Admins::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "forums", :admins_forums_path

  def index
    @feeds = collection.page(page).per(per_page)
    @collection_size = "(#{@feeds.total_count})"

    @comment_activities = FeedComment.where(feedable_type: FeedComment::FEED).not_in(user_id: current_user.id).latest.limit(50)
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    # @activities = @feed.activities.latest.page(page).per(per_page)
    @comment_activities = FeedComment.where(feedable_type: FeedComment::FEED).not_in(user_id: current_user.id).latest
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @feed = Feed.new
  end

  def create
    @feed = Feed.new params_permit
    @feed.user_id = current_user.id
    if @feed.save
      # create_activity(@feed, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@feed.title}")
      redirect_to admins_forum_path(@feed), notice: "Successfully created #{@resource_name}"
    else
      redirect_to admins_forums_path, alert: "Unsuccessfully created #{@resource_name}, #{@feed.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource
    if @feed.update params_permit
      # create_activity(@feed, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} #{@feed.title}")
      redirect_to admins_forum_path(@feed), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@feed.errors.full_messages.join(',')}"
    end
  end

  def destroy
    resource

    if @feed.destroy
      redirect_to :back, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully delete #{@resource_name}, #{@feed.errors.full_messages.join(',')}"
    end
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      if params[:commit] == Feed::ACTIVE
        Feed.where(:id.in => collect).each do |resource|
          resource.status = Feed::ACTIVE
          resource.save
          # create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
        end
      elsif params[:commit] == Feed::INACTIVE
        Feed.where(:id.in => collect).each do |resource|
          resource.status = Feed::INACTIVE
          resource.save
          # create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
        end
      end
      redirect_to admins_forums_path, notice: 'Status updated!'
    else
      redirect_to admins_forums_path, alert: 'Nothing checked!'
    end 
  end

  def change_status
    resource
    @feed.status = params[:feed][:status]
    if @feed.save
      redirect_to admins_forum_path(@feed), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@feed.errors.full_messages.join(',')}"
    end
  end

  def delete_comment
    @feed_comment = FeedComment.find_by(id: params[:id])
    if @feed_comment.destroy
      redirect_to :back, notice: "Successfully delete comment."
    else
      redirect_to :back, alert: "Unsuccessfully delete comment, #{@feed_comment.errors.full_messages.join(',')}"
    end
  end

  private
    def resource
      @feed = Feed.find(params[:id])
    end

    def collection
      @feeds = Feed.latest.search_by(params)
    end

    def params_permit
      params.require(:feed).permit(:user_id, :message, :attachment_image, :status)
    end

    def class_name
      @resource_name = 'Forum'
      @collection_name = 'Forums'
    end
end