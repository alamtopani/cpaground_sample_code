class Admins::InvoicesController < Admins::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "invoices", :admins_invoices_path

  def index
    collection
    @total_invoices = @invoices if params[:export] == 'true'
    @invoice_pending = @invoices.pending.sum(:price)
    @invoice_process = @invoices.process.sum(:price)
    @invoice_paid = @invoices.paid.sum(:price)
    @invoice_reject = @invoices.rejects.sum(:price)
    @invoices = @invoices.page(page).per(per_page)
    @collection_size = "(#{@invoices.total_count})"

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
      format.pdf do
        render pdf: "#{@resource_name} - #{@invoice.id}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @invoice = Invoice.new
  end

  def create
    @invoice = Invoice.new params_permit

    if @invoice.save
      redirect_to admins_invoice_path(@invoice), notice: "Successfully created #{@resource_name}"
    else
      redirect_to admins_invoices_path, alert: "Unsuccessfully created #{@resource_name}, #{@invoice.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource

    if @invoice.update params_permit
      redirect_to admins_invoice_path(@invoice), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@invoice.errors.full_messages.join(',')}"
    end
  end

  def destroy
    resource

    if @invoice.destroy
      redirect_to admins_invoices_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to admins_invoices_path, alert: "Unsuccessfully delete #{@resource_name}, #{@invoice.errors.full_messages.join(',')}"
    end
  end

  def change_status
    resource
    if params[:button] == Invoice::PROCESS
      @invoice.status = Invoice::PROCESS
      @invoice.save
    elsif params[:button] == Invoice::REJECT
      @invoice.status = Invoice::REJECT
      @invoice.save
    elsif params[:button] == Invoice::PAID
      @invoice.status = Invoice::PAID
      @invoice.save
    end
    redirect_to :back, notice: "change status #{@resource_name.downcase} to #{@invoice.status}"
  end

  private
    def resource
      @invoice = Invoice.find(params[:id])
    end

    def collection
      @invoices = Invoice.latest.search_by(params)
    end

    def params_permit
      params.require(:invoice).permit(:admin_id, :title, :category, :description, :price, :transaction_date, :from_acc, :to_acc, :attachment, :status)
    end

    def class_name
      @resource_name = 'Invoice'
      @collection_name = 'Invoices'
    end
end