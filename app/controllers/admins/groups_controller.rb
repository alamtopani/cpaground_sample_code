class Admins::GroupsController < Admins::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "groups", :admins_groups_path

  def index
    @groups = collection.page(page).per(per_page)
    @collection_size = "(#{@groups.total_count})"
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource

    @group_coverages_approve_all = @group.group_coverages.approve.latest.search_by(params)
    @group_coverages_waiting_all = @group.group_coverages.waiting.latest.search_by(params)

    @group_coverages_approve = @group_coverages_approve_all.page(params[:group_coverages_approve]).per(per_page)
    @group_coverages_waiting = @group_coverages_waiting_all.page(params[:group_coverages_waiting]).per(per_page)
    
    @feeds = @group.feeds.activated.latest.page(page).per(per_page)
    @groups = Group.all
    @comment_activities = FeedComment.where(feedable_type: FeedComment::FEED, :feedable_id.in => @group.feeds.pluck(:id)).not_in(user_id: current_user.id).latest.limit(50)
  end

  def destroy
    resource

    if @group.destroy
      redirect_to admins_groups_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to admins_groups_path, alert: "Unsuccessfully delete #{@resource_name}, #{@group.errors.full_messages.join(',')}"
    end
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      if params[:commit] == Group::ACTIVE
        Group.where(:id.in => collect).each do |resource|
          resource.status = Group::ACTIVE
          resource.save
          # create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
        end
      elsif params[:commit] == Group::INACTIVE
        Group.where(:id.in => collect).each do |resource|
          resource.status = Group::INACTIVE
          resource.save
          # create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
        end
      end
      redirect_to admins_groups_path, notice: 'Status updated!'
    else
      redirect_to admins_groups_path, alert: 'Nothing checked!'
    end 
  end

  def change_status
    resource
    @group.status = params[:group][:status]
    if @group.save
      redirect_to admins_group_path(@group), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@group.errors.full_messages.join(',')}"
    end
  end

  private
    def resource
      @group = Group.find(params[:id])
    end

    def collection
      @groups = Group.search_by(params)
    end

    def class_name
      @resource_name = 'Group'
      @collection_name = 'Groups'
    end
end