class Admins::OwnersController < Admins::ApplicationController 
  include OwnerHelper
  before_action :class_name
  prepend_before_filter :draw_password, only: :update

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "owners", :admins_owners_path

  def index
    @collection_size = "(#{collection.count})"
    @owners = collection
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    reports = @owner.report_campaign_snapshots
    get_analytic_report_owner(reports, params, @owner, 'dashbord_admin_owner')
    @invoices = @owner.invoice_owners.latest.page(page).per(per_page)
    
    respond_to do |format|
      format.html 
      format.js
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @owner = Owner.new
  end

  def create
    @owner = Owner.new(params_permit)
    @owner.confirmation_token = nil
    @owner.confirmed_at = Time.now
    if @owner.save
      redirect_to admins_owners_path, notice: "Successfully created #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully created #{@resource_name}, #{@owner.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource

    if @owner.update(params_permit)
      redirect_to :back, notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@owner.errors.full_messages.join(',')}"
    end
  end

  def destroy
    resource

    if @owner.destroy
      redirect_to :back, alert: "Successfully deleted #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully deleted #{@resource_name}, #{@owner.errors.full_messages.join(',')}"
    end
  end

  private
    def resource
      @owner = Owner.find(params[:id])
    end

    def collection
      @owners = Owner.latest
    end

    def params_permit
      params.require(:owner).permit(:role_id, :username, :email, :skype, :avatar, :phone_code, :phone, :verified, :password, :password_confirmation, :confirmation_token, :confirmed_at)
    end

    def class_name
      @resource_name = 'Owner'
      @collection_name = 'Owners'
    end

    def draw_password
      %w(password password_confirmation).each do |attr|
        params[:owner].delete(attr)
      end if params[:owner] && params[:owner][:password].blank?
    end
end
