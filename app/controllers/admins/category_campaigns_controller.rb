class Admins::CategoryCampaignsController < Admins::ApplicationController
	before_action :class_name

	add_breadcrumb "dashboard", :admins_dashboards_path
	add_breadcrumb "offers", :admins_category_campaigns_path

	def index
		collection
		@total_category_campaigns = @category_campaigns if params[:export] == 'true'
		@category_campaigns = @category_campaigns.page(page).per(per_page)
		@collection_size = "(#{@category_campaigns.total_count})"

		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

	def show
		add_breadcrumb "#{params[:action]}"
		resource
		@campaigns = @category_campaign.campaigns.latest.page(params[:page_campaigns]).per(per_page)
		@activities = @category_campaign.activities.latest.page(params[:page_activities]).per(per_page)
		@access_offers = @category_campaign.access_offers.latest.page(params[:page_access_offers]).per(per_page)
    @targetings_all = @category_campaign.targetings.latest
		@targetings = @targetings_all.page(params[:page_targetings]).per(per_page)
		@banner_images = @category_campaign.banner_images.latest.page(params[:page_banner_images]).per(per_page)
		
		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

	def new
		add_breadcrumb "#{params[:action]}"
		@category_campaign = CategoryCampaign.new
	end

	def create
		@category_campaign = CategoryCampaign.new params_permit
    @category_campaign.link = "#{CategoryCampaign::HTTP_DOMAIN_LINK_OFFER}/?offer_id="+@category_campaign.id
		if @category_campaign.save
			@category_campaign.blast_message("Available new offer #{@category_campaign.name} category #{@category_campaign.category_name}, please do request campaign in <a href='/publishers/offers?action_offer=#{@category_campaign.action}'>list offers</a>.", "Available new offer #{@category_campaign.name} category #{@category_campaign.category_name}", "Hopefully god bless your day, with this opportunity we give new offer for niche '#{@category_campaign.action}' category '#{@category_campaign.category_name}' as follows:", "Hopefully with this offer you can add your income, and given ease in your business.") if @category_campaign.status == "active"
			create_activity(@category_campaign, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@category_campaign.name}")
			redirect_to admins_category_campaign_path(@category_campaign), notice: "Successfully created #{@resource_name}"
		else
			redirect_to admins_category_campaigns_path, alert: "Unsuccessfully created #{@resource_name}, #{@category_campaign.errors.full_messages.join(',')}"
		end
	end

	def edit
		add_breadcrumb "#{params[:action]}"
		resource
	end

	def update
		resource
		if @category_campaign.update params_permit
			create_activity(@category_campaign, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} #{@category_campaign.name}")
			redirect_to admins_category_campaign_path(@category_campaign), notice: "Successfully updated #{@resource_name}"
		else
			redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@category_campaign.errors.full_messages.join(',')}"
		end
	end

	def destroy
		resource

		if @category_campaign.destroy
			redirect_to admins_category_campaigns_path, notice: "Successfully delete #{@resource_name}"
		else
			redirect_to admins_category_campaigns_path, alert: "Unsuccessfully delete #{@resource_name}, #{@category_campaign.errors.full_messages.join(',')}"
		end
	end

	def multiple_action
		if params[:ids].present?
			collect = params[:ids]
			if params[:commit] == CategoryCampaign::ACTIVE
				CategoryCampaign.where(:id.in => collect).each do |resource|
					resource.status = CategoryCampaign::ACTIVE
					resource.active_campaign
					resource.save
					create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.name}")
				end
			elsif params[:commit] == CategoryCampaign::INACTIVE
				CategoryCampaign.where(:id.in => collect).each do |resource|
					resource.status = CategoryCampaign::INACTIVE
					resource.featured = false
					resource.inactive_campaign
					resource.save
					create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.name}")
				end
			elsif params[:commit] == "featured"
				CategoryCampaign.where(:id.in => collect).each do |resource|
					resource.featured = true
					resource.save
					create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.name}")
				end
			elsif params[:commit] == "unfeatured"
				CategoryCampaign.where(:id.in => collect).each do |resource|
					resource.featured = false
					resource.save
					create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.name}")
				end
			elsif params[:commit] == "promoted"
				CategoryCampaign.where(:id.in => collect).each do |resource|
					resource.blast_message("Promote offer #{resource.name} category #{resource.category_name}, please check in <a href='/publishers/offers?action_offer=#{resource.action}'>list offers</a>.", "Promote offer #{resource.name} category #{resource.category_name}", "Promote offer #{resource.name} category #{resource.category_name}.", "Please check in list offers.") if resource.status == "active"
					create_activity(resource, current_user.id, "promote offer", "successfully promoted #{@resource_name.downcase} #{resource.name}")
				end
			end
			redirect_to admins_category_campaigns_path, notice: 'Status updated!'
		else
			redirect_to admins_category_campaigns_path, alert: 'Nothing checked!'
		end 
	end

	def change_status
		resource
		@category_campaign.status = params[:category_campaign][:status]
		if @category_campaign.save
			if @category_campaign.status == CategoryCampaign::INACTIVE
				@category_campaign.featured = false
				@category_campaign.save
				@category_campaign.inactive_campaign 
			else
				@category_campaign.active_campaign
			end
			create_activity(@category_campaign, current_user.id, "change status #{@resource_name.downcase} to #{@category_campaign.status}", "successfully changed #{@resource_name.downcase} #{@category_campaign.name}")
			redirect_to admins_category_campaign_path(@category_campaign), notice: "Successfully updated #{@resource_name}"
		else
			redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@category_campaign.errors.full_messages.join(',')}"
		end
	end

	def delete_targeting
		targeting = Targeting.find params[:id]
		if targeting.destroy
			redirect_to :back, notice: "Successfully deleted targeting"
		else
			redirect_to :back, alert: "Unuccessfully deleted targeting"
		end
	end

	def delete_banner_image
		banner_image = BannerImage.find params[:id]
		if banner_image.destroy
			redirect_to :back, notice: "Successfully deleted banner image"
		else
			redirect_to :back, alert: "Unuccessfully deleted banner image"
		end
	end

	def multiple_action_targeting
		if params[:ids].present?
			collect = params[:ids]
			if params[:commit] == "deleted"
				Targeting.where(:id.in => collect).each do |resource|
					resource.destroy
				end
			end
			redirect_to :back, notice: 'Status updated!'
		else
			redirect_to :back, alert: 'Nothing checked!'
		end 
	end

	def multiple_action_banner_image
		if params[:banner_image_ids].present?
			collect = params[:banner_image_ids]
			if params[:commit] == "deleted"
				BannerImage.where(:id.in => collect).each do |resource|
					resource.destroy
				end
			end
			redirect_to :back, notice: 'Status updated!'
		else
			redirect_to :back, alert: 'Nothing checked!'
		end 
	end

  def import
    CategoryCampaign.import(params)
    redirect_to admins_category_campaigns_path, notice: 'Targetings Offer imported!'
  end

	def import_per_offer
    @category_campaign = CategoryCampaign.find(params[:offer_id])
		CategoryCampaign.import_per_offer(params, @category_campaign)
		redirect_to admins_category_campaign_path(@category_campaign), notice: 'Targetings Offer imported!'
	end

	private
		def resource
			@category_campaign = CategoryCampaign.find(params[:id])
		end

		def collection
			@category_campaigns = CategoryCampaign.search_by(params)
		end

		def params_permit
			params.require(:category_campaign).permit(:name, :link, :action, :detail, :status, :category_id, :auto_approved, :need_approval, :expires, :cover, :link_offer, 
																					targetings_attributes: [
																						:id,
																						:category_campaign_id,
																						:country_code,
																						:payout_price,
																						:fee_publisher,
																						:fee_am,
																						:fee_company
																					],
																					banner_images_attributes: [
																						:id,
																						:category_campaign_id,
																						:name,
																						:description
																					])
		end

		def class_name
			@resource_name = 'Offer'
			@collection_name = 'Offers'
		end
end