class Admins::AccessOffersController < Admins::ApplicationController
  before_action :check_profile
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "access offers", :admins_access_offers_path

  def index
    @access_offers = collection.page(page).per(per_page)
    @collection_size = "(#{@access_offers.total_count})"
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      if params[:commit] == AccessOffer::APPROVE
        AccessOffer.where(:id.in => collect).each do |resource|
          unless resource.status == AccessOffer::APPROVE
            resource.status = AccessOffer::APPROVE
            resource.save
            @offer = resource.offer
            campaigns = Campaign.all_of({publisher_id: resource.publisher_id}, {category_campaign_id: resource.category_campaign_id})
            if campaigns.present?
              campaigns.each do |resource|
                resource.banned = false
                resource.save
              end
            end 
            create_activity(@offer, current_user.id, "Change status applied offer '#{@offer.name}' with code #{@offer.code}", "#{current_user.username} have successfully change status offer '#{@offer.name}' with code #{@offer.code} owned by #{resource.publisher.username} to #{resource.status}.")
            create_notification(@offer, resource.publisher_id, "Your applied offer '#{@offer.name}' with code #{@offer.code} has #{resource.status}", "Your applied offer '#{@offer.name}' with code #{@offer.code} has change status to #{resource.status}.")
            UserMailer.approve_access_offer(resource).deliver_now!
          end
        end
      elsif params[:commit] == AccessOffer::BANNED
        AccessOffer.where(:id.in => collect).each do |resource|
          unless resource.status == AccessOffer::BANNED
            resource.status = AccessOffer::BANNED
            resource.save
            @offer = resource.offer
            campaigns = Campaign.all_of({publisher_id: resource.publisher_id}, {category_campaign_id: resource.category_campaign_id})
            if campaigns.present?
              campaigns.each do |resource|
                resource.banned = true
                resource.save
              end
            end 
            create_activity(@offer, current_user.id, "Change status applied offer '#{@offer.name}' with code #{@offer.code}", "#{current_user.username} have successfully change status offer '#{@offer.name}' with code #{@offer.code} owned by #{resource.publisher.username} to #{resource.status}.")
            create_notification(@offer, resource.publisher_id, "Your applied offer '#{@offer.name}' with code #{@offer.code} has #{resource.status}", "Your applied offer '#{@offer.name}' with code #{@offer.code} has change status to #{resource.status}.")
            UserMailer.banned_access_offer(resource).deliver_now!
          end
        end
      elsif params[:commit] == "permission"
        AccessOffer.where(:id.in => collect).each do |resource|
          unless resource.checked == true
            resource.checked = true
            resource.save
            @offer = resource.offer
            create_activity(@offer, current_user.id, "Permission offer '#{@offer.name}' with code #{@offer.code}", "#{current_user.username} have successfully permission/approved offer '#{@offer.name}' with code #{@offer.code} owned by #{resource.publisher.username}.")
            create_notification(@offer, resource.publisher_id, "Your applied offer '#{@offer.name} with code #{@offer.code}' has #{resource.status}", "Now you can do get offer.")
            UserMailer.approve_access_offer(resource).deliver_now!
          end
        end
      end
      redirect_to admins_access_offers_path, notice: 'Status updated!'
    else
      redirect_to admins_access_offers_path, alert: 'Nothing checked!'
    end 
  end

  private
    def collection
      @access_offers = AccessOffer.latest.search_by(params)
    end

    def class_name
      @resource_name = 'Access Offer'
      @collection_name = 'Access Offers'
    end
end