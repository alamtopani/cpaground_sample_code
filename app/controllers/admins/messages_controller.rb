class Admins::MessagesController < Admins::ApplicationController
	before_action :class_name

	add_breadcrumb "dashboard", :admins_dashboards_path
	add_breadcrumb "messages", :admins_messages_path

	def index
		@messages = collection.page(page).per(per_page)
		@collection_size = "(#{@messages.total_count})"

		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

	def create
		publishers = params[:tags]
		message = params[:message][:message]

		if publishers.include? "all"
			get_publishers.each do |publisher|
				group_message_id = GroupMessage.where(account_manager_id: publisher.account_manager_id, publisher_id: publisher.id).first
				if group_message_id.blank?
		      group_message_id = GroupMessage.create(account_manager_id: publisher.account_manager_id, publisher_id: publisher.id, status: 'active')
		    end
				create_message(group_message_id, publisher, message)
				@message.save
			end
		else
			publishers.each do |publisher_id|
				publisher = Publisher.find publisher_id
				group_message_id = GroupMessage.where(account_manager_id: publisher.account_manager_id, publisher_id: publisher.id).first
				if group_message_id.blank?
		      group_message_id = GroupMessage.create(account_manager_id: publisher.account_manager_id, publisher_id: publisher.id, status: 'active')
		    end
				create_message(group_message_id, publisher, message)
				@message.save
			end
		end

		respond_to do |format|
			if @message.save
				redirect_to :back, notice: "Successfully send message"
				format.html
				format.js
			else
				redirect_to :back, notice: "Unsuccessfully send message"
				format.html
				format.js
			end
		end
	end

	def destroy
		resource

		if @message.destroy
			redirect_to admins_messages_path, notice: "Successfully delete #{@resource_name}"
		else
			redirect_to admins_messages_path, alert: "Unsuccessfully delete #{@resource_name}, #{@message.errors.full_messages.join(',')}"
		end
	end

	private
		def resource
			@message = Message.find(params[:id])
		end

		def collection
			@messages = Message.latest.search_by(params)
		end

		def class_name
			@resource_name = 'Message'
			@collection_name = 'Messages'
		end

		def create_message(group_message_id, publisher, message)
			@message = Message.new
			@message.group_message_id = group_message_id
			@message.from = publisher.account_manager_id
			@message.to = publisher.id
			@message.message = message
		end
end