class Admins::ContactsController < Admins::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "contacts", :admins_contacts_path

  def index
    collection
    @total_contacts = @contacts if params[:export] == 'true'
    @contacts = @contacts.page(page).per(per_page)
    @collection_size = "(#{@contacts.total_count})"

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    resource.update(status: Contact::READ)
    @activities = @contact.activities.page(page).per(per_page)
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @contact = Contact.new
  end

  def create
    @contact = Contact.new params_permit
    if @contact.save
      create_activity(@contact, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@contact.name}")
      redirect_to admins_contact_path(@contact), notice: "Successfully created #{@resource_name}"
    else
      redirect_to admins_contacts_path, alert: "Unsuccessfully created #{@resource_name}, #{@contact.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource
    if @contact.update params_permit 
      create_activity(@contact, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} #{@contact.name}")
      redirect_to admins_contact_path(@contact), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@contact.errors.full_messages.join(',')}"
    end
  end

  def destroy
    resource

    if @contact.destroy
      redirect_to admins_contacts_path, notice: "Successfully deleted #{@resource_name}"
    else
      redirect_to admins_contacts_path, alert: "Unsuccessfully deleted #{@resource_name}, #{@contact.errors.full_messages.join(',')}"
    end
  end

  private
    def resource
      @contact = Contact.find(params[:id])
    end

    def collection
      @contacts = Contact.latest.search_by(params)
    end

    def params_permit
      params.require(:contact).permit(:name, :email, :phone, :website, :message, :status)
    end

    def class_name
      @resource_name = 'Contact'
      @collection_name = 'Contacts'
    end
end