class Admins::DashboardsController < Admins::ApplicationController
  
	def index
    if current_user.is_staff?
  		@account_managers_size = AccountManager.all.count
      @publishers_size = Publisher.where(admin_id: current_user.id).verified.no_suspended.count
      @campaigns_size = Campaign.where(admin_id: current_user.id).count
      @landing_pages_size = RequestLandingPage.where(admin_id: current_user.id).count
    else
      @account_managers_size = AccountManager.all.count
      @publishers_size = Publisher.verified.no_suspended.count
      @campaigns_size = Campaign.all.count
      @landing_pages_size = RequestLandingPage.all.count
    end

    get_analytic_report(current_user, 'dashbord_admin')
    # multipe_chart(@convertion_reports)

    respond_to do |format|
      format.html
      format.js
    end
	end

end