class Admins::CampaignsController < Admins::ApplicationController
	before_action :class_name

	add_breadcrumb "dashboard", :admins_dashboards_path
	add_breadcrumb "campaigns", :admins_campaigns_path

	def index
		collection
		@total_campaigns = @campaigns if params[:export] == 'true'
		@campaigns = @campaigns.page(page).per(per_page)
		@collection_size = "(#{@campaigns.total_count})"

		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

	def show
		add_breadcrumb "#{params[:action]}"
		resource
		
		@landing_pages = @campaign.request_landing_pages.latest.page(params[:page_landing_pages]).per(per_page)
		
		# For history activities
		@activities = @campaign.activities.latest.page(params[:page_activities]).per(per_page)

		get_analytic_report(current_user, 'dashbord_admin_campaign')
		@offer = @campaign.category_campaign
		@banner_images = @offer.banner_images
		
		if @convertion_reports.present?
			set_collection_convertion_report(params[:group_by], @convertion_reports, "all")
			# multipe_chart(@convertion_reports_list)
		end
		
		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

	def new
		add_breadcrumb "#{params[:action]}"
		@campaign = Campaign.new
	end

	def create
		@campaign = Campaign.new params_permit
		@campaign.admin_id = current_user.id
		@campaign.account_manager_id = @campaign.publisher.account_manager_id
		if @campaign.save
			create_activity(@campaign, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@campaign.title}")
			redirect_to admins_campaign_path(@campaign), notice: "Successfully created #{@resource_name}"
		else
			redirect_to admins_campaigns_path, alert: "Unsuccessfully created #{@resource_name}, #{@campaign.errors.full_messages.join(',')}"
		end
	end

	def import
		Campaign.import(params)
		redirect_to admins_campaigns_path, notice: 'Report Campaigns imported!'
	end

	def edit
		add_breadcrumb "#{params[:action]}"
		resource
	end

	def update
		resource
		if @campaign.update params_permit
			create_activity(@campaign, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} #{@campaign.title}")
			redirect_to admins_campaign_path(@campaign), notice: "Successfully updated #{@resource_name}"
		else
			redirect_to :back, alert: "Unsuccessfully created #{@resource_name}, #{@campaign.errors.full_messages.join(',')}"
		end
	end

	def destroy
		resource

		if @campaign.destroy
			redirect_to admins_campaigns_path, notice: "Successfully delete #{@resource_name}"
		else
			redirect_to admins_campaigns_path, alert: "Unsuccessfully delete #{@campaign.errors.full_messages.join(',')}"
		end
	end

	def multiple_action
		if params[:ids].present?
			collect = params[:ids]
			if params[:commit] == Campaign::ACTIVE
				Campaign.where(:id.in => collect).each do |resource|
					resource.status = Campaign::ACTIVE
					resource.save
					create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
				end
			elsif params[:commit] == Campaign::INACTIVE
				Campaign.where(:id.in => collect).each do |resource|
					resource.status = Campaign::INACTIVE
					resource.save
					create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
				end
			elsif params[:commit] == Campaign::PENDING
				Campaign.where(:id.in => collect).each do |resource|
					resource.status = Campaign::PENDING
					resource.save
					create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
				end
			elsif params[:commit] == Campaign::COMPLETE
				Campaign.where(:id.in => collect).each do |resource|
					resource.status = Campaign::COMPLETE
					resource.save
					create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
				end
			end
			redirect_to :back, notice: 'Status updated!'
		else
			redirect_to :back, alert: 'Nothing checked!'
		end 
	end

	def change_status
		resource
		@campaign.status = params[:campaign][:status]
		# @campaign.link = params[:campaign][:link]
		# @campaign.tracking_code = params[:campaign][:tracking_code]
		# @campaign.admin_id = current_user.id
		if @campaign.save
			UserMailer.after_update_campaign(@campaign).deliver_now! if @campaign.is_active?
			create_activity(@campaign, current_user.id, "change status #{@resource_name.downcase} to #{@campaign.status}", "successfully changed #{@resource_name.downcase} #{@campaign.title}")
			create_notification(@campaign, @campaign.publisher_id, "Your #{@resource_name.downcase} #{@campaign.title}", "Your #{@resource_name.downcase} #{@campaign.title} has approved.")
			create_notification(@campaign, @campaign.account_manager_id, "#{@campaign.publisher.username.capitalize} #{@resource_name.downcase} #{@campaign.title}", "#{@campaign.publisher.username.capitalize} #{@resource_name.downcase} #{@campaign.title} has approved.")
			redirect_to admins_campaign_path(@campaign), notice: "Successfully updated #{@resource_name}"
		else
			redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@campaign.errors.full_messages.join(',')}"
		end
	end

	def click_notif
		notification = Notification.find(params[:notification_id])
		notification.status = "read"
		notification.save

		redirect_to admins_campaign_path(params[:id])
	end

	private
		def resource
			if current_user.is_staff?
				@campaign = Campaign.find(params[:id])
			else
				@campaign = Campaign.find(params[:id])
			end
		end

		def collection
			if current_user.is_staff?
				@campaigns = Campaign.where(admin_id: current_user.id).latest.search_by(params)
			else
				@campaigns = Campaign.latest.search_by(params)
			end
		end

		def params_permit
			params.require(:campaign).permit(:admin_id, :account_manager_id, :publisher_id, :category_campaign_id, :title, :status, :link, :tracking_code)
		end

		def class_name
			@resource_name = 'Campaign'
			@collection_name = 'Campaigns'
		end
end