class Admins::AccountManagersController < Admins::ApplicationController
	before_action :class_name
  prepend_before_filter :draw_password, only: :update

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "account managers", :admins_account_managers_path

  def index
    collection
    @total_account_managers = @account_managers if params[:export] == 'true'
    @account_managers = @account_managers.page(page).per(per_page)
    @collection_size = "(#{@account_managers.total_count})"

    @date = Time.zone.now.to_date

    if params[:campaign_start_at].blank? && params[:campaign_end_at].blank?
      reports = ReportCampaignSnapshot.have_sales_only.where(:campaign_date.gte => @date.beginning_of_month, :campaign_date.lte => @date)
    else
      reports = ReportCampaignSnapshot.have_sales_only.search_by_campaign_date(params)
    end

    @ranking_am = reports.group_by{|r| r.account_manager_id}.map{|am| am}.map{|am| [am[0], AccountManager.find(am[0]).the_name?, am[1].map(&:fee_am).reduce(:+), am[1].map(&:fee_publisher).reduce(:+), am[1].map(&:fee_company).reduce(:+), am[1].map(&:revenue).reduce(:+), am[1].map(&:sales).reduce(:+)]}.sort_by {|am| am[2]}.reverse
    # @sales_am = reports.group_by{|r| r.account_manager_id}.values.map{|r| [AccountManager.find(r.last.account_manager_id).the_name?, r.map(&:sales).reduce(:+)]}.sort_by {|r| r[1]}.reverse
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    @profile_account_manager = @account_manager.profile_account_manager
    @publishers = @account_manager.publishers.latest.search_by(params).page(params[:page_publishers]).per(per_page)
    @campaigns = @account_manager.campaigns.latest.search_by(params).page(params[:page_campaigns]).per(per_page)
    @request_landing_pages = @account_manager.request_landing_pages.latest.search_by(params).page(params[:page_landing_pages]).per(per_page)
    @payment_snapshots = @account_manager.payment_snapshots.latest.search_by(params).page(params[:page_payment_snapshots]).per(per_page)
    @activities = @account_manager.activities.latest.search_by(params).page(params[:page_activities]).per(per_page)
    @referrals = @account_manager.referrals.latest.search_by(params).page(params[:page_referrals]).per(per_page)
    @permission_referral_publishers = @account_manager.permission_referral_publishers.latest.search_by(params).page(params[:page_permission_referral_publishers]).per(per_page)
    @commisions = @account_manager.commisions.latest.search_by(params).page(params[:page_commisions]).per(per_page)

    reports = ReportCampaignSnapshot.where(account_manager_id: @account_manager.id)
    next_last_payment_all(reports)

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @account_manager = AccountManager.new
  end

  def create
    @account_manager = AccountManager.new params_permit
    @account_manager.confirmation_token = nil
    @account_manager.confirmed_at = Time.now
    if check_acc_payment(@account_manager.id, params_permit)
      if @account_manager.save
        create_activity(@account_manager, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@account_manager.username}")
        redirect_to admins_account_manager_path(@account_manager), notice: "Successfully created #{@resource_name}"
      else
        redirect_to admins_account_managers_path, alert: "Unsuccessfully created #{@resource_name}, #{@account_manager.errors.full_messages.join(',')}"
      end
    else
      redirect_to :back, alert: "Unsuccessfully created #{@resource_name}, #{@account_manager.profile_account_manager.get_payment_type} has already used."
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource
    if check_acc_payment(@account_manager.id, params_permit)
      if @account_manager.update params_permit
        create_activity(@account_manager, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} #{@account_manager.username}") if current_user.id != @account_manager.id
        redirect_to admins_account_manager_path(@account_manager), notice: "Successfully updated #{@resource_name}"
      else
        redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@account_manager.errors.full_messages.join(',')}"
      end
    else
      choice_payment_type = get_payment_type_choose(params_permit[:profile_account_manager_attributes][:choice_payment_type])
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{choice_payment_type} has already used."
    end
  end

  def destroy
    resource

    if @account_manager.destroy
      redirect_to admins_account_managers_path, notice: "Successfully deleted #{@resource_name}"
    else
      redirect_to admins_account_managers_path, alert: "Unsuccessfully deleted #{@resource_name}, #{@account_manager.errors.full_messages.join(',')}"
    end
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      if params[:commit] == 'active'
        AccountManager.where(id: collect).each do |resource|
          resource.verified = true
          resource.save
        end
      elsif params[:commit] == 'inactive'
        AccountManager.where(id: collect).each do |resource|
          resource.verified = false
          resource.save
        end
      end
      redirect_to :back, notice: 'Status updated!'
    else
      redirect_to :back, alert: 'Nothing checked!'
    end 
  end

  def change_status
    resource
    @account_manager.verified = params[:account_manager][:verified]
    if @account_manager.save
      redirect_to admins_account_manager_path(@account_manager), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@account_manager.errors.full_messages.join(',')}"
    end
  end

  def detail_report
    add_breadcrumb "reports"
    resource
    get_analytic_report(@account_manager, 'dashbord_admin_am')
    if @convertion_reports.present?
      set_collection_convertion_report(params[:group_by], @convertion_reports, "am")
      # multipe_chart(@convertion_reports_list)
    end
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  private
    def resource
      @account_manager = AccountManager.find(params[:id])
    end

    def collection
      @account_managers = AccountManager.latest.search_by(params)
    end

    def params_permit
      params.require(:account_manager).permit(:username, :email, :skype, :avatar, :phone_code, :phone, :verified, :password, :password_confirmation,
        profile_account_manager_attributes:[:account_manager_id, :first_name, :last_name, :country, :province, :city, :address, :zip_code, :choice_payment_type, :acc_paypal, :acc_payoneer, :acc_bank, :longitude, :latitude, :bank_name, :acc_name])
    end

    def class_name
      @resource_name = 'Account manager'
      @collection_name = 'Account managers'
    end

    def draw_password
      %w(password password_confirmation).each do |attr|
        params[:account_manager].delete(attr)
      end if params[:account_manager] && params[:account_manager][:password].blank?
    end

    def check_acc_payment(account_manager_id, params_permit)
      params = params_permit[:profile_account_manager_attributes]
      choice_payment_type = params[:choice_payment_type]
      if choice_payment_type == "1"
        if ProfileAccountManager.where.not(account_manager_id: account_manager_id).where(acc_paypal: params[:acc_paypal]).blank?
          return true
        else
          return false
        end
      elsif choice_payment_type == "2"
        if ProfileAccountManager.where.not(account_manager_id: account_manager_id).where(acc_payoneer: params[:acc_payoneer]).blank?
          return true
        else
          return false
        end
      elsif choice_payment_type == "3"
        if ProfileAccountManager.where.not(account_manager_id: account_manager_id).where(acc_bank: params[:acc_bank]).blank?
          return true
        else
          return false
        end
      end
    end

    def get_payment_type_choose(type)
      case type
      when "1" then type = ProfileAccountManager::ACC_PAYPAL
      when "2" then type = ProfileAccountManager::ACC_PAYONEER
      when "3" then type = ProfileAccountManager::ACC_BANK
      end
    end
end