class Admins::CommisionsController < Admins::ApplicationController
  before_action :check_profile
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "commisions", :admins_commisions_path

  def index
    collection
    @collection_size = "(#{@commisions.count})"
    @fee_publisher_approved = @commisions.approved.sum(:fee_publisher)
    @fee_publisher_pending = @commisions.pending.sum(:fee_publisher)
    @fee_publisher_rejected = @commisions.rejected.sum(:fee_publisher)
    @fee_am_approved = @commisions.approved.sum(:fee_am)
    @fee_am_pending = @commisions.pending.sum(:fee_am)
    @fee_am_rejected = @commisions.rejected.sum(:fee_am)
    @total_revenue_approved = @commisions.approved.sum(:revenue)
    @total_revenue_pending = @commisions.pending.sum(:revenue)
    @total_revenue_rejected = @commisions.rejected.sum(:revenue)
    
    if params[:group_by].present?
      if @commisions.present?
        set_collection_group(@commisions)
        @commisions_group = @commisions
        @commisions = Kaminari.paginate_array(@commisions).page(page).per(per_page)
      end
    else
      @commisions = @commisions.page(page).per(per_page)
    end
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  private
    def collection
      @commisions = Commision.search_by(params)
    end

    def class_name
      @resource_name = 'Commision'
      @collection_name = 'Commisions'
    end

    def set_collection_group(collection)
      commisions_group = collection.group_by {|d| d.publisher_id} rescue nil
      if commisions_group.present?
        @commisions = commisions_group.values.map{|w| [w.map(&:publisher_id).last, w.map(&:account_manager_id).last, w.map(&:fee_publisher).reduce(:+)] }
      end
    end

end