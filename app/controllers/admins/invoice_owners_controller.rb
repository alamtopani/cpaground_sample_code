class Admins::InvoiceOwnersController < Admins::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "invoice owners", :admins_invoice_owners_path

  def index
    collection
    @total_invoices = @invoices if params[:export] == 'true'
    @invoice_pending = @invoices.pending.sum(:price)
    @invoice_process = @invoices.process.sum(:price)
    @invoice_paid = @invoices.paid.sum(:price)
    @invoice_reject = @invoices.rejects.sum(:price)
    @invoices = @invoices.page(page).per(per_page)
    @collection_size = "(#{@invoices.total_count})"

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
      format.pdf do
        render pdf: "#{@resource_name} - #{@invoice.id}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @invoice = InvoiceOwner.new
  end

  def create
    owner_id = params[:invoice_owner][:owner_id]

    owner = Owner.find owner_id
    payment_at = params[:invoice_owner][:payment_at]
    payment_end = params[:invoice_owner][:payment_end]

    if Time.zone.now.to_date > payment_end.to_date
      unless check_data(owner_id, payment_at).present?
        @invoice = InvoiceOwner.new params_permit
        reports = owner.report_campaign_snapshots.where(:campaign_date.gte => payment_at, :campaign_date.lte => payment_end)
        total = '%.4f' % reports.sum(:fee_company)
        @invoice.price = '%.4f' % (total.to_f * owner.fee)
        if @invoice.save
          redirect_to admins_invoice_owner_path(@invoice), notice: "Successfully created #{@resource_name}"
        else
          redirect_to admins_invoice_owners_path, alert: "Unsuccessfully created #{@resource_name}, #{@invoice.errors.full_messages.join(',')}"
        end
      else
        redirect_to :back, alert: "Unsuccessfully created #{@resource_name} because #{owner.username} period #{payment_at} - #{payment_end} has already entered!"
      end
    else
      redirect_to :back, alert: "Unsuccessfully created #{@resource_name} because payment_end selected is bigger than this day!"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource

    owner_id = @invoice.owner_id
    owner = Owner.find owner_id
    payment_at = params[:invoice_owner][:payment_at]
    payment_end = params[:invoice_owner][:payment_end]
    if Time.zone.now.to_date > payment_end.to_date
      if check_data_update(owner_id, payment_at, payment_end).present?
        reports = owner.report_campaign_snapshots.where(:campaign_date.gte => payment_at, :campaign_date.lte => payment_end)
        total = '%.4f' % reports.sum(:fee_company)
        @invoice.price = '%.4f' % (total.to_f * owner.fee)
        if @invoice.update params_permit
          redirect_to admins_invoice_owner_path(@invoice), notice: "Successfully updated #{@resource_name}"
        else
          redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@invoice.errors.full_messages.join(',')}"
        end
      else
        redirect_to :back, alert: "Unsuccessfully updated #{@resource_name} because #{owner.username} period #{payment_at} - #{payment_end} has never entered!"
      end
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name} because payment_end selected is bigger than this day!"
    end
  end

  def destroy
    resource

    if @invoice.destroy
      redirect_to admins_invoice_owners_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to admins_invoice_owners_path, alert: "Unsuccessfully delete #{@resource_name}, #{@invoice.errors.full_messages.join(',')}"
    end
  end

  def change_status
    resource
    if params[:button] == InvoiceOwner::PROCESS
      @invoice.status = InvoiceOwner::PROCESS
      @invoice.save
    elsif params[:button] == InvoiceOwner::REJECT
      @invoice.status = InvoiceOwner::REJECT
      @invoice.save
    elsif params[:button] == InvoiceOwner::PAID
      @invoice.status = InvoiceOwner::PAID
      @invoice.save
    end
    redirect_to :back, notice: "change status #{@resource_name.downcase} to #{@invoice.status}"
  end

  private
    def resource
      @invoice = InvoiceOwner.find(params[:id])
    end

    def collection
      if current_user.is_owner?
        @invoices = InvoiceOwner.latest.where(owner_id: current_user.id).search_by(params)
      else
        @invoices = InvoiceOwner.latest.search_by(params)
      end
    end

    def params_permit
      params.require(:invoice_owner).permit(:owner_id, :title, :description, :price, :payment_at, :payment_end, :transaction_date, :from_acc, :to_acc, :attachment, :status)
    end

    def class_name
      @resource_name = 'Invoice Owner'
      @collection_name = 'Invoice Owners'
    end

    def check_data(owner_id, payment_at)
      invoice_owner = InvoiceOwner.where(owner_id: owner_id, payment_at: payment_at).ne(status: InvoiceOwner::REJECT)
      return invoice_owner
    end

    def check_data_update(owner_id, payment_at, payment_end)
      invoice_owner = InvoiceOwner.where(owner_id: owner_id , payment_at: payment_at, payment_end: payment_end).ne(status: InvoiceOwner::PAID)
      return invoice_owner
    end
end