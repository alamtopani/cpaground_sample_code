 class Admins::PaymentSnapshotsController < Admins::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "payment snapshots", :admins_payment_snapshots_path

  def index
    collection
    @total_payment_snapshots = @payment_snapshots if params[:export] == 'true'
    @payment_for_publisher = @payment_snapshots.where('users.type =?', User::PUBLISHER)
    @payment_for_manager = @payment_snapshots.where('users.type =?', User::ACCOUNTMANAGER)
    @payment_pending = @payment_snapshots.pending.sum(:price)
    @payment_process = @payment_snapshots.process.sum(:price)
    @payment_paid = @payment_snapshots.paid.sum(:price)
    @payment_reject = @payment_snapshots.rejects.sum(:price)

    @payment_snapshots = @payment_snapshots.page(page).per(per_page)
    @collection_size = "(#{@payment_snapshots.total_count})"

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    @activities = @payment_snapshot.activities.latest.page(page).per(per_page)
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
      format.pdf do
        render pdf: "#{@resource_name} - #{@payment_snapshot.id}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @payment_snapshot = PaymentSnapshot.new
  end

  def create
    user_id = params[:payment_snapshot][:user_id]

    user = User.find user_id
    payment_at = params[:payment_snapshot][:payment_at]
    payment_end = params[:payment_snapshot][:payment_end]
    from_acc = params[:from_acc]
    to_acc = params[:to_acc]
    
    @payment_snapshot = PaymentSnapshot.new params_permit
    @payment_snapshot.admin_id = current_user.id
    if user.is_publisher?
      reports = ReportCampaignSnapshot.where(publisher_id: user_id, :campaign_date.gte => payment_at, :campaign_date.lte => payment_end)
    elsif user.is_account_manager?
      reports = ReportCampaignSnapshot.where(account_manager_id: user_id, :campaign_date.gte => payment_at, :campaign_date.lte => payment_end)
    end
    @payment_snapshot.total = '%.4f' % reports.sum(:revenue)
    if @payment_snapshot.user.is_publisher?
      @payment_snapshot.price = '%.4f' % reports.sum(:fee_publisher)
    else
      @payment_snapshot.price = '%.4f' % reports.sum(:fee_am)
    end
    @payment_snapshot.fee_publisher = '%.4f' % reports.sum(:fee_publisher)
    @payment_snapshot.fee_am = '%.4f' % reports.sum(:fee_am)
    @payment_snapshot.fee_company = '%.4f' % reports.sum(:fee_company)
    @payment_snapshot.from_acc = from_acc
    @payment_snapshot.to_acc = to_acc
    if @payment_snapshot.save
      create_activity(@payment_snapshot, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} period #{@payment_snapshot.payment_at} - #{@payment_snapshot.payment_end}")
      redirect_to admins_payment_snapshot_path(@payment_snapshot), notice: "Successfully created #{@resource_name}"
    else
      redirect_to admins_payment_snapshots_path, alert: "Unsuccessfully created #{@resource_name}, #{@payment_snapshot.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource
    user_id = @payment_snapshot.user_id
    user = User.find user_id
    payment_at = params[:payment_snapshot][:payment_at]
    payment_end = params[:payment_snapshot][:payment_end]
    from_acc = params[:from_acc]
    to_acc = params[:to_acc]
    if Time.zone.now.to_date > payment_end.to_date
      # if check_data_update(user_id, payment_at, payment_end).present?
      @payment_snapshot.admin_id = current_user.id
      if user.is_publisher?
        reports = ReportCampaignSnapshot.where(publisher_id: user_id, :campaign_date.gte => payment_at, :campaign_date.lte => payment_end)
      elsif user.is_account_manager?
        reports = ReportCampaignSnapshot.where(account_manager_id: user_id, :campaign_date.gte => payment_at, :campaign_date.lte => payment_end)
      end
      @payment_snapshot.total = '%.4f' % reports.sum(:revenue)
      if @payment_snapshot.user.is_publisher?
        @payment_snapshot.price = '%.4f' % reports.sum(:fee_publisher)
      else
        @payment_snapshot.price = '%.4f' % reports.sum(:fee_am)
      end
      @payment_snapshot.fee_publisher = '%.4f' % reports.sum(:fee_publisher)
      @payment_snapshot.fee_am = '%.4f' % reports.sum(:fee_am)
      @payment_snapshot.fee_company = '%.4f' % reports.sum(:fee_company)
      @payment_snapshot.from_acc = from_acc
      @payment_snapshot.to_acc = to_acc
      if @payment_snapshot.update params_permit
        create_activity(@payment_snapshot, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} period #{@payment_snapshot.payment_at} - #{@payment_snapshot.payment_end}")
        redirect_to admins_payment_snapshot_path(@payment_snapshot), notice: "Successfully updated #{@resource_name}"
      else
        redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@payment_snapshot.errors.full_messages.join(',')}"
      end
      # else
      #   redirect_to :back, alert: "Unsuccessfully updated #{@resource_name} because #{user.username} period #{payment_at} - #{payment_end} has never entered!"
      # end
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name} because payment_end selected is bigger than this day!"
    end
  end

  def destroy
    resource

    if @payment_snapshot.destroy
      redirect_to admins_payment_snapshots_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to admins_payment_snapshots_path, alert: "Unsuccessfully delete #{@resource_name}, #{@payment_snapshot.errors.full_messages.join(',')}"
    end
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      if params[:commit] == PaymentSnapshot::PAID
        PaymentSnapshot.where(id: collect).each do |resource|
          resource.status = PaymentSnapshot::PAID
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} period #{resource.payment_at} - #{resource.payment_end}")
        end
      elsif params[:commit] == PaymentSnapshot::PENDING
        PaymentSnapshot.where(id: collect).each do |resource|
          resource.status = PaymentSnapshot::PENDING
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} period #{resource.payment_at} - #{resource.payment_end}")
        end
      elsif params[:commit] == PaymentSnapshot::PROCESS
        PaymentSnapshot.where(id: collect).each do |resource|
          resource.status = PaymentSnapshot::PROCESS
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} period #{resource.payment_at} - #{resource.payment_end}")
        end
      elsif params[:commit] == PaymentSnapshot::REJECT
        PaymentSnapshot.where(id: collect).each do |resource|
          resource.status = PaymentSnapshot::REJECT
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} period #{resource.payment_at} - #{resource.payment_end}")
        end
      end
      redirect_to admins_payment_snapshots_path, notice: 'Status updated!'
    else
      redirect_to admins_payment_snapshots_path, alert: 'Nothing checked!'
    end 
  end

  def change_status
    resource
    if params[:button] == PaymentSnapshot::PROCESS
      @payment_snapshot.status = PaymentSnapshot::PROCESS
      @payment_snapshot.save
      create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{@payment_snapshot.status}", "successfully changed #{@resource_name.downcase} period #{@payment_snapshot.payment_at} - #{@payment_snapshot.payment_end}")
      create_notification(@payment_snapshot, @payment_snapshot.user_id, "We already transfer your payment", "successfully transfer payment period #{@payment_snapshot.payment_at} - #{@payment_snapshot.payment_end}")
    elsif params[:button] == PaymentSnapshot::REJECT
      @payment_snapshot.status = PaymentSnapshot::REJECT
      @payment_snapshot.save
      create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{@payment_snapshot.status}", "successfully changed #{@resource_name.downcase} period #{@payment_snapshot.payment_at} - #{@payment_snapshot.payment_end}")
    elsif params[:button] == PaymentSnapshot::PAID
      @payment_snapshot.status = PaymentSnapshot::PAID
      @payment_snapshot.save
      UserMailer.after_payment_publisher_paid(@payment_snapshot, "Payment").deliver_now!
      create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{@payment_snapshot.status}", "successfully changed #{@resource_name.downcase} period #{@payment_snapshot.payment_at} - #{@payment_snapshot.payment_end}")
    end
    redirect_to :back, notice: "change status #{@resource_name.downcase} to #{@payment_snapshot.status}"
  end

  private
    def resource
      @payment_snapshot = PaymentSnapshot.find(params[:id])
    end

    def collection
      @payment_snapshots = PaymentSnapshot.search_by(params)
    end

    def params_permit
      params.require(:payment_snapshot).permit(:user_id, :price, :fee_publisher, :fee_am, :fee_company, :total, :message, :status, :payment_at, :payment_end, :category, :from_acc, :to_acc)
    end

    def class_name
      @resource_name = 'Payment Snapshot'
      @collection_name = 'Payment Snapshots'
    end

    def check_data(user_id, payment_at)
      payment_snapshot = PaymentSnapshot.where("payment_snapshots.user_id =? AND payment_snapshots.payment_end >?", user_id, payment_at).where.not(status: PaymentSnapshot::REJECT)
      return payment_snapshot
    end

    def check_data_update(user_id, payment_at, payment_end)
      payment_snapshot = PaymentSnapshot.where("payment_snapshots.user_id =? AND payment_snapshots.payment_at =? AND payment_snapshots.payment_end =?", user_id, payment_at, payment_end).where.not(status: PaymentSnapshot::PAID)
      return payment_snapshot
    end
end