class Admins::InformationsController < Admins::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "informations", :admins_informations_path

  def index
    @informations = collection.page(page).per(per_page)
    @collection_size = "(#{@informations.total_count})"
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @information = Information.new
  end

  def create
    @information = Information.new params_permit

    if @information.save
      # @information.send_email_blast if @information.status == Information::ACTIVE
      redirect_to admins_information_path(@information), notice: "Successfully created #{@resource_name}"
    else
      redirect_to admins_informations_path, alert: "Unsuccessfully created #{@resource_name}, #{@information.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource

    if @information.update params_permit
      redirect_to admins_information_path(@information), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@information.errors.full_messages.join(',')}"
    end
  end

  def destroy
    resource

    if @information.destroy
      redirect_to admins_informations_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to admins_informations_path, alert: "Unsuccessfully delete #{@resource_name}, #{@information.errors.full_messages.join(',')}"
    end
  end

  def change_status
    resource
    if params[:information][:status] == Information::ACTIVE
      @information.status = Information::ACTIVE
      @information.save
    elsif params[:information][:status] == Information::INACTIVE
      @information.status = Information::INACTIVE
      @information.save
    end
    redirect_to :back, notice: "change status #{@resource_name.downcase} to #{@information.status}"
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      if params[:commit] == Information::ACTIVE
        Information.where(:id.in => collect).each do |resource|
          resource.status = Information::ACTIVE
          resource.save
        end
      elsif params[:commit] == Information::INACTIVE
        Information.where(:id.in => collect).each do |resource|
          resource.status = Information::INACTIVE
          resource.save
        end
      elsif params[:commit] == Information::PROMOTE
        Information.where(:id.in => collect).each do |resource|
          # resource.send_email_blast if resource.status == Information::ACTIVE
        end
      end
      redirect_to admins_informations_path, notice: 'Status updated!'
    else
      redirect_to admins_informations_path, alert: 'Nothing checked!'
    end 
  end

  private
    def resource
      @information = Information.find(params[:id])
    end

    def collection
      @informations = Information.search_by(params)
    end

    def params_permit
      params.require(:information).permit(:title, :description, :status)
    end

    def class_name
      @resource_name = 'Information'
      @collection_name = 'Informations'
    end
end