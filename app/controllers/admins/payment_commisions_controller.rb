 class Admins::PaymentCommisionsController < Admins::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "payment commisions", :admins_payment_commisions_path

  def index
    collection
    @total_payment_commisions = @payment_commisions if params[:export] == 'true'
    @payment_pending = @payment_commisions.pending.sum(:price)
    @payment_process = @payment_commisions.process.sum(:price)
    @payment_paid = @payment_commisions.paid.sum(:price)
    @payment_reject = @payment_commisions.rejects.sum(:price)

    @payment_commisions = @payment_commisions.page(page).per(per_page)
    @collection_size = "(#{@payment_commisions.total_count})"

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    @activities = @payment_commision.activities.latest.page(page).per(per_page)
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
      format.pdf do
        render pdf: "#{@resource_name} - #{@payment_commision.id}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @payment_commision = PaymentCommision.new
  end

  def create
    user_id = params[:payment_commision][:user_id]

    publisher = Publisher.find user_id
    payment_at = params[:payment_commision][:payment_at]
    payment_end = params[:payment_commision][:payment_end]
    from_acc = params[:from_acc]
    to_acc = params[:to_acc]
    
    @payment_commision = PaymentCommision.new params_permit
    @payment_commision.admin_id = current_user.id
    commisions = Commision.where(publisher_id: user_id, :campaign_date.gte => payment_at, :campaign_date.lte => payment_end, status: "approved")
    @payment_commision.total = '%.4f' % commisions.sum(:revenue)
    @payment_commision.price = '%.4f' % commisions.sum(:fee_publisher)
    @payment_commision.fee_publisher = '%.4f' % commisions.sum(:fee_publisher)
    @payment_commision.fee_am = '%.4f' % commisions.sum(:fee_am)
    @payment_commision.from_acc = from_acc
    @payment_commision.to_acc = to_acc
    if @payment_commision.save
      create_activity(@payment_commision, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} period #{@payment_commision.payment_at} - #{@payment_commision.payment_end}")
      redirect_to admins_payment_commision_path(@payment_commision), notice: "Successfully created #{@resource_name}"
    else
      redirect_to admins_payment_commisions_path, alert: "Unsuccessfully created #{@resource_name}, #{@payment_commision.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource
    user_id = @payment_commision.user_id
    publisher = Publisher.find user_id
    payment_at = params[:payment_commision][:payment_at]
    payment_end = params[:payment_commision][:payment_end]
    from_acc = params[:from_acc]
    to_acc = params[:to_acc]
    if Time.zone.now.to_date > payment_end.to_date
      if check_data_update(user_id, payment_at, payment_end).present?
        @payment_commision.admin_id = current_user.id
        @payment_commision.total = '%.4f' % reports.sum(:revenue)
        @payment_commision.price = '%.4f' % reports.sum(:fee_publisher)
        @payment_commision.fee_publisher = '%.4f' % reports.sum(:fee_publisher)
        @payment_commision.fee_am = '%.4f' % reports.sum(:fee_am)
        @payment_commision.from_acc = from_acc
        @payment_commision.to_acc = to_acc
        if @payment_commision.update params_permit
          create_activity(@payment_commision, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} period #{@payment_commision.payment_at} - #{@payment_commision.payment_end}")
          redirect_to admins_payment_commision_path(@payment_commision), notice: "Successfully updated #{@resource_name}"
        else
          redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@payment_commision.errors.full_messages.join(',')}"
        end
      else
        redirect_to :back, alert: "Unsuccessfully updated #{@resource_name} because #{user.username} period #{payment_at} - #{payment_end} has never entered!"
      end
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name} because payment_end selected is bigger than this day!"
    end
  end

  def destroy
    resource

    if @payment_commision.destroy
      redirect_to admins_payment_commisions_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to admins_payment_commisions_path, alert: "Unsuccessfully delete #{@resource_name}, #{@payment_commision.errors.full_messages.join(',')}"
    end
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      if params[:commit] == PaymentCommision::PAID
        PaymentCommision.where(id: collect).each do |resource|
          resource.status = PaymentCommision::PAID
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} period #{resource.payment_at} - #{resource.payment_end}")
        end
      elsif params[:commit] == PaymentCommision::PENDING
        PaymentCommision.where(id: collect).each do |resource|
          resource.status = PaymentCommision::PENDING
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} period #{resource.payment_at} - #{resource.payment_end}")
        end
      elsif params[:commit] == PaymentCommision::PROCESS
        PaymentCommision.where(id: collect).each do |resource|
          resource.status = PaymentCommision::PROCESS
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} period #{resource.payment_at} - #{resource.payment_end}")
        end
      elsif params[:commit] == PaymentCommision::REJECT
        PaymentCommision.where(id: collect).each do |resource|
          resource.status = PaymentCommision::REJECT
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} period #{resource.payment_at} - #{resource.payment_end}")
        end
      end
      redirect_to admins_payment_commisions_path, notice: 'Status updated!'
    else
      redirect_to admins_payment_commisions_path, alert: 'Nothing checked!'
    end 
  end

  def change_status
    resource
    if params[:button] == PaymentCommision::PROCESS
      @payment_commision.status = PaymentCommision::PROCESS
      @payment_commision.save
      create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{@payment_commision.status}", "successfully changed #{@resource_name.downcase} period #{@payment_commision.payment_at} - #{@payment_commision.payment_end}")
      create_notification(@payment_commision, @payment_commision.user_id, "We already transfer your payment", "successfully transfer payment period #{@payment_commision.payment_at} - #{@payment_commision.payment_end}")
    elsif params[:button] == PaymentCommision::REJECT
      @payment_commision.status = PaymentCommision::REJECT
      @payment_commision.save
      create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{@payment_commision.status}", "successfully changed #{@resource_name.downcase} period #{@payment_commision.payment_at} - #{@payment_commision.payment_end}")
    elsif params[:button] == PaymentCommision::PAID
      @payment_commision.status = PaymentCommision::PAID
      @payment_commision.save
      UserMailer.after_payment_publisher_paid(@payment_commision, "Payment Commision").deliver_now!
      create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{@payment_commision.status}", "successfully changed #{@resource_name.downcase} period #{@payment_commision.payment_at} - #{@payment_commision.payment_end}")
    end
    redirect_to :back, notice: "change status #{@resource_name.downcase} to #{@payment_commision.status}"
  end

  private
    def resource
      @payment_commision = PaymentCommision.find(params[:id])
    end

    def collection
      @payment_commisions = PaymentCommision.search_by(params)
    end

    def params_permit
      params.require(:payment_commision).permit(:user_id, :price, :fee_publisher, :fee_am, :total, :message, :status, :payment_at, :payment_end, :from_acc, :to_acc)
    end

    def class_name
      @resource_name = 'Payment Commision'
      @collection_name = 'Payment Commisions'
    end

    def check_data(user_id, payment_at)
      payment_commision = PaymentCommision.where("payment_commisions.user_id =? AND payment_commisions.payment_end >?", user_id, payment_at).where.not(status: PaymentCommision::REJECT)
      return payment_commision
    end

    def check_data_update(user_id, payment_at, payment_end)
      payment_commision = PaymentCommision.where("payment_commisions.user_id =? AND payment_commisions.payment_at =? AND payment_commisions.payment_end =?", user_id, payment_at, payment_end).where.not(status: PaymentCommision::PAID)
      return payment_commision
    end
end