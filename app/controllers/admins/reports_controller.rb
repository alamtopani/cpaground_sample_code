class Admins::ReportsController < Admins::ApplicationController
	before_action :check_profile
	before_action :class_name

	add_breadcrumb "dashboard", :admins_dashboards_path
	add_breadcrumb "reports", :admins_reports_path

	def index
		@date = Time.zone.now.to_date

    unless params[:start_at].present? && params[:end_at].present?
      unless params[:status] == "visited"
        collection = ConvertionReport.where(convertion_date: @date).have_sales
      else
        collection = ConvertionReport.where(convertion_date: @date).visited
      end
    else
      unless params[:status] == "visited"
        collection = ConvertionReport.have_sales.search_by(params)
      else
        collection = ConvertionReport.visited.search_by(params)
      end
    end

    collection = collection.where(admin_id: current_user.id) if current_user.is_staff?
		
		# multipe_chart(collection) if collection.present? && current_user.is_admin? && params[:status] != "visited"
		# @convertion_reports = collection

		set_collection_convertion_report(params[:group_by], collection, "all") rescue nil
		
    unless params[:status] == "visited"
      @collection_name = "Convertion Reports"
      @collection_size = "(#{collection.count rescue 0})"

  		if params[:group_by].blank?			
  			@total_revenue_approved = collection.approved.sum(:revenue) rescue 0
  			@total_revenue_pending = collection.pending.sum(:revenue) rescue 0
  			@total_revenue_reject = collection.where(status: "reject").sum(:revenue) rescue 0

  			# @fee_publisher_approved = collection.approved.sum(:fee_publisher) rescue 0
  			# @fee_publisher_pending = collection.pending.sum(:fee_publisher) rescue 0
  			# @fee_publisher_reject = collection.where(status: "reject").sum(:fee_publisher) rescue 0

  			# @fee_am_approved = collection.approved.sum(:fee_am) rescue 0
  			# @fee_am_pending = collection.pending.sum(:fee_am) rescue 0
  			# @fee_am_reject = collection.where(status: "reject").sum(:fee_am) rescue 0

  			# @fee_company_approved = collection.approved.sum(:fee_company) rescue 0
  			# @fee_company_pending = collection.pending.sum(:fee_company) rescue 0
  			# @fee_company_reject = collection.where(status: "reject").sum(:fee_company) rescue 0
  		end

      if params[:export] == 'true'
        @total_convertion_reports = collection.have_sales_only.latest rescue nil
      end
    else
      @collection_name = "Performance Reports"
    end

		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

	def show
		add_breadcrumb "#{params[:action]}"
		resource
    @report_items = @report.report_items
    @activities = @report.activities.latest.page(params[:page_activities]).per(per_page)

		respond_to do |format|
			format.html 
			format.xls
			format.js
		end
	end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource
    if @report.update params_permit
      update_report_items_value(@report)
      redirect_to admins_report_path(@report), notice: "Successfully updated report items"
    else
      redirect_to :back, alert: "Unsuccessfully updated report items, #{@report.errors.full_messages.join(',')}"
    end
  end

  def delete_report_item
    report_item = ReportItem.find params[:report_item_id]
    convertion_report = ConvertionReport.find params[:id]

    if report_item.destroy
      update_report_items_value(convertion_report)
      redirect_to admins_report_path(convertion_report), notice: "Successfully deleted report item"
    else
      redirect_to admins_report_path(convertion_report), alert: "Unuccessfully deleted report item"
    end
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      report_item_last = ReportItem.find(collect.last)
      report = ConvertionReport.find(report_item_last.convertion_report_id)
      if params[:commit] == ReportItem::APPROVED
        ReportItem.where(:id.in => collect).each do |resource|
          resource.status = ReportItem::APPROVED
          resource.save
        end
      elsif params[:commit] == ReportItem::PENDING
        ReportItem.where(:id.in => collect).each do |resource|
          resource.status = ReportItem::PENDING
          resource.save
        end
      elsif params[:commit] == ReportItem::REJECT
        ReportItem.where(:id.in => collect).each do |resource|
          resource.status = ReportItem::REJECT
          resource.save
        end
      end

      update_report_items_value(report)
      redirect_to :back, notice: 'Status updated!'
    else
      redirect_to :back, alert: 'Nothing checked!'
    end 
  end

  def input
    add_breadcrumb "Input Report"
  end

	private
		def resource
			@report = ConvertionReport.find(params[:id])
		end

    def update_report_items_value(report)
      convertion_report = report
      old_status_convertion_report = convertion_report.status
      targeting = convertion_report.category_campaign.targetings.latest.last
      convertion_report.report_items.each do |resource|
        resource.name = convertion_report.category_campaign.name
        resource.fee_publisher = (targeting.fee_publisher.to_f / 100) * resource.revenue
        resource.fee_am = (targeting.fee_am.to_f / 100) * resource.revenue
        resource.fee_company = (targeting.fee_company.to_f / 100) * resource.revenue
        resource.save
      end

      if convertion_report.report_items_sales.present?
        convertion_report.sales = 1
        convertion_report.status = ConvertionReport::APPROVED
        convertion_report.revenue = convertion_report.report_items.approved.sum(:revenue)
        convertion_report.fee_publisher = convertion_report.report_items.approved.sum(:fee_publisher)
        convertion_report.fee_am = convertion_report.report_items.approved.sum(:fee_am)
        convertion_report.fee_company = convertion_report.report_items.approved.sum(:fee_company)
      elsif convertion_report.report_items_sales_pending.present?
        convertion_report.sales = 0
        convertion_report.status = ConvertionReport::PENDING
        convertion_report.revenue = convertion_report.report_items.pending.sum(:revenue)
        convertion_report.fee_publisher = convertion_report.report_items.pending.sum(:fee_publisher)
        convertion_report.fee_am = convertion_report.report_items.pending.sum(:fee_am)
        convertion_report.fee_company = convertion_report.report_items.pending.sum(:fee_company)
      elsif convertion_report.report_items_sales_reject.present?
        convertion_report.sales = 0
        convertion_report.status = ConvertionReport::REJECT
        convertion_report.revenue = convertion_report.report_items.rejected.sum(:revenue)
        convertion_report.fee_publisher = convertion_report.report_items.rejected.sum(:fee_publisher)
        convertion_report.fee_am = convertion_report.report_items.rejected.sum(:fee_am)
        convertion_report.fee_company = convertion_report.report_items.rejected.sum(:fee_company)
      else
        convertion_report.sales = 0
        convertion_report.status = ConvertionReport::VISITED
        convertion_report.revenue = 0
        convertion_report.fee_publisher = 0
        convertion_report.fee_am = 0
        convertion_report.fee_company = 0
      end

      referral_publisher_top = convertion_report.publisher.referral_publisher_top
      if referral_publisher_top.present?
        commision_percent = referral_publisher_top.permission_referral_publisher.commision
        commision = Commision.find_by(convertion_report_id: convertion_report.id)
        commision = commision.blank? ? Commision.new : commision
        if convertion_report.report_items.present?
          commision.convertion_report_id = convertion_report.id
          commision.account_manager_id = convertion_report.account_manager_id
          commision.publisher_id = referral_publisher_top.id
          commision.referral_publisher_id = convertion_report.publisher_id
          commision.report_campaign_snapshot_id = convertion_report.report_campaign_snapshot_id
          commision.category_campaign_id = convertion_report.category_campaign_id
          commision.campaign_id = convertion_report.campaign_id
          commision.campaign_date = convertion_report.report_campaign_snapshot.campaign_date
          commision.revenue = convertion_report.fee_am
          commision.fee_publisher = (commision_percent.to_f / 100) * commision.revenue
          commision.fee_am = commision.revenue - commision.fee_publisher
          commision.status = convertion_report.status
          commision.save
          convertion_report.fee_am = commision.fee_am
        else
          commision.destroy
        end
      end 

      if convertion_report.save
        if convertion_report.status == ConvertionReport::APPROVED && old_status_convertion_report != ConvertionReport::APPROVED
          update_offer_sales(convertion_report.category_campaign)
        end
        create_activity(convertion_report, current_user.id, "Update report item", "#{current_user.username} has update report item to revenue #{helpers.get_currency(convertion_report.revenue)}.")
        today_report = convertion_report.report_campaign_snapshot

        convertion_reports_approved = today_report.convertion_reports.approved
        today_report.sales = convertion_reports_approved.sum(:sales)
        today_report.revenue = convertion_reports_approved.sum(:revenue)
        today_report.fee_publisher = convertion_reports_approved.sum(:fee_publisher)
        today_report.fee_am = convertion_reports_approved.sum(:fee_am)
        today_report.fee_company = convertion_reports_approved.sum(:fee_company)
        today_report.save
      end
    end

    def update_offer_sales(offer)
      offer = offer
      offer.sales = offer.sales + 1
      offer.save
    end

    def params_permit
      params.require(:convertion_report).permit(report_items_attributes: [
                                            :id,
                                            :convertion_report_id,
                                            :report_campaign_snapshot_id,
                                            :category_campaign_id,
                                            :campaign_id,
                                            :publisher_id,
                                            :account_manager_id,
                                            :admin_id,
                                            :name,
                                            :revenue,
                                            :fee_publisher,
                                            :fee_am,
                                            :fee_company,
                                            :status
                                          ])
    end

		def class_name
			@resource_name = 'Report'
			@collection_name = 'Reports'
		end
end