class Admins::BlogsController < Admins::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :admins_dashboards_path
  add_breadcrumb "blogs", :admins_blogs_path

  def index
    @blogs = collection.page(page).per(per_page)
    @collection_size = "(#{@blogs.total_count})"

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    @activities = @blog.activities.latest.page(page).per(per_page)
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @blog = Blog.new
  end

  def create
    @blog = Blog.new params_permit
    @blog.user_id = current_user.id
    if @blog.save
      create_activity(@blog, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@blog.title}")
      redirect_to admins_blog_path(@blog), notice: "Successfully created #{@resource_name}"
    else
      redirect_to admins_blogs_path, alert: "Unsuccessfully created #{@resource_name}, #{@blog.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource
    if @blog.update params_permit
      create_activity(@blog, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} #{@blog.title}")
      redirect_to admins_blog_path(@blog), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@blog.errors.full_messages.join(',')}"
    end
  end

  def destroy
    resource

    if @blog.destroy
      redirect_to admins_blogs_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to admins_blogs_path, alert: "Unsuccessfully delete #{@resource_name}, #{@blog.errors.full_messages.join(',')}"
    end
  end

  def multiple_action
    if params[:ids].present?
      collect = params[:ids]
      if params[:commit] == Blog::ACTIVE
        Blog.where(:id.in => collect).each do |resource|
          resource.status = Blog::ACTIVE
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
        end
      elsif params[:commit] == Blog::INACTIVE
        Blog.where(:id.in => collect).each do |resource|
          resource.status = Blog::INACTIVE
          resource.save
          create_activity(resource, current_user.id, "change status #{@resource_name.downcase} to #{resource.status}", "successfully changed #{@resource_name.downcase} #{resource.title}")
        end
      end
      redirect_to admins_blogs_path, notice: 'Status updated!'
    else
      redirect_to admins_blogs_path, alert: 'Nothing checked!'
    end 
  end

  def change_status
    resource
    @blog.status = params[:blog][:status]
    if @blog.save
      create_activity(@blog, current_user.id, "change status #{@resource_name.downcase} to #{@blog.status}", "successfully changed #{@resource_name.downcase} #{@blog.title}")
      redirect_to admins_blog_path(@blog), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@blog.errors.full_messages.join(',')}"
    end
  end

  private
    def resource
      @blog = Blog.find(params[:id])
    end

    def collection
      @blogs = Blog.latest.search_by(params)
    end

    def params_permit
      params.require(:blog).permit(:title, :description, :category, :attachment_image, :status)
    end

    def class_name
      @resource_name = 'Blog'
      @collection_name = 'Blogs'
    end
end