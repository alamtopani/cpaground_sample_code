class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  
  def facebook   
    init_omni_devise_callback('facebook')
  end

  def google_oauth2
    init_omni_devise_callback('google')
  end

  private
    def init_omni_devise_callback(provider)
      @user = User.find_for_devise_oauth(request.env["omniauth.auth"], current_user)
      case @user
      when :error
        message = 'Username or Email Already Used By Another Account!'
        service_redirect(message)
      when :error_current
        message = "Your #{provider} account has already been used by another user, please use a different #{provider} account!"
        rservice_redirect(message)
      when :error_different
        message = "The account #{provider} who is currently logged in does not match the account #{provider} you registered, please login with the account #{provider} you registered first!"
        service_redirect(message)
      else
        if @user.present?
          sign_in_and_redirect @user, :event => :authentication
          set_flash_message(:notice, :success, :kind => "#{provider}") if is_navigational_format?
        else
          session["devise.#{provider}_data"] = request.env["omniauth.auth"]
          redirect_to new_user_session_path
        end
      end
    end

    def service_redirect(message)
      if current_user.present?
        redirect_to request.referer || edit_account(current_user), alert: message 
      else
        redirect_to new_user_session_path, alert: message 
      end
    end
end