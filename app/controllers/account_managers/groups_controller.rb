class AccountManagers::GroupsController < AccountManagers::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :account_managers_dashboards_path
  add_breadcrumb "groups", "#"

  def show
    resource
    @group_coverages_approve_all = @group.group_coverages.approve.latest.search_by(params)
    @group_coverages_waiting_all = @group.group_coverages.waiting.latest.search_by(params)

    @group_coverages_approve = @group_coverages_approve_all.page(params[:group_coverages_approve]).per(per_page)
    @group_coverages_waiting = @group_coverages_waiting_all.page(params[:group_coverages_waiting]).per(per_page)

    @feeds = @group.feeds.activated.latest.page(page).per(per_page)
    @groups = @account_manager.groups
    @comment_activities = FeedComment.where(feedable_type: FeedComment::FEED, :feedable_id.in => @group.feeds.pluck(:id)).not_in(user_id: current_user.id).latest.limit(50)
  
    add_breadcrumb "#{@group.name}", "#"
  end

  def create
    @group = Group.new params_permit
    @group.user_id = current_user.id
    if @group.save
      create_activity(@group, current_user.id, "create group", "successfully created group #{@group.name}.")
      redirect_to account_managers_group_path(@group), notice: "Successfully created #{@resource_name}"
    else
      redirect_to account_managers_forums_path, alert: "Unsuccessfully created #{@resource_name}, #{@group.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource
    if @group.update params_permit
      redirect_to account_managers_group_path(@group), notice: "Successfully updated #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@group.errors.full_messages.join(',')}"
    end
  end

  def destroy
    resource

    if @group.destroy
      redirect_to account_managers_forums_path, notice: "Successfully delete #{@resource_name}"
    else
      redirect_to :back, alert: "Unsuccessfully delete #{@resource_name}, #{@group.errors.full_messages.join(',')}"
    end
  end

  def change_status_group_coverage
    @group_coverage = GroupCoverage.find_by(id: params[:id])
    @group_coverage.status = params[:status]
    
    if @group_coverage.save
      group = @group_coverage.group
      create_notification(group, @group_coverage.user_id, "Respond your request join group #{group.name}", "Request join group #{group.name} has #{@group_coverage.status}.")
      # redirect_to :back, notice: "Successfully #{params[:button]} publisher."
    else
      # redirect_to :back, alert: "Unsuccessfully #{params[:button]} publisher, #{@group_coverage.errors.full_messages.join(',')}"
    end

    respond_to do |format|
      format.js { render json: { status: 'Success', data: @group_coverage }, status: :ok }
    end
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to account_managers_group_path(params[:id])
  end

  private
    def resource
      @group = Group.find(params[:id])
    end

    def collection
      @groups = @account_manager.groups.latest
    end

    def params_permit
      params.require(:group).permit(:name, :description, :cover, :icon, :user_id, :status)
    end

    def class_name
      @resource_name = 'Group'
      @collection_name = 'Groups'
    end
end