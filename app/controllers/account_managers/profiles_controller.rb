class AccountManagers::ProfilesController < AccountManagers::ApplicationController  
  before_action :class_name
  prepend_before_filter :draw_password, only: :update

  def edit
    resource
  end

  def update
    resource
    if check_acc_payment(@account_manager.id, params_permit)
      if @account_manager.update params_permit
        # if @account_manager.payment_snapshots.process.present?
        #   @account_manager.payment_snapshots.process.each do |resource|
        #     resource.to_acc = @account_manager.profile_account_manager.payment_to
        #     resource.save
        #   end
        # end
        create_activity(@account_manager, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase}.")
        redirect_to edit_account_managers_profile_path(@account_manager), notice: "Successfully updated #{@resource_name}"
      else
        redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@account_manager.errors.full_messages.join(',')}"
      end
    else
      choice_payment_type = get_payment_type_choose(params_permit[:profile_account_manager_attributes][:choice_payment_type])
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{choice_payment_type} has already used."
    end
  end

  private
    def resource
      @account_manager = AccountManager.find(current_user.id)
      @profile_account_manager = @account_manager.profile_account_manager
    end

    def class_name
      @resource_name = 'Profile'
      @collection_name = 'Profiles'
    end

    def draw_password
      %w(password password_confirmation).each do |attr|
        params[:account_manager].delete(attr)
      end if params[:account_manager] && params[:account_manager][:password].blank?
    end

    def params_permit
      params.require(:account_manager).permit(:username, :email, :skype, :avatar, :phone_code, :phone, :verified, :password, :password_confirmation, :account_manager_id,
        profile_account_manager_attributes:[:account_manager_id, :first_name, :last_name, :country, :province, :city, :address, :zip_code, :acc_paypal, :acc_payoneer, :acc_bank, :longitude, :latitude, :choice_payment_type, :bank_name, :acc_name])
    end

    def check_acc_payment(account_manager_id, params_permit)
      params = params_permit[:profile_account_manager_attributes]
      choice_payment_type = params[:choice_payment_type]

      if choice_payment_type.present?
        if choice_payment_type == "1"
          if ProfileAccountManager.where.not(account_manager_id: account_manager_id).where(acc_paypal: params[:acc_paypal]).blank?
            return true
          else
            return false
          end
        elsif choice_payment_type == "2"
          if ProfileAccountManager.where.not(account_manager_id: account_manager_id).where(acc_payoneer: params[:acc_payoneer]).blank?
            return true
          else
            return false
          end
        elsif choice_payment_type == "3"
          if ProfileAccountManager.where.not(account_manager_id: account_manager_id).where(acc_bank: params[:acc_bank]).blank?
            return true
          else
            return false
          end
        end
      else
        return true
      end
    end

    def get_payment_type_choose(type)
      case type
      when "1" then type = ProfileAccountManager::ACC_PAYPAL
      when "2" then type = ProfileAccountManager::ACC_PAYONEER
      when "3" then type = ProfileAccountManager::ACC_BANK
      end
    end
end
