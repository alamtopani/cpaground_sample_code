class AccountManagers::PermissionReferralPublishersController < AccountManagers::ApplicationController
	before_action :check_profile
	before_action :class_name
	before_action :resource, only: [:show, :edit, :update, :destroy]
	before_action :publishers, only: [:new, :edit]
	
	add_breadcrumb "dashboard", :account_managers_dashboards_path
	add_breadcrumb "permission referral publishers", :account_managers_permission_referral_publishers_path

	def index
		@permission_referral_publishers = collection.page(params[:page_permission_referral_publishers]).per(per_page)
	end

	def new
		add_breadcrumb "#{params[:action]}"
		@permission_referral_publisher = PermissionReferralPublisher.new
	end

	def show
		add_breadcrumb "#{params[:action]}"
		@referral_publishers = @permission_referral_publisher.publisher.referral_publishers.latest.page(params[:page_referral_publishers]).per(per_page)
    @offers = CategoryCampaign.active.hot_offers.page(params[:page_list_offers]).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

	def create
		@permission_referral_publisher = PermissionReferralPublisher.new params_permit
		@permission_referral_publisher.account_manager_id = current_user.id
		if @permission_referral_publisher.save
			update_referral_token_publisher(@permission_referral_publisher.publisher, @permission_referral_publisher.referral_token, @permission_referral_publisher.status)
			create_activity(@permission_referral_publisher, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@permission_referral_publisher.publisher.username}")
			send_message(@permission_referral_publisher.publisher, "Now you have referral link, please look at dashboard or referrals menu.") if @permission_referral_publisher.status == "active"
			redirect_to edit_account_managers_permission_referral_publisher_path(@permission_referral_publisher), notice: "Successfully created #{@resource_name}"
		else
			redirect_to :back, alert: "Unsuccessfully created #{@resource_name}, #{@permission_referral_publisher.errors.full_messages.join(',')}"
		end
	end

	def edit
		add_breadcrumb "#{params[:action]}"
    @offers = CategoryCampaign.active.hot_offers.page(params[:page_list_offers]).per(per_page)
	end

	def update
		old_status = @permission_referral_publisher.status
		if @permission_referral_publisher.update params_permit
			update_referral_token_publisher(@permission_referral_publisher.publisher, @permission_referral_publisher.referral_token, @permission_referral_publisher.status)
			create_activity(@permission_referral_publisher, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase} #{@permission_referral_publisher.publisher.username}")
			if old_status != @permission_referral_publisher.status 
				if @permission_referral_publisher.status == "active"
					send_message(@permission_referral_publisher.publisher, "Now you have referral link, please look at dashboard or referrals menu.")
				else
					send_message(@permission_referral_publisher.publisher, "Your referral link is disabled.")
				end
			end
			redirect_to edit_account_managers_permission_referral_publisher_path(@permission_referral_publisher), notice: "Successfully updated #{@resource_name}"
		else
			redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@permission_referral_publisher.errors.full_messages.join(',')}"
		end
	end

	def destroy
		if @permission_referral_publisher.destroy
			redirect_to account_managers_permission_referral_publishers_path, notice: "Successfully delete #{@resource_name}"
		else
			redirect_to account_managers_permission_referral_publishers_path, alert: "Unsuccessfully delete #{@resource_name}, #{@permission_referral_publisher.errors.full_messages.join(',')}"
		end
	end

	private
		def resource
			@permission_referral_publisher = PermissionReferralPublisher.find(params[:id])
		end

		def publishers
			@publishers = @account_manager.publishers.no_referral_publisher.oldest
		end

		def collection
			@permission_referral_publishers = @account_manager.permission_referral_publishers.search_by(params)
		end

		def params_permit
			params.require(:permission_referral_publisher).permit(:account_manager_id, :publisher_id, :referral_token, :commision, :status)
		end

		def class_name
			@resource_name = 'Permission Referral Publisher'
			@collection_name = 'Permission Referral Publishers'
		end

		def update_referral_token_publisher(publisher, referral_token, status)
			publisher = publisher
			publisher.referral_token = referral_token
			publisher.referral_status = status
			publisher.save
		end

		def send_message(publisher, message)
			group_message_id = GroupMessage.where(account_manager_id: publisher.account_manager_id, publisher_id: publisher.id).first
			if group_message_id.blank?
				group_message_id = GroupMessage.create(account_manager_id: publisher.account_manager_id, publisher_id: publisher.id, status: 'active')
			end
			@message = Message.new
			@message.group_message_id = group_message_id
			@message.from = publisher.account_manager_id
			@message.to = publisher.id
			@message.message = message
			@message.save
		end
	
end