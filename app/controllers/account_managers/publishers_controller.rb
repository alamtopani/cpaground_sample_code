class AccountManagers::PublishersController < AccountManagers::ApplicationController
	before_action :class_name

  add_breadcrumb "dashboard", :account_managers_dashboards_path
  add_breadcrumb "publishers", :account_managers_publishers_path

  def index
    if params[:export] == 'true'
      @total_publishers = collection
    end
    
    @publishers = collection.page(params[:publishers_list]).per(6)
    @publishers_waiting_all = @account_manager.publishers.pending.confirmed.waiting_search_by(params).latest
    @publishers_waiting = @publishers_waiting_all.page(params[:publishers_waiting]).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "messages"
    resource
    if @publisher.account_manager_id == current_user.id
      @profile = @publisher.profile
      check_group_message_publisher(@account_manager.id, @publisher.id)

      if @latest_messages.where(from: @publisher.id).present?
        @latest_messages.where(from: @publisher.id).each do |m|
          m.status = 'read'
          m.save
        end
      end
    else
      redirect_to :back, alert: "You can not access this page because he/she is not your publisher."
    end
  end

  def detail_report
    add_breadcrumb "reports"
    resource

    # ---------- CGAWARDTRIP2018 -----------
    @cgaward = ReportCampaignSnapshot.where(publisher_id: @publisher.id).where(:sales.gte => 1)
                      .where(:campaign_date.gte => 'Mon, 1 Mar 2018'.to_date, :campaign_date.lte => @date)
                      .sum(:fee_publisher)
    @cgaward_percent = '%.4f' % (('%.4f' % (@cgaward.to_f/10000)).to_f * 100)
    # --------------------------------------

    get_analytic_report(@publisher, 'dashboard_am_publisher')
    if @convertion_reports.present?
      set_collection_convertion_report(params[:group_by], @convertion_reports, "publisher")
      # multipe_chart(@convertion_reports_list)
    end

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def click_message
    message = Message.find(params[:message_id])
    message.status = "read"
    message.save

    redirect_to account_managers_publisher_path(params[:id])
  end

  def change_status
    @publisher = Publisher.find_by(id: params[:id])
    @publisher.verified = true
    if Rails.env.production?
      @publisher.admin_id = Admin.find_by(slug: 'winda').id
    else
      @publisher.admin_id = Admin.last.id
    end

    if @publisher.save
      verification_snapshot(@publisher)
      create_activity(@publisher, current_user.id, "verification publisher", "successfully verification publisher #{@publisher.username}")
      UserMailer.publisher_approved(@publisher).deliver_now! if @publisher.is_verified?
    else

    end

    respond_to do |format|
      format.js { render json: { status: 'Success', data: @publisher }, status: :ok }
    end
  end

  def suspended
    @publisher = Publisher.find_by(id: params[:id])
    @publisher.suspended = params[:publisher][:suspended]
    @publisher.suspended_message = params[:publisher][:suspended_message] if params[:publisher][:suspended_message].present?

    if @publisher.save
      if @publisher.suspended?
        create_activity(@publisher, current_user.id, "suspended publisher", "successfully suspended publisher #{@publisher.username}")
        UserMailer.publisher_suspended(@publisher).deliver_now!
      else
        @publisher.suspended_message = nil
        @publisher.save
        create_activity(@publisher, current_user.id, "activated publisher", "successfully activated publisher #{@publisher.username}")
        UserMailer.publisher_activated(@publisher).deliver_now!
      end
    else

    end

    respond_to do |format|
      format.js { render json: { status: 'Success', data: @publisher }, status: :ok }
    end
  end

  private
    def resource
      @publisher = Publisher.find params[:id]
    end

    def collection
      @publishers = @account_manager.publishers.verified.search_by(params)
    end

    def class_name
      @resource_name = 'Publisher'
      @collection_name = 'Publishers'
    end

    def verification_snapshot(publisher)
      verification_snapshot = VerificationSnapshot.new
      verification_snapshot.publisher_id = publisher.id
      verification_snapshot.admin_id = publisher.admin_id
      verification_snapshot.account_manager_id = current_user.id
      verification_snapshot.status = VerificationSnapshot::ACTIVE
      verification_snapshot.message = "#{current_user.username}(AM) has change verification status to #{verification_snapshot.status}."
      verification_snapshot.save
    end
end