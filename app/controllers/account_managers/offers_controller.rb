class AccountManagers::OffersController < AccountManagers::ApplicationController
  before_action :check_profile
  before_action :class_name

  add_breadcrumb "dashboard", :account_managers_dashboards_path
  add_breadcrumb "offers", :account_managers_offers_path

  def index
    @offers = collection.page(page).per(per_page)
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    resource
    @approve_offers = @offer.access_offers.where(account_manager_id: current_user.id).approved.search_by(params).page(params[:page_approve]).per(per_page)
    @banned_offers = @offer.access_offers.where(account_manager_id: current_user.id).banned.search_by(params).page(params[:page_banned]).per(per_page)
  end

  def checked
    @access_offer = AccessOffer.find(params[:id])
    @access_offer.checked = true
    @access_offer.save
    @offer = @access_offer.offer
    create_activity(@offer, current_user.id, "Checked offer '#{@offer.name}' with code #{@offer.code}", "#{current_user.username} have successfully checked/approved offer '#{@offer.name}' with code #{@offer.code} submitted by #{@access_offer.publisher.username}.")
    create_notification(@offer, @access_offer.publisher_id, "Your applied offer '#{@offer.name} with code #{@offer.code}' has #{@access_offer.status}", "Now you can do get offer.")
    UserMailer.approve_access_offer(@access_offer).deliver_now!

    respond_to do |format|
      format.js { render json: { status: 'Success', data: @access_offer }, status: :ok }
    end
  end

  def change_status
    @access_offer = AccessOffer.find(params[:id])
    @access_offer.status = params[:access_offer][:status]

    if @access_offer.save
      @offer = @access_offer.offer
      create_activity(@offer, current_user.id, "Change status applied offer '#{@offer.name}' with code #{@offer.code}", "#{current_user.username} have successfully change status offer '#{@offer.name}' with code #{@offer.code} to #{@access_offer.status} submitted by #{@access_offer.publisher.username}.")
      create_notification(@offer, @access_offer.publisher_id, "Your applied offer '#{@offer.name}' with code #{@offer.code} has #{@access_offer.status}", "Your applied offer '#{@offer.name}' with code #{@offer.code} has change status to #{@access_offer.status}.")
      if @access_offer.status == AccessOffer::APPROVE
        campaigns = Campaign.all_of({publisher_id: @access_offer.publisher_id}, {category_campaign_id: @access_offer.category_campaign_id})
        if campaigns.present?
          campaigns.each do |resource|
            resource.banned = false
            resource.save
          end
        end 
        UserMailer.approve_access_offer(@access_offer).deliver_now!
      else
        campaigns = Campaign.all_of({publisher_id: @access_offer.publisher_id}, {category_campaign_id: @access_offer.category_campaign_id})
        if campaigns.present?
          campaigns.each do |resource|
            resource.banned = true
            resource.save
          end
        end 
        UserMailer.banned_access_offer(@access_offer).deliver_now!
      end
      # create_notification(@access_offer, @access_offer.publisher_id, "Offer #{@access_offer.offer.name}", "Status offer #{@access_offer.offer.name} has #{@@access_offer.status}.")
      
    else
      
    end

    respond_to do |format|
      format.js { render json: { status: 'Success', data: @access_offer }, status: :ok }
    end
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to account_managers_offer_path(params[:id])
  end


  private
    def resource
      @offer = CategoryCampaign.find(params[:id])
    end

    def collection
      @offers = CategoryCampaign.active.need_approval.latest.search_by(params)
    end

    def class_name
      @resource_name = 'Offer'
      @collection_name = 'Offers'
    end
end