class AccountManagers::BlogsController < AccountManagers::ApplicationController
  before_action :check_profile
  
  add_breadcrumb "dashboard", :account_managers_dashboards_path
  add_breadcrumb "blogs", :account_managers_blogs_path

  def index
    blogs = Blog.activated
    
    if params[:user].present?
      blogs = Blog.where(user_id: current_user.id)
    else
      blogs = blogs
    end

    @blogs = blogs.latest.page(page).per(per_page)
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    @latest_blogs = Blog.not_in(id: @blog.id).activated.latest.limit(5)
    @blog_comments = @blog.feed_comments.latest.page(page).per(5)

    @blog.update(view_count: @blog.view_count + 1)
  end

  def create
    @blog = Blog.new params_permit
    @blog.user_id = current_user.id
    if @blog.save
      create_activity(@blog, current_user.id, "create blog", "successfully created blog #{@blog.title}")
      redirect_to account_managers_blog_path(@blog), notice: "Successfully created blog"
    else
      redirect_to account_managers_blogs_path, alert: "Unsuccessfully created blog, #{@blog.errors.full_messages.join(',')}"
    end
  end

  def edit
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource
    if @blog.update params_permit
      redirect_to account_managers_blog_path(@blog), notice: "Successfully updated blog"
    else
      redirect_to :back, alert: "Unsuccessfully updated blog, #{@blog.errors.full_messages.join(',')}"
    end
  end

  def comment_create
    @feed_comment = FeedComment.create(user_id: current_user.id, message: params[:message], feedable_id: params[:feedable_id], feedable_type: params[:feedable_type])
    
    respond_to do |format|
      format.js { render json: { status: 'Success', user_username: @feed_comment.user.username, user_avatar: @feed_comment.user.avatar.url(:thumb), data: @feed_comment}, status: :ok }
    end
  end

  private
    def resource
      @blog = Blog.find params[:id]
    end

    def params_permit
      params.require(:blog).permit(:title, :description, :category, :attachment_image, :status)
    end
  
end