class AccountManagers::ForumsController < AccountManagers::ApplicationController
  before_action :check_profile
  
  add_breadcrumb "dashboard", :account_managers_dashboards_path
  add_breadcrumb "forums", :account_managers_forums_path

  def index
    if params[:user].present?
      feeds = Feed.public.activated.latest.where(user_id: current_user.id)
    else
      feeds = Feed.public.activated.latest
    end

    @total_collection = feeds.count
    @feeds = feeds.page(page).per(per_page)
    @groups = @account_manager.groups
    feed_ids = Feed.public.pluck(:id).map{|id| {feedable_id: id}}
    @comment_activities = FeedComment.where(feedable_type: FeedComment::FEED).any_of(feed_ids).not_in(user_id: current_user.id).latest.limit(50)
  end

  def show
    @feed = Feed.activated.find(params[:id])
    feed_ids = Feed.public.pluck(:id).map{|id| {feedable_id: id}}
    @comment_activities = FeedComment.where(feedable_type: FeedComment::FEED).any_of(feed_ids).not_in(user_id: current_user.id).latest.limit(50)
  end

  def delete_comment
    @feed_comment = FeedComment.find_by(id: params[:id])
    if @feed_comment.destroy
      redirect_to :back, notice: "Successfully delete comment."
    else
      redirect_to :back, alert: "Unsuccessfully delete comment, #{@feed_comment.errors.full_messages.join(',')}"
    end
  end

  def destroy
    @feed = Feed.find_by(id: params[:id])

    if @feed.destroy
      redirect_to :back, notice: "Successfully delete feed."
    else
      redirect_to :back, alert: "Unsuccessfully delete feed, #{@feed.errors.full_messages.join(',')}"
    end
  end
  
end