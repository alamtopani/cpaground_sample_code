class AccountManagers::MessagesController < AccountManagers::ApplicationController

  add_breadcrumb "dashboard", :account_managers_dashboards_path
  add_breadcrumb "messages", :account_managers_messages_path

  def index
    @total_collection = Message.where(to: current_user.id).count
    collection = @latest_messages_collection.sort_by {|e| e[4]}
    @messages = Kaminari.paginate_array(collection.reverse!).page(page).per(per_page)
  end

  def create
    create_message(params[:group_message_id], params[:send_to], params[:message])

    respond_to do |format|
      if @message.save
        check_group_message_publisher(current_user.id, params[:send_to])
        format.html
        format.js
      end
    end
  end

  def blast_message
    publishers = params[:tags]
    message = params[:message][:message]
    if publishers.include? "all"
      @my_publishers.each do |publisher|
        group_message_id = GroupMessage.where(account_manager_id: current_user.id, publisher_id: publisher.id).first
        create_message(group_message_id, publisher.id, message)
        @message.save
      end
    else
      publishers.each do |publisher_id|
        publisher = Publisher.find publisher_id
        group_message_id = GroupMessage.where(account_manager_id: current_user.id, publisher_id: publisher.id).first
        create_message(group_message_id, publisher.id, message)
        @message.save
      end
    end
    redirect_to :back, notice: "Successfully send message"
  end

  private
    def create_message(group_message_id, send_to, message)
      @message = Message.new
      @message.group_message_id = group_message_id
      @message.from = current_user.id
      @message.to = send_to
      @message.message = message
    end
end