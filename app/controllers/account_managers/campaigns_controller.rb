class AccountManagers::CampaignsController < AccountManagers::ApplicationController
  before_action :class_name
  before_action :check_profile

  add_breadcrumb "dashboard", :account_managers_dashboards_path
  add_breadcrumb "publisher campaigns", :account_managers_campaigns_path

  def index
    @campaigns = collection.page(page).per(per_page)
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    redirect_to :back, alert: "You can not enter this page, your Campaign '#{@campaign.title}' is still not approved" unless @campaign.is_active? || @campaign.is_complete?
    @landing_pages = @campaign.request_landing_pages.latest.page(params[:page_landing_pages]).per(per_page) if @campaign.request_landing_pages.present?
    
    get_analytic_report(@campaign.publisher, 'dashboard_am_campaign')
    @offer = @campaign.category_campaign
    @banner_images = @campaign.category_campaign.banner_images
    
    if @convertion_reports.present?
      set_collection_convertion_report(params[:group_by], @convertion_reports, "publisher")
      # multipe_chart(@convertion_reports_list)
    end
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to account_managers_campaign_path(params[:id])
  end

  def approvement
    resource
    @campaign.status = Campaign::ACTIVE
    if @campaign.save
      UserMailer.after_update_campaign(@campaign).deliver_now!
      create_activity(@campaign, current_user.id, "change status #{@resource_name.downcase} to #{@campaign.status}", "successfully changed #{@resource_name.downcase} #{@campaign.title}")
      create_activity(@account_manager, current_user.id, "change status #{@resource_name.downcase} to #{@campaign.status}", "successfully changed #{@resource_name.downcase} #{@campaign.title}")
      create_notification(@campaign, @campaign.publisher_id, "Your #{@resource_name.downcase} #{@campaign.title}", "Your #{@resource_name.downcase} #{@campaign.title} has approved.")
      create_notification(@campaign, @campaign.admin_id, "#{@campaign.publisher.username.capitalize} #{@resource_name.downcase} #{@campaign.title}", "#{@campaign.publisher.username.capitalize} #{@resource_name.downcase} #{@campaign.title} has approved by #{@campaign.account_manager.username}.")
    else
    end

    respond_to do |format|
      format.js { render json: { status: 'Success', data: @campaign }, status: :ok }
    end
  end

  private
    def resource
      @campaign = @account_manager.campaigns.find(params[:id])
    end

    def collection
      @campaigns = @account_manager.campaigns.latest.search_by(params)
    end

    def class_name
      @resource_name = 'Campaign'
      @collection_name = 'Campaigns'
    end
end