class AccountManagers::RequestLandingPagesController < AccountManagers::ApplicationController
  before_action :class_name
  before_action :resource, only: [:show]

  add_breadcrumb "dashboard", :account_managers_dashboards_path
  add_breadcrumb "lp publishers", :account_managers_request_landing_pages_path
  
  def index
    @request_landing_pages = collection.page(page).per(per_page)

    respond_to do |format|
      format.html
      format.xls
      format.js
    end 
  end

  def show
    add_breadcrumb "#{params[:action]}"
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to account_managers_request_landing_page_path(params[:id])
  end

  private
    def resource
      @request_landing_page = RequestLandingPage.where(account_manager_id: current_user.id).find params[:id]
    end

    def collection
      @request_landing_pages = RequestLandingPage.where(account_manager_id: current_user.id).latest.search_by(params)
    end

    def class_name
      @resource_name = 'Request Landing Page'
      @collection_name = 'Request Landing Pages'
    end
  
end