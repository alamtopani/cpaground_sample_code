class AccountManagers::NotificationsController < AccountManagers::ApplicationController
  
  add_breadcrumb "dashboard", :account_managers_dashboards_path
  add_breadcrumb "notifications", :account_managers_notifications_path

  def index
    collection = Notification.latest.where(user_id: @account_manager.id)
    @total_collection = collection.count
    @notifications = collection.page(page).per(per_page)
  end
end