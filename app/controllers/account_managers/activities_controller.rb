class AccountManagers::ActivitiesController < AccountManagers::ApplicationController
  before_action :check_profile
  
  add_breadcrumb "dashboard", :account_managers_dashboards_path
  add_breadcrumb "my activities", :account_managers_activities_path

  def index
    @activities = @account_manager.activities.latest.search_by(params).page(page).per(per_page)
  end
  
end