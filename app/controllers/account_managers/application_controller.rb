class AccountManagers::ApplicationController < ApplicationController
  before_action :authenticate_user!

  layout "application_account_manager"
  def authenticate_user!
    unless user_signed_in? && current_user.is_account_manager?
      redirect_to new_user_session_path, alert: "Cannot access this site!"
    else
      @account_manager = AccountManager.find current_user
      @my_publishers = @account_manager.publishers.verified.order(username: :asc)
      ongoing_payment(@account_manager)
      notification
      check_group_message(current_user.id)
    end
  end

  def notification
    @notifications = Notification.sort_unread.latest.where(user_id: @account_manager.id)
  end

  def check_group_message(account_manager_id)
    @group_message = GroupMessage.where(account_manager_id: current_user.id).first

    if @group_message.blank?
      @group_message = GroupMessage.create(account_manager_id: current_user.id, status: 'active')
    end

    if @group_message.present?
      messages = Message.where(to: current_user.id)
      @latest_messages = messages.latest
      latest_messages = messages
      messages_collection = latest_messages.group_by {|m| m.from}
      set_messages_collection = set_collection_message(messages_collection)
      latest_messages_collection = set_messages_collection.sort_by {|e| e[3]}
      @latest_messages_collection = latest_messages_collection.reverse!
    end
  end

  def check_group_message_publisher(account_manager_id, publisher_id)
    @group_message = GroupMessage.where(account_manager_id: current_user.id, publisher_id: publisher_id).first

    if @group_message.blank?
      @group_message = GroupMessage.create(account_manager_id: current_user.id, publisher_id: publisher_id, status: 'active')
    end

    if @group_message.present?
      @publisher_messages = @group_message.messages.oldest
    end
  end

  private
    def set_collection_message(messages)
      messages.values.map{|w| [w.map(&:id).last,
                            w.map(&:from).last,
                            w.map(&:message).last,
                            w.map(&:status).last,
                            w.map(&:created_at).last] 
                          }
    end

end