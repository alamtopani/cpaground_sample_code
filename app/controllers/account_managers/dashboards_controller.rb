class AccountManagers::DashboardsController < AccountManagers::ApplicationController
	before_action :check_profile
  
  def index
    offers = CategoryCampaign.active.latest
    @new_offers = offers.limit(4)
    @top_offers = offers.hot_offers.limit(4)
    @promo_offers = offers.promo.limit(4)

    get_analytic_report(@account_manager, 'dashboard_am')

    @publishers_count = @account_manager.publishers.verified.no_suspended.count
    @publishers_campaign_count = @account_manager.campaigns.count
    @publishers_lp = @account_manager.request_landing_pages.count
    @have_recieve_payment = PaymentSnapshot.where(user_id: current_user.id).paid.count
    
    @information = Information.actived.latest.last if Information.actived.present?

    # if @convertion_reports.present?
    #   multipe_chart(@convertion_reports)
    # end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def ranking
    @ranking_am = ReportCampaignSnapshot.where(:sales.gte => 1).search_by_campaign_date(params).group_by{|r| r.account_manager_id}.map{|am| am}.map{|am| [am[1].map(&:sales).reduce(:+), AccountManager.find(am[0]).username, AccountManager.find(am[0]).the_name?, Campaign.active.where(account_manager_id: am[0]).count]}.sort_by {|am| am[0]}.reverse
  end
end