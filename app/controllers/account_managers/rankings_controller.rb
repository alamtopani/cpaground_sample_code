class AccountManagers::RankingsController < AccountManagers::ApplicationController
	before_action :check_profile

  def index
    @date = Time.zone.now.to_date
    reports = ReportCampaignSnapshot.where(:sales.gte => 1).search_by_campaign_date(params)
    if params[:campaign_start_at].blank? && params[:campaign_end_at].blank?
      reports = reports.where(:campaign_date.gte => @date.beginning_of_month, :campaign_date.lte => @date)
    end
    # @ranking_am = reports.group_by{|r| r.account_manager_id}.map{|am| am}.map{|am| [ AccountManager.find(am[0]).the_name?, am[1].map(&:sales).reduce(:+)]}.sort_by {|am| am[1]}.reverse
    @ranking_am_by_revenue = reports.group_by{|r| r.account_manager_id}.map{|am| am}.map{|am| [ AccountManager.find(am[0]).the_name?, am[1].map(&:revenue).reduce(:+)]}.sort_by {|am| am[1]}.reverse
  end
end