class Publishers::PaymentsController < Publishers::ApplicationController
  before_action :check_profile

  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "payments", :publishers_payments_path
  
  def index
    @payments = @publisher.payment_snapshots.for_publisher.latest.search_by(params).page(page).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
      format.pdf do
        render pdf: "#{@resource_name} - #{@payment_snapshot.id}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    @payment = PaymentSnapshot.find params[:id]

    respond_to do |format|
      format.html 
      format.xls
      format.js
      format.pdf do
        render pdf: "Payment - #{@payment.id}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end

    # redirect_to :back
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to publishers_payment_path(params[:id])
  end
end