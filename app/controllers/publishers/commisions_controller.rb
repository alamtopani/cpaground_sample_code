class Publishers::CommisionsController < Publishers::ApplicationController
  before_action :class_name, :check_link_referral
  
  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "commisions", :publishers_commisions_path

  def index
    collection
    @total_collection = @commisions.count rescue 0
    @fee_approved = @commisions.approved.sum(:fee_publisher)
    @fee_pending = @commisions.pending.sum(:fee_publisher)
    @fee_rejected = @commisions.rejected.sum(:fee_publisher)
    query_last_next_payment_commisions(@commisions)
    commisions = @commisions.where(status: "approved")
    @commision_today_so_far = commisions.where(campaign_date: @date).sum(:fee_publisher) rescue 0
    @commision_yesterday = commisions.where(campaign_date: @date.yesterday).sum(:fee_publisher) rescue 0
    @commision_last_7_month = commisions.where(:campaign_date.gte => @date - 6.days, :campaign_date.lte => @date).sum(:fee_publisher) rescue 0
    @commision_this_month = commisions.where(:campaign_date.gte => @date.beginning_of_month, :campaign_date.lte => @date.end_of_month).sum(:fee_publisher) rescue 0

    unless params[:start_at].present? && params[:end_at].present?
      if @commisions.present?
        @commisions = @commisions.where(:campaign_date.gte => @date.beginning_of_month).where(:campaign_date.lte => @date)
      end
    else
      @commisions = @commisions
    end

    @commisions_group = @commisions.group_by {|d| d.campaign_date} rescue nil 

    @commisions = @commisions.page(params[:page_commisions]).per(per_page) rescue nil 
  end

  private
    def collection
      @commisions = @publisher.commisions.search_by(params)
    end

    def class_name
      @resource_name = 'Commision'
      @collection_name = 'Commisions'
    end

    def check_link_referral
      unless @publisher.have_link_referral?
        redirect_to publishers_dashboards_path, alert: "permission denied!"
      end 
    end
end