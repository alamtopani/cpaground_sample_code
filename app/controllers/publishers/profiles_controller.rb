class Publishers::ProfilesController < Publishers::ApplicationController  
  before_action :class_name
  prepend_before_filter :draw_password, only: :update

  def edit
    resource
  end

  def update
    resource
    if check_acc_payment(@publisher.id, params_permit)
      if @publisher.update params_permit
        # if @publisher.payment_snapshots.process.present?
        #   @publisher.payment_snapshots.process.each do |resource|
        #     resource.to_acc = @publisher.profile.payment_to
        #     resource.save
        #   end
        # end
        create_activity(@publisher, current_user.id, "update #{@resource_name.downcase}", "successfully updated #{@resource_name.downcase}.")
        redirect_to edit_publishers_profile_path(@publisher), notice: "Successfully updated #{@resource_name}"
      else
        redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@publisher.errors.full_messages.join(',')}"
      end
    else
      choice_payment_type = get_payment_type_choose(params_permit[:profile_attributes][:choice_payment_type])
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{choice_payment_type} has already used."
    end
  end

  private
    def resource
      @publisher = Publisher.find(current_user.id)
      @profile = @publisher.profile
    end

    def class_name
      @resource_name = 'Profile'
      @collection_name = 'Profiles'
    end

    def draw_password
      %w(password password_confirmation).each do |attr|
        params[:publisher].delete(attr)
      end if params[:publisher] && params[:publisher][:password].blank?
    end

    def params_permit
      params.require(:publisher).permit(:facebook_url, :skype, :avatar, :phone_code, :phone, :verified, :password, :password_confirmation, :account_manager_id, :id_card, :foto_id_card, :selfi_id_card,
        profile_attributes:[:publisher_id, :first_name, :last_name, :country, :province, :city, :address, :zip_code, :acc_paypal, :acc_payoneer, :acc_bank, :longitude, :latitude, :choice_payment_type, :bank_name, :acc_name])
    end

    def check_acc_payment(publisher_id, params_permit)
      params = params_permit[:profile_attributes]
      choice_payment_type = params[:choice_payment_type]

      if choice_payment_type.present?
        if choice_payment_type == "1"
          if Profile.where.not(publisher_id: publisher_id).where(acc_paypal: params[:acc_paypal]).blank?
            return true
          else
            return false
          end
        elsif choice_payment_type == "2"
          if Profile.where.not(publisher_id: publisher_id).where(acc_payoneer: params[:acc_payoneer]).blank?
            return true
          else
            return false
          end
        elsif choice_payment_type == "3"
          if Profile.where.not(publisher_id: publisher_id).where(acc_bank: params[:acc_bank]).blank?
            return true
          else
            return false
          end
        end
      else
        return true
      end
    end

    def get_payment_type_choose(type)
      case type
      when "1" then type = Profile::ACC_PAYPAL
      when "2" then type = Profile::ACC_PAYONEER
      when "3" then type = Profile::ACC_BANK
      end
    end
end
