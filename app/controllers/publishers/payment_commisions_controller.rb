class Publishers::PaymentCommisionsController < Publishers::ApplicationController
  before_action :check_link_referral

  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "payment commisions", :publishers_payment_commisions_path
  
  def index
    collection = @publisher.payment_commisions.for_publisher.latest.search_by(params)
    @total_collection = collection.count
    @payment_commisions = collection.page(page).per(per_page)

    respond_to do |format|
      format.html 
      format.xls
      format.js
      format.pdf do
        render pdf: "#{@resource_name} - #{@payment_commision.id}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    @payment_commision = PaymentCommision.find params[:id]

    respond_to do |format|
      format.html 
      format.xls
      format.js
      format.pdf do
        render pdf: "Payment - #{@payment_commision.id}",
        layout: 'layouts/print_layout.html.erb',
        show_as_html: params[:debug].present?
      end
    end

    # redirect_to :back
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to publishers_payment_commision_path(params[:id])
  end

  private
    def check_link_referral
      unless @publisher.have_link_referral?
        redirect_to publishers_dashboards_path, alert: "permission denied!"
      end 
    end
end