class Publishers::OfferCommisionsController < Publishers::ApplicationController
  before_action :class_name, :check_link_referral

  def index
    collection = CategoryCampaign.active.hot_offers
    @total_collection = collection.count rescue 0
    @offers = collection.page(params[:page_list_offers]).per(per_page)
    @permission_referral_publisher = @publisher.permission_referral_publisher
  end
  
  private
    def class_name
      @resource_name = 'Offer Commision'
      @collection_name = 'Offer Commisions'
    end

    def check_link_referral
      unless @publisher.have_link_referral?
        redirect_to publishers_dashboards_path, alert: "permission denied!"
      end 
    end
  
end 