class Publishers::DashboardsController < Publishers::ApplicationController
	before_action :check_profile
  
	def index
    offers = CategoryCampaign.active
    @new_offers = offers.latest.limit(3)
    @top_offers = offers.hot_offers.limit(3)
    @promo_offers = offers.promo.updated.limit(3)
    
    # ---------- CGAWARDTRIP2018 -----------
    @cgaward = ReportCampaignSnapshot.where(publisher_id: @publisher.id).where(:sales.gte => 1)
                      .where(:campaign_date.gte => 'Mon, 1 Mar 2018'.to_date, :campaign_date.lte => @date)
                      .sum(:fee_publisher)
    @cgaward_percent = '%.4f' % (('%.4f' % (@cgaward.to_f/10000)).to_f * 100)
    # --------------------------------------

    get_analytic_report(@publisher, 'dashbord_publisher')

    @publishers_campaign_count = @publisher.campaigns.count
    @publishers_lp = @publisher.request_landing_pages.count
    @have_recieve_payment = PaymentSnapshot.where(user_id: current_user.id).paid.count

    @information = Information.actived.latest.last if Information.actived.present?

    # if @convertion_reports.present?
    #   multipe_chart(@convertion_reports)
    # end

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
	end
end