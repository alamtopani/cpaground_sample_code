class Publishers::NotificationsController < Publishers::ApplicationController

  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "notifications", :publishers_notifications_path

  def index
    collection = Notification.latest.where(user_id: @publisher.id)
    @total_collection = collection.count
    @notifications = collection.page(page).per(per_page)
  end
end