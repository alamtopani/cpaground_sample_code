class Publishers::ApplicationController < ApplicationController
  before_action :authenticate_user!

  layout "application_publisher"
  def authenticate_user!
    if user_signed_in? && current_user.verified == false
      redirect_to root_path, alert: "Your account will be activated after the verification process of our team, please wait for further information from us!"
    else
      unless user_signed_in? && current_user.is_publisher?
        redirect_to new_user_session_path, alert: "Cannot access this site!"
      else
        @publisher = Publisher.find_by(id: current_user.id)
        unless current_user.suspended?
          ongoing_payment(@publisher)
          notification
          check_group_message(current_user.id, current_user.account_manager_id)
        else
          skype = @publisher.account_manager.skype
          sign_out(current_user)
          redirect_to new_user_session_path, alert: "Your account is suspended because '#{@publisher.suspended_message}' please contact your manager on this skype id #{skype} to recover it"
        end
      end
    end
  end

  def notification
    @notifications = Notification.sort_unread.latest.where(user_id: @publisher.id)
  end

  def check_group_message(publisher_id, account_manager_id)
    @group_message = GroupMessage.where(publisher_id: current_user.id, account_manager_id: account_manager_id).first

    if @group_message.blank?
      @group_message = GroupMessage.create(publisher_id: current_user.id, account_manager_id: account_manager_id, status: 'active')
    end

    if @group_message.present?
      @messages = @group_message.messages.oldest
      @latest_messages = @group_message.messages.where(to: current_user.id).latest
    end
  end

end