class Publishers::ActivitiesController < Publishers::ApplicationController
  before_action :check_profile

  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "my activities", :publishers_activities_path
  
  def index
    @activities = @publisher.activities.latest.search_by(params).page(page).per(per_page)
  end
  
end