class Publishers::BlogsController < Publishers::ApplicationController
	before_action :check_profile

	add_breadcrumb "dashboard", :publishers_dashboards_path
	add_breadcrumb "blogs", :publishers_blogs_path
	
	def index
		@blogs = Blog.activated.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{params[:action]}"
		@blog = Blog.find params[:id]
		@latest_blogs = Blog.not_in(id: @blog.id).activated.latest.limit(5)
		@blog_comments = @blog.feed_comments.latest.page(page).per(5)

		@blog.update(view_count: @blog.view_count + 1)
	end

	def comment_create
		@feed_comment = FeedComment.create(user_id: current_user.id, message: params[:message], feedable_id: params[:feedable_id], feedable_type: params[:feedable_type])
		
		respond_to do |format|
			format.js { render json: { status: 'Success', user_username: @feed_comment.user.username, user_avatar: @feed_comment.user.avatar.url(:thumb), data: @feed_comment}, status: :ok }
		end
	end
	
end