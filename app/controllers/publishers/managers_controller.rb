class Publishers::ManagersController < Publishers::ApplicationController
  before_action :check_profile
  
  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "my manager", :publishers_managers_path

  def index
    manager = @publisher.account_manager
    @manager = manager
    @profile_manager = manager.profile_account_manager

    if @latest_messages.present?
      @latest_messages.each do |m|
        m.status = 'read'
        m.save
      end
    end
  end
end