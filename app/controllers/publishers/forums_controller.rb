class Publishers::ForumsController < Publishers::ApplicationController
  before_action :check_profile
  
  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "forums", :publishers_forums_path

  def index
    if params[:user].present?
      feeds = Feed.public.activated.latest.where(user_id: current_user.id)
    else
      feeds = Feed.public.activated.latest
    end

    @total_collection = feeds.count
    @feeds = feeds.page(page).per(per_page)
    @groups = @publisher.groups
    feed_ids = Feed.public.pluck(:id).map{|id| {feedable_id: id}}
    @comment_activities = FeedComment.where(feedable_type: FeedComment::FEED).any_of(feed_ids).not_in(user_id: current_user.id).latest.limit(50)
  end

  def show
    @feed = Feed.activated.find(params[:id])
    feed_ids = Feed.public.pluck(:id).map{|id| {feedable_id: id}}
    @comment_activities = FeedComment.where(feedable_type: FeedComment::FEED).any_of(feed_ids).not_in(user_id: current_user.id).latest
  end
end