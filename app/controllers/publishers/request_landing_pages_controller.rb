class Publishers::RequestLandingPagesController < Publishers::ApplicationController
  before_action :check_profile, :class_name
  
  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "landing pages", :publishers_request_landing_pages_path

  def index
    @request_landing_pages = @publisher.request_landing_pages.latest.search_by(params).page(page).per(per_page)
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @request_landing_page = RequestLandingPage.new
    @request_landing_page.category_id = params[:category_id] if params[:category_id].present?
  end

  def create
    @request_landing_page = RequestLandingPage.new params_permit
    @request_landing_page.publisher_id = current_user.id
    @request_landing_page.admin_id = current_user.admin_id
    @request_landing_page.account_manager_id = current_user.account_manager_id
    @request_landing_page.domain = LayoutTheme.find_by(id: @request_landing_page.layout_theme_id).link_preview
    @request_landing_page.link_campaign = @request_landing_page.campaign.tracking_code+"&s3="+@request_landing_page.layout_theme_id
    if @request_landing_page.save
      if @request_landing_page.domain.include? "//"
        domain = @request_landing_page.domain.split("//")
        @request_landing_page.domain = domain[0]+"//"+@request_landing_page.try(:code).downcase+'.'+domain[1]
      else
        domain = @request_landing_page.domain
        @request_landing_page.domain = "http://"+@request_landing_page.try(:code).downcase+'.'+domain
      end
      @request_landing_page.status = RequestLandingPage::ACTIVE
      @request_landing_page.save
      # UserMailer.after_review_request_lp(@request_landing_page).deliver_now!
      # UserMailer.after_request_lp(@request_landing_page).deliver_now!
      # UserMailer.after_request_lp_admin(@request_landing_page).deliver_now!
      # create_notification(@request_landing_page, @request_landing_page.publisher.admin_id, "#{@request_landing_page.publisher.username} has request #{@resource_name.downcase} for #{@request_landing_page.campaign.title}", "#{@request_landing_page.publisher.username} has #{@resource_name.downcase} for #{@request_landing_page.campaign.title} please review.")
      create_notification(@request_landing_page, @request_landing_page.publisher_id, "Your #{@resource_name.downcase} for #{@request_landing_page.campaign.title}", "Your #{@resource_name.downcase} for #{@request_landing_page.campaign.title} has #{@request_landing_page.status}.")
      create_notification(@request_landing_page, @request_landing_page.account_manager_id, "#{@request_landing_page.publisher.username} #{@resource_name.downcase} for #{@request_landing_page.campaign.title}", "#{@request_landing_page.publisher.username} #{@resource_name.downcase} for #{@request_landing_page.campaign.title} has #{@request_landing_page.status}.")
      create_activity(@request_landing_page, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@request_landing_page.category[:name]}")
      redirect_to publishers_request_landing_page_path(@request_landing_page), notice: "Successfully request landing page, please input the script code from histats or google analytic for tracking your landing page and click save"
    else
      redirect_to publishers_request_landing_pages_path, alert: "Unsuccessfully created #{@resource_name}, #{@request_landing_page.errors.full_messages.join(',')}"
    end
    # end
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to publishers_request_landing_page_path(params[:id])
  end

  def change_status
    resource
    @request_landing_page.track_script = params[:request_landing_page][:track_script]
    if @request_landing_page.save
      redirect_to publishers_request_landing_page_path(@request_landing_page), notice: "Successfully updated #{@resource_name} script analytics"
    else
      redirect_to :back, alert: "Unsuccessfully updated #{@resource_name}, #{@request_landing_page.errors.full_messages.join(',')}"
    end
  end

  def download_lp
  end

  private
    def class_name
      @resource_name = 'Landing Page'
    end

    def resource
      @request_landing_page = @publisher.request_landing_pages.find(params[:id])
    end

    def params_permit
      params.require(:request_landing_page).permit(:category_id, :campaign_id, :publisher_id, :layout_theme_id, :admin_id, :account_manager_id, :domain, :track_script, :status, :created_at, :updated_at)
    end
  
end