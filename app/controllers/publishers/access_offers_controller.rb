class Publishers::AccessOffersController < Publishers::ApplicationController
	before_action :check_profile
	before_action :class_name

	def create
		@access_offer = AccessOffer.new params_permit
		@access_offer.publisher_id = current_user.id
		@access_offer.account_manager_id = current_user.account_manager_id
		@access_offer.status = AccessOffer::APPROVE

		if @access_offer.save
      @offer = @access_offer.offer
			create_activity(@offer, current_user.id, "Applied offer '#{@offer.name}' with code #{@offer.code}", "#{@access_offer.publisher.username} have successfully submitted a request to create a campaign on offer '#{@offer.name}' with code #{@offer.code}.")
			create_notification(@offer, @access_offer.account_manager_id, "#{@access_offer.publisher.username} applied offer '#{@offer.name}' with code #{@offer.code}", "please verify your publisher data before approval offer.")
			UserMailer.apply_access_offer(@access_offer).deliver_now!
			redirect_to :back, notice: "You have successfully submitted a request to create a campaign on offer '#{@access_offer.offer.name}' with code #{@offer.code}, please wait, our team will verify your data first."
		else
			redirect_to :back, alert: "Unsuccessfully applied offer."
		end
	end

	private

		def params_permit
			params.require(:access_offer).permit(:publisher_id, :account_manager_id, :category_campaign_id, :status, :checked, :message)
		end

		def class_name
			@resource_name = 'Offer'
			@collection_name = 'Offers'
		end
end