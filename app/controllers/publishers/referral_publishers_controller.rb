class Publishers::ReferralPublishersController < Publishers::ApplicationController
	before_action :check_link_referral

	def index
		collection = @publisher.referral_publishers
		@total_collection = collection.count rescue 0
		@referral_publishers = collection.search_by(params).page(params[:page_referral_publishers]).per(per_page)
	end
	
	private
		def check_link_referral
			unless @publisher.have_link_referral?
				redirect_to publishers_dashboards_path, alert: "permission denied!"
			end 
		end
	
end