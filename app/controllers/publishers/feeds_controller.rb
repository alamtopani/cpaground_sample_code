class Publishers::FeedsController < Publishers::ApplicationController  
  def create
    @feed = Feed.new(params_permit)
    @feed.user_id = current_user.id
    @feed.group_id = params[:group_id] if params[:group_id].present?
    if @feed.save
      if params[:group_id].present?
        create_activity(@feed, current_user.id, "create feed", "successfully created feed on group #{@feed.group.name}.")
        redirect_to publishers_group_path(@feed.group), notice: 'Your feed successfully share to group'
      else
        @feed.status = Feed::INACTIVE
        @feed.save
        create_activity(@feed, current_user.id, "create feed", "successfully created feed.")
        redirect_to :back, notice: 'Your feed successfully created for everyone. Feed content will be reviewed, please wait for approval.'
      end
    else
      redirect_to :back, alert: "Unsuccessfully created feed}"
    end
  end

  def comment_create
    @feed_comment = FeedComment.create(user_id: current_user.id, message: params[:message], feedable_id: params[:feedable_id], feedable_type: params[:feedable_type])
    
    respond_to do |format|
      format.js { render json: { status: 'Success', user_type: @feed_comment.user.type.titleize, user_username: @feed_comment.user.username, user_avatar: @feed_comment.user.avatar.url(:thumb), data: @feed_comment}, status: :ok }
    end
  end

  def like_create
    @feed_like = Like.create(user_id: current_user.id, feed_id: params[:feed_id])
    
    respond_to do |format|
      format.js { render json: { status: 'Success', data: @feed_like }, status: :ok }
    end
  end

  private
    def params_permit
      params.require(:feed).permit(:user_id, :message, :attachment_image)
    end
end