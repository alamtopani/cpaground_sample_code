class Publishers::GroupsController < Publishers::ApplicationController
  before_action :class_name

  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "groups", :publishers_groups_path

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    @feeds = @group.feeds.activated.latest.page(page).per(per_page)
    @groups = @publisher.groups
    @group_coverages_approve = @group.group_coverages.approve
    @comment_activities = FeedComment.where(feedable_type: FeedComment::FEED, :feedable_id.in => @group.feeds.pluck(:id)).not_in(user_id: current_user.id).latest.limit(50)
    
    add_breadcrumb "#{@group.name}", "#"
  end

  def join
    @group_coverage = GroupCoverage.new
    @group_coverage.group_id = params[:group_id]
    @group_coverage.user_id = current_user.id
    if @group_coverage.save
      group = @group_coverage.group
      create_notification(group, group.user_id, "Request join group #{group.name}", "#{@group_coverage.user.username} request to join group #{group.name}.")
      create_activity(@group_coverage, current_user.id, "request join group", "request to join group #{@group_coverage.group.name}.")
      redirect_to :back, notice: "Successfully send request join this group."
    else
      redirect_to :back, alert: "Unsuccessfully send request join this group, #{@group_coverage.errors.full_messages.join(',')}"
    end
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to publishers_group_path(params[:id])
  end

  private
    def resource
      @group = Group.find(params[:id])
    end

    def collection
      @groups = @publisher.groups.latest
    end

    def class_name
      @resource_name = 'Group'
      @collection_name = 'Groups'
    end
end