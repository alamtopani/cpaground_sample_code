class Publishers::CampaignsController < Publishers::ApplicationController
  before_action :class_name
  before_action :check_profile

  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "my campaigns", :publishers_campaigns_path

  def index
    @campaigns = collection.page(page).per(per_page)
  end

  def show
    add_breadcrumb "#{params[:action]}"
    resource
    redirect_to :back, alert: "You can not enter this page, your Campaign '#{@campaign.title}' is still not approved" unless @campaign.is_active? || @campaign.is_complete?
    @landing_pages = @campaign.request_landing_pages.latest.page(params[:page_landing_pages]).per(per_page) if @campaign.request_landing_pages.present?
    
    get_analytic_report(@publisher, 'dashbord_publisher_campaign')
    @offer = @campaign.category_campaign
    @banner_images = @campaign.category_campaign.banner_images
    @layout_themes = LayoutTheme.active.where(category_id: @campaign.category_campaign.category_id)
    
    if @convertion_reports.present?
      set_collection_convertion_report(params[:group_by], @convertion_reports, "publisher")
      # multipe_chart(@convertion_reports_list)
    end
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def new
    add_breadcrumb "#{params[:action]}"
    @campaign = @publisher.campaigns.build
  end

  def create
    @campaign =  @publisher.campaigns.build params_permit
    @campaign.admin_id = current_user.admin_id
    @campaign.account_manager_id = current_user.account_manager_id
    @campaign.link = @campaign.category_campaign.link

    if @campaign.save
      if CategoryCampaign::ALL_ACTIVE_DOMAIN.select{|t| @campaign.category_campaign.link.include?(t)}.present? == true
        if Rails.env.production? || Rails.env.productiontrack?
          uniq_cp = '&s1=cg-'+@publisher.id.to_s+'&s2='+@campaign.id
        else
          uniq_cp = '&s1=test-'+@publisher.id.to_s+'&s2='+@campaign.id
        end
        uniq_cp = @campaign.category_campaign.link.split('?')[1]+uniq_cp
        uniq_cp = Base64.encode64(uniq_cp)
        offer_link = @campaign.category_campaign.link.split('offer_id=')[0]
        @campaign.tracking_code = offer_link+'c='+uniq_cp
      else
        if Rails.env.production? || Rails.env.productiontrack?
          @campaign.tracking_code = @campaign.category_campaign.link.include?('groundsec.com') == true ? @campaign.category_campaign.link+'/?s1=cg-'+@publisher.id.to_s+'&s2='+@campaign.id : @campaign.category_campaign.link+'&s1=cg-'+@publisher.id.to_s+'&s2='+@campaign.id
        else
          @campaign.tracking_code = @campaign.category_campaign.link.include?('groundsec.com') == true ? @campaign.category_campaign.link+'/?s1=test-'+@publisher.id.to_s+'&s2='+@campaign.id : @campaign.category_campaign.link+'&s1=test-'+@publisher.id.to_s+'&s2='+@campaign.id
        end
      end
      @campaign.tracking_code = @campaign.tracking_code.gsub(/\s+/, "") if @campaign.tracking_code.present?
      create_activity(@campaign, current_user.id, "new #{@resource_name.downcase}", "successfully created #{@resource_name.downcase} #{@campaign.title}")
      if @campaign.check_approved?
        @campaign.status = Campaign::ACTIVE
        create_notification(@campaign, @campaign.publisher_id, "Your #{@resource_name.downcase} #{@campaign.title}", "Your #{@resource_name.downcase} #{@campaign.title} has approved.")
        create_notification(@campaign, @campaign.account_manager_id, "#{@campaign.publisher.username.capitalize} #{@resource_name.downcase} #{@campaign.title}", "#{@campaign.publisher.username.capitalize} #{@resource_name.downcase} #{@campaign.title} has approved.")
        @campaign.update
        link_campaign_show = params[:c].present? ? publishers_campaign_path(@campaign, c: params[:c]) : publishers_campaign_path(@campaign)
        redirect_to link_campaign_show, notice: "Successfully applied offer '#{@campaign.category_campaign.name}' with title '#{@campaign.title}' "
      else
        @campaign.status = Campaign::PENDING
        # UserMailer.after_create_campaign(@campaign).deliver_now!
        # UserMailer.after_create_campaign_admin(@campaign).deliver_now!
        # UserMailer.after_update_campaign(@campaign).deliver_now!
        create_notification(@campaign, @campaign.publisher.admin_id, "#{@campaign.publisher.username} has request #{@resource_name.downcase} with title #{@campaign.title}", "#{@campaign.publisher.username} has request #{@resource_name.downcase} with title #{@campaign.title} please review.")
        create_notification(@campaign, @campaign.publisher_id, "Your #{@resource_name.downcase} #{@campaign.title}", "Waiting approvement for #{@resource_name.downcase} #{@campaign.title}.")
        create_notification(@campaign, @campaign.account_manager_id, "#{@campaign.publisher.username.capitalize} #{@resource_name.downcase} #{@campaign.title}", "#{@campaign.publisher.username.capitalize} waiting approvement for #{@resource_name.downcase} #{@campaign.title} please review.")
        @campaign.update
        redirect_to publishers_campaigns_path, notice: " Successfully requested offer '#{@campaign.category_campaign.name}' with title '#{@campaign.title}', please wait for approval from us, we will inform you immediately to your email if your request offer is approved "
      end
    else
      redirect_to publishers_campaigns_path, alert: "Unsuccessfully created #{@resource_name}, #{@campaign.errors.full_messages.join(',')}"
    end
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to publishers_campaign_path(params[:id])
  end
  

  private
    def resource
      @campaign = @publisher.campaigns.find(params[:id])
    end

    def collection
      @campaigns = @publisher.campaigns.latest.search_by(params)
    end

    def params_permit
      params.require(:campaign).permit(:publisher_id, :title, :category_campaign_id)
    end

    def class_name
      @resource_name = 'Campaign'
      @collection_name = 'Campaigns'
    end
end