class Publishers::ReportsController < Publishers::ApplicationController
  before_action :check_profile
  before_action :class_name

  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "reports", :publishers_reports_path

  def index
    @date = Time.zone.now.to_date

    unless params[:start_at].present? && params[:end_at].present?
      unless params[:status] == "visited"
        collection = @publisher.convertion_reports.where(convertion_date: @date).have_sales
      else
        collection = @publisher.convertion_reports.where(convertion_date: @date).visited
      end
    else
      unless params[:status] == "visited"
        collection = @publisher.convertion_reports.have_sales.search_by(params)
      else
        collection = @publisher.convertion_reports.visited.search_by(params)
      end
    end
    
    # multipe_chart(collection) unless params[:status] == "visited"
    set_collection_convertion_report(params[:group_by], collection, "publisher") rescue nil

    unless params[:status] == 'visited'
      @total_collection_approved = collection.approved.count rescue 0
      @total_collection_pending = collection.pending.count rescue 0
      @total_collection_reject = collection.where(status: "reject").count rescue 0

      @fee_approved = collection.approved.sum(:fee_publisher) rescue 0
      @fee_pending = collection.pending.sum(:fee_publisher) rescue 0
      @fee_reject = collection.where(status: "reject").sum(:fee_publisher) rescue 0

      if params[:export] == 'true'
        @total_convertion_reports = collection.have_sales_only.latest rescue nil
      end
    end

    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  private
    def resource
      @report = @publisher.convertion_reports.find(params[:id])
    end

    def class_name
      @resource_name = 'Report'
      @collection_name = 'Reports'
    end
end