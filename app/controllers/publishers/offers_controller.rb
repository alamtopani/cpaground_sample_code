class Publishers::OffersController < Publishers::ApplicationController
  before_action :check_profile
  before_action :class_name

  add_breadcrumb "dashboard", :publishers_dashboards_path
  add_breadcrumb "offers", :publishers_offers_path

  def index
    @offers = collection.page(page).per(per_page)
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def show
    add_breadcrumb "#{params[:action]}"
    
    respond_to do |format|
      format.html 
      format.xls
      format.js
    end
  end

  def click_notif
    notification = Notification.find(params[:notification_id])
    notification.status = "read"
    notification.save

    redirect_to publishers_offers_path
  end


  private
    def resource
      @offer = CategoryCampaign.find(params[:id])
    end

    def collection
      @offers = CategoryCampaign.active.search_by(params)
    end

    def params_permit
      params.require(:category_campaign).permit(:name, :link, :action, :detail, :status)
    end

    def class_name
      @resource_name = 'Offer'
      @collection_name = 'Offers'
    end
end