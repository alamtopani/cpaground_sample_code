class Like
  include Mongoid::Document
  field :feed_id, type: String
  field :user_id, type: Integer
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ feed_id: 1, user_id: 1})

  before_create :before_create_service
  before_save :before_update_service

  def user
    user = User.find_by(id: self.user_id)
  end
  
  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
