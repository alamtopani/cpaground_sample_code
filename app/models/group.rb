class Group
  include Mongoid::Document
  field :name, type: String
  field :description, type: String
  field :cover, type: String
  field :icon, type: String
  field :user_id, type: Integer
  field :status, type: String, default: 'active'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  mount_uploader :cover, PhotoUploader
  mount_uploader :icon, PhotoUploader

  scope :active, ->{where(status: 'active')}
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  before_create :before_create_service
  before_save :before_update_service
  before_destroy :destroy_feeds

  has_many :group_coverages

  include TheGroups::GroupsSearching

  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze

  STATUSES = [self::ACTIVE, self::INACTIVE].freeze

  def account_manager
    account_manager = AccountManager.find_by(id: self.user_id)
  end

  def feeds
    feeds = Feed.where(group_id: self.id)
  end

  def notifications
    notifications = Notification.where(notification_id: self.id)
  end

  def status?
    if self.status == "active"
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  def is_active?
    if self.status == Group::ACTIVE
      return true
    end
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end

    def destroy_feeds
      self.feeds.each do |feed|
        feed.feed_comments.delete_all
      end
      self.feeds.delete_all
      self.notifications.delete_all
   end
end
