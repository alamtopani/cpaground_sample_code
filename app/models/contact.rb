class Contact
  include Mongoid::Document
  field :name, type: String
  field :email, type: String
  field :phone_code, type: String 
  field :phone, type: String 
  field :website, type: String
  field :message, type: String
  field :status, type: String, default: 'unread'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  include TheContacts::ContactsSearching

  # ------- RELATIONS --------

  def activities
    activities = Activity.where(activitiable_id: self.id)
  end

  # ------- STATUS --------
  READ = 'read'.freeze
  UNREAD = 'unread'.freeze

  STATUSES = [self::READ, self::UNREAD].freeze

  def status?
    if self.status == "read"
      return "<label class='label label-success'>Read</label>".html_safe
    else
      return "<label class='label label-danger'>Unread</label>".html_safe
    end
  end

  def get_phone_number
    if self.phone_code.present?
      self.phone_code + self.phone
    else
      self.phone
    end
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
