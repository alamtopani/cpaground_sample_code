class CategoryName < CategoryCampaign
  NAMES = [
    {
      id: 1,
      name: 'Movie TV'
    },
    {
      id: 2,
      name: 'Sport'
    },
    {
      id: 3,
      name: 'Music'
    },
    {
      id: 4,
      name: 'Games'
    },
    {
      id: 5,
      name: 'Books'
    },
    {
      id: 6,
      name: 'Dating'
    },
    {
      id: 7,
      name: 'App'
    },
    {
      id: 8,
      name: 'Giveaways'
    },
    {
      id: 9,
      name: 'Freebies'
    },
    {
      id: 10,
      name: 'Sweepstakes'
    },
    {
      id: 11,
      name: 'Ecommerce'
    },
    {
      id: 12,
      name: 'Casino/Binary/Bingo'
    },
    {
      id: 13,
      name: 'Multimedia'
    }

    # ,
    # {
    #   id: 4,
    #   name: 'Game',
    #   domain: 'http://game.com',
    #   link_geo: [
    #     {
    #       country: 'US',
    #       link: ''
    #     },
    #     {
    #       country: 'GB',
    #       link: ''
    #     },
    #     {
    #       country: 'CA',
    #       link: ''
    #     },
    #     {
    #       country: 'IT',
    #       link: ''
    #     },
    #     {
    #       country: 'IE',
    #       link: ''
    #     },
    #     {
    #       country: 'NL',
    #       link: ''
    #     },
    #     {
    #       country: 'FR',
    #       link: ''
    #     },
    #     {
    #       country: 'BE',
    #       link: ''
    #     },
    #     {
    #       country: 'DE',
    #       link: ''
    #     },
    #     {
    #       country: 'SE',
    #       link: ''
    #     },
    #     {
    #       country: 'NO',
    #       link: ''
    #     },
    #     {
    #       country: 'DK',
    #       link: ''
    #     },
    #     {
    #       country: 'FI',
    #       link: ''
    #     },
    #     {
    #       country: 'IS',
    #       link: ''
    #     },
    #     {
    #       country: 'RS',
    #       link: ''
    #     },
    #     {
    #       country: 'RW',
    #       link: ''
    #     },
    #     {
    #       country: 'AE',
    #       link: ''
    #     },
    #     {
    #       country: 'CH',
    #       link: ''
    #     },
    #     {
    #       country: 'JP',
    #       link: ''
    #     },
    #     {
    #       country: 'AU',
    #       link: ''
    #     }
    #   ]
    # }
  ]
end