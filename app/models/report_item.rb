class ReportItem
  include Mongoid::Document
  field :convertion_report_id, type: String
  field :report_campaign_snapshot_id, type: String
  field :category_campaign_id, type: String
  field :campaign_id, type: String
  field :publisher_id, type: Integer
  field :account_manager_id, type: Integer
  field :admin_id, type: Integer
  field :name, type: String
  field :revenue, type: Float, default: 0
  field :fee_publisher, type: Float, default: 0
  field :fee_am, type: Float, default: 0
  field :fee_company, type: Float, default: 0
  field :status, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ convertion_report_id: 1, report_campaign_snapshot_id: 1,campaign_id: 1,publisher_id: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :approved, ->{where(status: self::APPROVED)}
  scope :pending, ->{where(status: self::PENDING)}
  scope :rejected, ->{where(status: self::REJECT)}

  belongs_to :campaign
  belongs_to :category_campaign
  belongs_to :report_campaign_snapshot
  belongs_to :convertion_report

  # ------- STATUS --------
  APPROVED = 'approved'.freeze
  REJECT = 'reject'.freeze
  PENDING = 'pending'.freeze

  STATUSES = [self::PENDING, self::APPROVED, self::REJECT].freeze

  def status?
    if self.status == "approved"
      return "<label class='label label-success'>Approved</label>".html_safe
    elsif self.status == "pending"
      return "<label class='label label-warning'>Pending</label>".html_safe
    else
      return "<label class='label label-danger'>Reject</label>".html_safe
    end
  end

  def admin
    admin = Admin.find_by(id: self.admin_id)
  end

  def publisher
    publisher = Publisher.find_by(id: self.publisher_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.account_manager_id)
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
