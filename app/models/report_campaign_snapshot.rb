class ReportCampaignSnapshot # Always Create and Update in account manager
  include Mongoid::Document
  field :code, type: String
  field :campaign_id, type: String
  field :admin_id, type: Integer
  field :account_manager_id, type: Integer
  field :publisher_id, type: Integer
  field :campaign_date, type: Date
  field :impressions, type: Integer, default: 0
  field :clicks, type: Integer, default: 0
  field :visitors, type: Integer, default: 0
  field :unique_visitors, type: Integer, default: 0
  field :sales, type: Integer, default: 0
  field :country, type: String
  field :revenue, type: Float, default: 0
  field :fee_publisher, type: Float, default: 0
  field :fee_am, type: Float, default: 0
  field :fee_company, type: Float, default: 0
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ publisher_id: 1, account_manager_id: 1, campaign_id: 1, campaign_date: 1, sales: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :have_sales_only, ->{where(:sales.gte => 1)}
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  include TheReportCampaignSnapshots::ReportCampaignSnapshotsSearching
  include TheReportCampaignSnapshots::ReportCampaignSnapshotsSearchingAnalytic
  
  # ------- RELATIONS --------
  belongs_to :campaign
  # has_many :convertion_reports

  def admin
    admin = Admin.find_by(id: self.admin_id)
  end

  def publisher
    publisher = Publisher.find_by(id: self.publisher_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.account_manager_id)
  end

  def activities
    activities = Activity.where(activitiable_id: self.id)
  end

  def category_campaign
    category_campaign = CategoryCampaign.find_by(id: self.campaign.category_campaign_id)
  end

  def convertion_reports
    convertion_reports = ConvertionReport.where(report_campaign_snapshot_id: self.id)
  end

  def self.total(user_type)
    if user_type == User::PUBLISHER
      sum(:fee_publisher) rescue 0
    elsif user_type == User::ACCOUNTMANAGER
      sum(:fee_am) rescue 0
    else
      sum(:fee_company) rescue 0
    end
  end

  private
    def before_create_service
      self.code = self.id
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
