class GroupCoverage
  include Mongoid::Document
  field :group_id, type: String
  field :user_id, type: Integer
  field :status, type: String, default: 'pending'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  scope :approve, ->{where(status: "approve")}
  scope :waiting, ->{ne(status: "approve")}
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  index({ group_id: 1, user_id: 1, status: 1})

  before_create :before_create_service
  before_save :before_update_service

  belongs_to :group

  # ------- STATUS --------
  PENDING = 'pending'.freeze
  APPROVE = 'approve'.freeze
  REJECT = 'reject'.freeze

  STATUSES = [self::PENDING, self::APPROVE, self::REJECT].freeze

  include TheGroups::GroupsSearching

  def user
    user = User.find_by(id: self.user_id)
    user = Publisher.find_by(id: self.user_id) if user.is_publisher?
    return user
  end

  def status?
    if self.status == GroupCoverage::PENDING
      return "<label class='label label-warning'>Pending</label>".html_safe
    elsif self.status == GroupCoverage::APPROVE
      return "<label class='label label-success'>Approve</label>".html_safe
    else
      return "<label class='label label-danger'>Reject</label>".html_safe
    end
  end

  def status_available?
    if self.status == GroupCoverage::PENDING
      return "<label class='label label-warning'>Pending</label>".html_safe
    elsif self.status == GroupCoverage::REJECT
      return "<label class='label label-danger'>Reject</label>".html_safe
    else
      return ""
    end
  end

  def is_pending?
    if self.status == GroupCoverage::PENDING
      return true
    end
  end

  def is_approve?
    if self.status == GroupCoverage::APPROVE
      return true
    end
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
