module UserModule
  module UserAuthenticate
    extend ActiveSupport::Concern
    included do
      attr_accessor :login

      def authenticate(login, password)
        user = where("username = :login OR email = :login", { login: login }).first
        return nil unless user
        return nil unless user.valid_password?(password)
        user
      end

      def self.find_for_database_authentication(warden_conditions)
        conditions = warden_conditions.dup
        if login = conditions.delete(:login)
          where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
        else
          where(conditions).first
        end
      end

      def login=(login)
        @login = login
      end

      def login
        @login || self.username || self.email
      end

      def self.find_for_devise_oauth(auth_hash, signed_in_resource)
        check_email = auth_hash.info.email ? auth_hash.info.email : auth_hash.extra.raw_info.name.downcase

        if auth_hash.provider == 'facebook'
          @user = User.where("users.facebook_uid =? OR users.email =?", auth_hash.uid, check_email).last 
          if signed_in_resource.present?
            user = signed_in_resource

            if @user.blank? || @user == signed_in_resource
              if user.facebook_uid.blank? || user.facebook_uid == auth_hash.uid  
                user.prepare_attr_devise('facebook', signed_in_resource, auth_hash, check_email)
              elsif user.facebook_uid != auth_hash.uid
                return :error_different
              end
            elsif @user.present? && @user.facebook_uid != user.facebook_uid
              return :error_current
            else
              return :error
            end

            return signed_in_resource
          elsif @user.blank?
            check_user = User.where("users.email =? OR users.username =?", check_email, auth_hash.extra.raw_info.name.downcase).last

            if check_user.present?
              return :error
            else
              user = self.find_or_create_by(provider: auth_hash.provider, facebook_uid: auth_hash.uid)
              user = user.prepare_attr_devise('facebook', signed_in_resource, auth_hash, check_email)
              return user
            end

          else
            @user.facebook_uid = auth_hash.uid
            @user.facebook_oauth_token = auth_hash.credentials.token
            @user.facebook_oauth_token_expires = Time.at(auth_hash.credentials.expires_at).in_time_zone
            @user.save

            return @user

          end

        elsif auth_hash.provider == 'google_oauth2'
          @user = User.where("users.google_uid =? OR users.email =?", auth_hash.uid, check_email).last 

          if signed_in_resource.present?
            user = signed_in_resource

            if @user.blank? || @user == signed_in_resource
              if user.google_uid.blank? || user.google_uid == auth_hash.uid  
                user.prepare_attr_devise('google_oauth2', signed_in_resource, auth_hash, check_email)            
              elsif user.google_uid != auth_hash.uid
                return :error_different
              end
            elsif @user.present? && @user.google_uid != user.google_uid
              return :error_current
            else
              return :error
            end

            return signed_in_resource
          elsif @user.blank?
            check_user = User.where("users.email =? OR users.username =?", check_email, auth_hash.extra.raw_info.name.downcase).last

            if check_user.present?
              return :error
            else
              user = self.find_or_create_by(provider: auth_hash.provider, google_uid: auth_hash.uid)
              user = user.prepare_attr_devise('google_oauth2', signed_in_resource, auth_hash, check_email)
              return user
            end
          else
            @user.google_uid = auth_hash.uid
            @user.google_oauth_token = auth_hash.credentials.token
            @user.google_oauth_token_expires = Time.at(auth_hash.credentials.expires_at).in_time_zone
            @user.save

            return @user
          end

        end

      end


      ################ CHECK ATTR DEVISE OUTH ################

      def prepare_attr_devise(provider, current_user, auth_hash, check_email)
        unless current_user.present?
          self.username = auth_hash.extra.raw_info.name.downcase
          self.provider = auth_hash.provider
          self.email = check_email
          self.password = Devise.friendly_token[0,10]
          self.confirmation_token = nil
          self.confirmed_at = Time.now
          self.type = User::PUBLISHER
        end

        if provider == 'facebook'
          self.facebook_uid = auth_hash.uid
          self.facebook_oauth_token = auth_hash.credentials.token
          self.facebook_oauth_token_expires = Time.at(auth_hash.credentials.expires_at).in_time_zone
          self.facebook_oauth_token_register_at = Time.now.in_time_zone
          self.remote_facebook_avatar_url = auth_hash.info.image.gsub('http://','https://') if self.facebook_avatar.blank?
          self.remote_avatar_url = auth_hash.info.image.gsub('http://','https://') if self.avatar.blank?
          self.save!
          self.facebook_check_token_expires(auth_hash.credentials.token, auth_hash.credentials.expires_at)
        elsif provider == 'google_oauth2'
          self.google_uid = auth_hash.uid
          self.google_oauth_token = auth_hash.credentials.token
          self.google_oauth_token_expires = Time.at(auth_hash.credentials.expires_at).in_time_zone
          self.google_oauth_token_register_at = Time.now.in_time_zone
          self.save!
          self.google_check_token_expires(auth_hash.credentials.token, auth_hash.credentials.expires_at)
        end
      end

      ################ CHECK EXPIRED TOKEN ################

      def facebook_check_token_expires(token, token_expires)
        if self.facebook_oauth_token.nil? || (Time.now.in_time_zone >= self.facebook_oauth_token_expires)
          self.facebook_oauth_token = token
          self.facebook_oauth_token_expires = Time.at(token_expires).in_time_zone
        end
      end

      def google_check_token_expires(token, token_expires)
        if Time.zone.now >= self.google_oauth_token_expires
          self.google_oauth_token = token
          self.google_oauth_token_expires = Time.at(token_expires).in_time_zone
        end
      end
      
    end
  end
end