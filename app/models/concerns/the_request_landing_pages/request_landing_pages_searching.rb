module TheRequestLandingPages
  module RequestLandingPagesSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        # query_opts = [
        #   "LOWER(campaigns.tracking_code) LIKE LOWER(:key)",
        #   "LOWER(campaigns.title) LIKE LOWER(:key)"
        # ].join(' OR ')
        # where(query_opts, {key: /#{_key}/} )
        any_of({code: /#{_key}/i}, {domain: /#{_key}/i})
      end

      def by_publisher_id(_publisher_id)
        return if _publisher_id.blank?
        any_of({publisher_id: _publisher_id})
      end

      def by_category_id(_category_id)
        return if _category_id.blank?
        where(category_id: _category_id)
      end

      def by_status(_status)
        return if _status.blank?
        where(status: _status)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where(:created_at.gte => _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where(:created_at.lte => _end_at.to_date)
      end

      def search_by(options={})
        results = latest

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:publisher_id].present?
          results = results.by_publisher_id(options[:publisher_id])
        end

        if options[:category_id].present?
          results = results.by_category_id(options[:category_id])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_landing_pages].present?
          results = results.by_keywords(options[:key_landing_pages])
        end

        if options[:status_landing_pages].present?
          results = results.by_status(options[:status_landing_pages])
        end

        if options[:start_at_landing_pages].present?
          results = results.by_start_at(options[:start_at_landing_pages])
        end

        if options[:end_at_landing_pages].present?
          results = results.by_end_at(options[:end_at_landing_pages])
        end

        return results
      end
    end

  end
end
