module TheAccessOffers
  module AccessOffersSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        where(publisher_id: { :$in => _key })
      end

      def by_checkeds(_checked)
        return if _checked.blank?
        where(checked: _checked)
      end

      def by_status(_status)
        return if _status.blank?
        where(status: _status)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where(:created_at.gte => _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where(:created_at.lte => _end_at.to_date)
      end

      def find_publisher(_key)
        find_publisher = Publisher.where("LOWER(users.username) LIKE ?", "%#{_key.downcase}%").pluck(:id)
      end

      def search_by(options={})
        results = latest

        if options[:key].present?
          results = results.by_keywords(find_publisher(options[:key]))
        end

        if options[:key2].present?
          results = results.by_keywords(find_publisher(options[:key2]))
        end

        if options[:checked].present?
          results = results.by_checkeds(options[:checked])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        return results
      end
    end

  end
end
