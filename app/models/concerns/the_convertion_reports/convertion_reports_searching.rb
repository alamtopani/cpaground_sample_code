module TheConvertionReports
  module ConvertionReportsSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_click_id(_click_id)
        return if _click_id.blank?
        where(click_id: _click_id)
      end

      def by_category_campaign_id(_category_campaign_id)
        return if _category_campaign_id.blank?
        where(category_campaign_id: _category_campaign_id)
      end

      def by_campaign_id(_campaign_id)
        return if _campaign_id.blank?
        where(campaign_id: _campaign_id)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where(:convertion_date.gte => _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where(:convertion_date.lte => _end_at.to_date)
      end

      def by_country(_country)
        return if _country.blank?
        where(country_code: _country)
      end

      def by_referrer(_referrer)
        return if _referrer.blank?
        where(referrer: _referrer)
      end

      def by_device(_device)
        return if _device.blank?
        where(device: _device)
      end

      def by_browser(_browser)
        return if _browser.blank?
        where(browser: /#{_browser}/i)
      end

      def by_sales(_sales)
        return if _sales.blank?
        where(sales: _sales)
      end

      def by_source(_source)
        return if _source.blank?
        where(source: _source)
      end

      def by_sub1(_sub1)
        return if _sub1.blank?
        where(sub1: _sub1)
      end

      def by_sub2(_sub2)
        return if _sub2.blank?
        where(sub2: _sub2)
      end

      def by_status(_status)
        return if _status.blank?
        if _status == "convertion"
          where(:status.in => [ConvertionReport::APPROVED, ConvertionReport::REJECT, ConvertionReport::PENDING])
        else
          where(status: _status)
        end
      end

      def search_by(options={})
        results = all

        if options[:publisher_username].present?
          return if options[:publisher_username].blank?
          query_opts = [
            "LOWER(users.username) LIKE LOWER(:key)",
            "LOWER(users.email) LIKE LOWER(:key)"
          ].join(' OR ')
          if options[:account_manager_id].present?
            users = User.where(account_manager_id: options[:account_manager_id])
          elsif options[:admin_id].present?
            users = User.where(admin_id: options[:admin_id])
          else
            users = User.all
          end
          user_ids = users.where(query_opts, {key: "%#{options[:publisher_username]}%"} ).pluck(:id)
          results = results.where(publisher_id: { :$in => user_ids })
        end

        if options[:click_id].present?
          results = results.by_click_id(options[:click_id])
        end

        if options[:category_campaign_id].present?
          results = results.by_category_campaign_id(options[:category_campaign_id])
        end

        if options[:campaign_id].present?
          results = results.by_campaign_id(options[:campaign_id])
        end

        if options[:campaign_code].present?
          find_campaign = Campaign.where(code: options[:campaign_code]).last || Campaign.where(id: options[:campaign_code]).last
          campaign_id = find_campaign.id if find_campaign.present?
          results = results.by_campaign_id(campaign_id) if campaign_id.present?
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:country].present?
          results = results.by_country(options[:country])
        end

        if options[:referrer].present?
          results = results.by_referrer(options[:referrer])
        end

        if options[:device].present?
          results = results.by_device(options[:device])
        end

        if options[:browser].present?
          results = results.by_browser(options[:browser])
        end

        if options[:sales].present?
          results = results.by_sales(options[:sales])
        end

        if options[:source].present?
          results = results.by_source(options[:source])
        end

        if options[:sub1].present?
          results = results.by_sub1(options[:sub1])
        end

        if options[:sub2].present?
          results = results.by_sub2(options[:sub2])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        return results
      end
    end

  end
end
