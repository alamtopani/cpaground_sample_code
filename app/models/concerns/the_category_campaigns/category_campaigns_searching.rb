module TheCategoryCampaigns
  module CategoryCampaignsSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        # query_opts = [
        #   "LOWER(campaigns.tracking_code) LIKE LOWER(:key)",
        #   "LOWER(campaigns.title) LIKE LOWER(:key)"
        # ].join(' OR ')
        # where(query_opts, {key: /#{_key}/} )
        any_of({id: "#{_key}"}, {code: /#{_key}/i}, {name: /#{_key}/i}, {action: /#{_key}/i}, {detail: /#{_key}/i}, {link: /#{_key}/i})
      end

      def by_category(_category)
        return if _category.blank?
        where(category_id: _category)
      end

      def by_action(_action)
        return if _action.blank?
        where(action: _action)
      end

      def by_status(_status)
        return if _status.blank?
        if _status == "inactive"
          where({ :$or => [{:expires.lte => Date.today}, {:status => _status}] })
        else
          where(status: _status)
        end
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where(:created_at.gte => _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where(:created_at.lte => _end_at.to_date)
      end

      def by_shorter(_shorter)
        if _shorter == "featured"
          where(featured: true)
        elsif _shorter == "latest"
          latest
        elsif _shorter == "oldest"
          oldest
        else
          order(sales: :desc)
        end
      end

      def search_by(options={})
        results = all

        if options[:action_offer].present?
          results = results.by_action(options[:action_offer])
        end

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:category_offer].present?
          results = results.by_category(options[:category_offer])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:shorter].blank?
          results = results.order(sales: :desc)
        end

        if options[:shorter].present?
          results = results.by_shorter(options[:shorter])
        end

        return results
      end
    end

  end
end
