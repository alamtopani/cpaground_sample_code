module TheSubscribers
  module SubscribersSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        where({email: /#{_key}/i})
      end

      def by_status(_status)
        return if _status.blank?
        where(status: _status)
      end

      def by_subscriber_type(_subscriber_type)
        return if _subscriber_type.blank?
        where(subscriber_type: _subscriber_type)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where(:created_at.gte => _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where(:created_at.lte => _end_at.to_date)
      end

      def search_by(options={})
        results = latest

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:subscriber_type].present?
          results = results.by_subscriber_type(options[:subscriber_type])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        return results
      end
    end

  end
end
