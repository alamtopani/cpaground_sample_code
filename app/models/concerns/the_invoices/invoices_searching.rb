module TheInvoices
  module InvoicesSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        any_of({code: /#{_key}/i}, {title: /#{_key}/i}, {description: /#{_key}/i})
      end

      def by_status(_status)
        return if _status.blank?
        where(status: _status)
      end

      def by_category(_category)
        return if _category.blank?
        where(category: _category)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where(:transaction_date.gte => _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where(:transaction_date.lte => _end_at.to_date)
      end

      def search_by(options={})
        results = latest

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:category].present?
          results = results.by_category(options[:category])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        return results
      end
    end

  end
end
