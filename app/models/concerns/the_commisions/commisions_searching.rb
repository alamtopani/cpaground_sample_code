module TheCommisions
  module CommisionsSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        any_of({name: /#{_key}/i}, {email: /#{_key}/i}, {phone: /#{_key}/i}, {website: /#{_key}/i}, {message: /#{_key}/i})
      end

      def by_publisher_ids(_publisher_ids)
        return if _publisher_ids.blank?
        where(publisher_id: { :$in => _publisher_ids })
      end

      def by_referral_publisher_ids(_publisher_ids)
        return if _publisher_ids.blank?
        where(referral_publisher_id: { :$in => _publisher_ids })
      end

      def by_account_manager_ids(_am_ids)
        return if _am_ids.blank?
        where(account_manager_id: { :$in => _am_ids })
      end

      def by_status(_status)
        return if _status.blank?
        where(status: _status)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where(:campaign_date.gte => _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where(:campaign_date.lte => _end_at.to_date)
      end

      def find_publisher(_key)
        find_publisher = Publisher.where("LOWER(users.username) LIKE ?", "%#{_key.downcase}%").pluck(:id)
      end

      def find_am(_key)
        find_am = AccountManager.where("LOWER(users.username) LIKE ?", "%#{_key.downcase}%").pluck(:id)
      end

      def search_by(options={})
        results = latest

        if options[:publisher_username].present?
          results = results.by_publisher_ids(find_publisher(options[:publisher_username]))
        end

        if options[:referral_publisher_username].present?
          results = results.by_referral_publisher_ids(find_publisher(options[:referral_publisher_username]))
        end

        if options[:am_username].present?
          results = results.by_account_manager_ids(find_am(options[:am_username]))
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:publisher_username_commisions].present?
          results = results.by_publisher_ids(find_publisher(options[:publisher_username_commisions]))
        end

        if options[:referral_publisher_username_commisions].present?
          results = results.by_referral_publisher_ids(find_publisher(options[:referral_publisher_username_commisions]))
        end

        if options[:status_commisions].present?
          results = results.by_status(options[:status_commisions])
        end

        if options[:start_at_commisions].present?
          results = results.by_start_at(options[:start_at_commisions])
        end

        if options[:end_at_commisions].present?
          results = results.by_end_at(options[:end_at_commisions])
        end

        return results
      end
    end

  end
end
