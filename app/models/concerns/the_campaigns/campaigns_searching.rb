module TheCampaigns
  module CampaignsSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        # query_opts = [
        #   "LOWER(campaigns.tracking_code) LIKE LOWER(:key)",
        #   "LOWER(campaigns.title) LIKE LOWER(:key)"
        # ].join(' OR ')
        # where(query_opts, {key: /#{_key}/} )
        any_of({id: /#{_key}/i}, {code: /#{_key}/i}, {title: /#{_key}/i}, {tracking_code: /#{_key}/i})
      end

      def by_publisher_ids(_publisher_ids)
        return if _publisher_ids.blank?
        where(publisher_id: { :$in => _publisher_ids })
      end

      def by_publisher_id(_publisher_id)
        return if _publisher_id.blank?
        any_of({publisher_id: _publisher_id})
      end

      def by_category_campaign(_category_campaign)
        return if _category_campaign.blank?
        where(category_campaign_id: _category_campaign)
      end

      def by_status(_status_campaign)
        return if _status_campaign.blank?
        where(status: _status_campaign)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where(:created_at.gte => _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where(:created_at.lte => _end_at.to_date + 1)
      end

      def by_action_offer(_action_offer)
        return if _action_offer.blank?
        where(category_campaign_id: { :$in => _action_offer })
      end

      def find_publisher(_key)
        find_publisher = Publisher.where("LOWER(users.username) LIKE ?", "%#{_key.downcase}%").pluck(:id)
      end

      def find_offer_ids(_key)
        find_offer_ids = CategoryCampaign.where(action: _key).pluck(:id)
      end

      def search_by(options={})
        results = latest

        if options[:action_offer].present?
          results = results.by_action_offer(find_offer_ids(options[:action_offer]))
        end

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:publisher_id].present?
          results = results.by_publisher_id(options[:publisher_id])
        end

        if options[:category_campaign].present?
          results = results.by_category_campaign(options[:category_campaign])
        end

        if options[:status_campaign].present?
          results = results.by_status(options[:status_campaign])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_campaigns].present?
          results = results.by_keywords(options[:key_campaigns])
        end

        if options[:start_at_campaigns].present?
          results = results.by_start_at(options[:start_at_campaigns])
        end

        if options[:end_at_campaigns].present?
          results = results.by_end_at(options[:end_at_campaigns])
        end

        if options[:publisher_ids].present?
          results = results.by_publisher_ids(find_publisher(options[:publisher_ids]))
        end

        return results
      end
    end

  end
end
