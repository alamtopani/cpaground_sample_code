module ThePaymentSnapshots
  module PaymentSnapshotsSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        results = where(user_id: _key)
        unless results.present?
          query_opts = [
            "LOWER(users.username) LIKE LOWER(:key)",
            "LOWER(users.email) LIKE LOWER(:key)",
            "LOWER(payment_snapshots.message) LIKE LOWER(:key)",
            "LOWER(payment_snapshots.code) LIKE LOWER(:key)"
          ].join(' OR ')

          results = where(query_opts, {key: "%#{_key}%"} )
        end
        return results
      end

      def by_status(_status)
        return if _status.blank?
        where("payment_snapshots.status =?", _status)
      end

      def by_category(_category)
        return if _category.blank?
        where("payment_snapshots.category =?", _category)
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("payment_snapshots.created_at >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("payment_snapshots.created_at <=?", _end_at.to_date)
      end

      def search_by(options={})
        results = eager_load(:user).latest

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_payment_snapshots].present?
          results = results.by_keywords(options[:key_payment_snapshots])
        end

        if options[:status_payment_snapshots].present?
          results = results.by_status(options[:status_payment_snapshots])
        end

        if options[:category_payment_snapshots].present?
          results = results.by_category(options[:category_payment_snapshots])
        end

        if options[:start_at_payment_snapshots].present?
          results = results.by_start_at(options[:start_at_payment_snapshots])
        end

        if options[:end_at_payment_snapshots].present?
          results = results.by_end_at(options[:end_at_payment_snapshots])
        end

        return results
      end
    end

  end
end
