module TheUsers
  module PublishersSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        query_opts = [
          "LOWER(users.id_card) LIKE LOWER(:key)",
          "LOWER(users.username) LIKE LOWER(:key)",
          "LOWER(users.email) LIKE LOWER(:key)",
          "LOWER(users.phone) LIKE LOWER(:key)",
          "LOWER(users.skype) LIKE LOWER(:key)",
          "LOWER(users.facebook_url) LIKE LOWER(:key)",
          "LOWER(profiles.acc_bank) LIKE LOWER(:key)"
        ].join(' OR ')
        where(query_opts, {key: "%#{_key}%"} )
      end

      def by_status(_status)
        return if _status.blank?
        
        if _status == 'true' || _status ==  'false'
          condition = where("users.verified =?", _status).where(suspended: false)
        elsif _status == 'suspended'
          condition = where(suspended: true)
        else
          condition = where(suspended: false)
        end

        condition.where.not(confirmed_at: nil)
      end

      def by_user_id(_user_id)
        return if _user_id.blank?
        where("users.id =?", _user_id)
      end

      def by_suspended(_suspended)
        return if _suspended.blank?
        _suspended = _suspended == '0' ? false : true
        where("users.suspended =?", _suspended)
      end

      def by_verify_email(_verify_email)
        return if _verify_email.blank?
        
        if _verify_email == 'true'
          where.not(confirmed_at: nil)
        else
          where(confirmed_at: nil)
        end
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where("users.created_at >=?", _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where("users.created_at <=?", _end_at.to_date)
      end

      def by_publisher_ids(_publisher_ids)
        return if _publisher_ids.blank?
        where(id: _publisher_ids)
      end

      def by_manager_id(_manager_id)
        return if _manager_id.blank?
        where(account_manager_id: _manager_id)
      end

      def by_search_order(_search_order)
        return if _search_order.blank?
        if _search_order == "oldest"
          oldest
        elsif _search_order == "latest"
          latest
        elsif _search_order == "have not campaign"
          account_manager = AccountManager.find last.account_manager_id
          publisher_ids = Campaign.where(account_manager_id: account_manager.id).pluck(:publisher_id).uniq
          where.not(id: publisher_ids)
        elsif _search_order == "have campaign"
          account_manager = AccountManager.find last.account_manager_id
          publisher_ids = Campaign.where(account_manager_id: account_manager.id).pluck(:publisher_id).uniq
          where(id: publisher_ids)
        elsif _search_order == "have referrals"
          where.not(referral_token: nil)
        end
      end

      def last_payment(options)
        @date = Time.zone.now.to_date
        @last_month = (@date.beginning_of_month - 1.month)
        @beginning_of_month = @date.beginning_of_month
        @half_end_of_month = @date.beginning_of_month + 14.days
        @beginning_half_of_month = @half_end_of_month + 1.days
        @end_of_month = @date.end_of_month

        if @beginning_of_month <= @date && @half_end_of_month >= @date
          @last_payment_start_at = @last_month.beginning_of_month + 15.days
          @last_payment_end_at = @last_month.end_of_month
        elsif @beginning_half_of_month <= @date && @end_of_month >= @date
          @last_payment_start_at = @date.beginning_of_month
          @last_payment_end_at = @last_payment_start_at + 14.days
        end
        
        if options == "true"
          @last_payment = ReportCampaignSnapshot.where(:campaign_date.gte => @last_payment_start_at, :campaign_date.lte => @last_payment_end_at, :revenue.ne => 0)
          else
          @last_payment = ReportCampaignSnapshot.where(:campaign_date.gte => @last_payment_start_at, :campaign_date.lte => @last_payment_end_at, revenue: 0)
        end

        if @last_payment.present?
          return @last_payment.pluck(:publisher_id)
        else
          return false
        end
      end

      def search_by(options={})
        results = eager_load(:profile).all

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:status].present?
          results = results.by_status(options[:status])
        end

        if options[:user_id].present?
          results = results.by_user_id(options[:user_id])
        end

        if options[:suspended].present?
          results = results.by_suspended(options[:suspended])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_publishers].present?
          results = results.by_keywords(options[:key_publishers])
        end

        if options[:status_publishers].present?
          results = results.by_status(options[:status_publishers])
        end

        if options[:start_at_publishers].present?
          results = results.by_start_at(options[:start_at_publishers])
        end

        if options[:end_at_publishers].present?
          results = results.by_end_at(options[:end_at_publishers])
        end

        if options[:last_payment].present?
          results = results.by_publisher_ids(last_payment(options[:last_payment]))
        end

        if options[:manager_id].present?
          results = results.by_manager_id(options[:manager_id])
        end

        if options[:search_order].blank?
          results = results.latest
        end

        if options[:search_order].present?
          results = results.by_search_order(options[:search_order])
        end

        return results
      end

      def waiting_search_by(options={})
        results = eager_load(:profile).all

        if options[:key2].present?
          results = results.by_keywords(options[:key2])
        end

        if options[:verify_email].present?
          results = results.by_verify_email(options[:verify_email])
        end

        return results
      end
    end

  end
end
