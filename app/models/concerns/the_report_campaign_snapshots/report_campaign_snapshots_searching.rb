module TheReportCampaignSnapshots
  module ReportCampaignSnapshotsSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        return if _key.blank?
        any_of({code: /#{_key}/i}, {publisher_id: "#{_key}"})
      end

      def by_publisher_id(_key)
        return if _key.blank?
        any_of({publisher_id: "#{_key}"})
      end

      def by_campaign_id(_key)
        return if _key.blank?
        any_of({campaign_id: "#{_key}"})
      end

      def by_start_at(_start_at)
        return if _start_at.blank?
        where(:campaign_date.gte => _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where(:campaign_date.lte => _end_at.to_date)
      end

      def by_sales(_sales)
        return if _sales.blank?
        if _sales == "1"
          where(:sales.gte => 1)
        else
          where(sales: _sales)
        end
      end

      def search_by(options={})
        results = latest

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:publisher_id].present?
          results = results.by_publisher_id(options[:publisher_id])
        end

        if options[:campaign_id].present?
          results = results.by_campaign_id(options[:campaign_id])
        end

        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        if options[:key_report_campaign_snapshots].present?
          results = results.by_keywords(options[:key_report_campaign_snapshots])
        end

        if options[:start_at_report_campaign_snapshots].present?
          results = results.by_start_at(options[:start_at_report_campaign_snapshots])
        end

        if options[:end_at_report_campaign_snapshots].present?
          results = results.by_end_at(options[:end_at_report_campaign_snapshots])
        end

        if options[:sales].present?
          results = results.by_sales(options[:sales])
        end

        return results
      end

      def search_by_campaign_date(options={})
        results = latest

        if options[:campaign_start_at].present?
          results = results.by_start_at(options[:campaign_start_at])
        end

        if options[:campaign_end_at].present?
          results = results.by_end_at(options[:campaign_end_at])
        end

        return results
      end
    end

  end
end
