module TheReportCampaignSnapshots
  module ReportCampaignSnapshotsSearchingAnalytic
    extend ActiveSupport::Concern

    module ClassMethods
      def by_start_at(_start_at)
        return if _start_at.blank?
        where(:campaign_date.gte => _start_at.to_date)
      end

      def by_end_at(_end_at)
        return if _end_at.blank?
        where(:campaign_date.lte => _end_at.to_date)
      end

      def search_by_date(options={})
        results = oldest
        
        if options[:start_at].present?
          results = results.by_start_at(options[:start_at])
        end

        if options[:end_at].present?
          results = results.by_end_at(options[:end_at])
        end

        return results
      end
    end

  end
end
