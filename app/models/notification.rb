class Notification
  include Mongoid::Document
  field :user_id, type: Integer
  field :title, type: String
  field :message, type: String
  field :status, type: String, default: "unread"
  field :notification_id, type: String 
  field :notification_type, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ user_id: 1, status: 1, notification_id: 1, notification_type: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :unread, ->{where(status: "unread")}
  scope :sort_unread, ->{order(status: :desc)}

  # ------- RELATIONS --------

  READ = 'read'.freeze
  UNREAD = 'unread'.freeze

  def user
    user = User.find_by(id: self.user_id)
  end

  def admin
    admin = Admin.find_by(id: self.user_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.user_id)
  end

  def publisher
    publisher = Publisher.find_by(id: self.user_id)
  end

  def campaign
    if self.notification_type == "Campaign"
      campaign = Campaign.find_by(id: self.notification_id)
    end
  end

  def payment_snapshot
    if self.notification_type == "PaymentSnapshot"
      payment_snapshot = PaymentSnapshot.find_by(id: self.notification_id)
    end
  end

  def request_landing_page
    if self.notification_type == "RequestLandingPage"
      request_landing_page = RequestLandingPage.find_by(id: self.notification_id)
    end
  end

  def group
    if self.notification_type == "Group"
      group = Group.find_by(id: self.notification_id)
    end
  end

  def offer
    if self.notification_type == "CategoryCampaign"
      category_campaign = CategoryCampaign.find_by(id: self.notification_id)
    end
  end

  def has_readed?
    if self.status == "read"
      return "<li style='background: none;'>".html_safe
    else
      return "<li>".html_safe
    end
  end

  def status?
    if self.status == "unread"
      return "<label class='label label-success'>New</label>".html_safe
    end
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
