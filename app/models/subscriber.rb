class Subscriber
  include Mongoid::Document
  field :user_id, type: Integer
  field :email, type: String
  field :subscriber_type, type: String, default: "guest"
  field :status, type: String, default: 'active'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ user_id: 1, email: 1, status: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  include TheSubscribers::SubscribersSearching

  # ------- RELATIONS --------

  def activities
    activities = Activity.where(activitiable_id: self.id)
  end

  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze

  STATUSES = [self::ACTIVE, self::INACTIVE].freeze

  TYPE = ["guest", User::PUBLISHER, User::ACCOUNTMANAGER].freeze

  def status?
    if self.status == "active"
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  def is_active?
    self.status == Subscriber::ACTIVE
  end

  def is_inactive?
    self.status == Subscriber::INACTIVE
  end

  def is_publisher?
    self.subscriber_type == User::PUBLISHER 
  end

  def is_account_manager?
    self.subscriber_type == User::ACCOUNTMANAGER
  end

  def is_admin?
    self.subscriber_type == User::ADMIN
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
