class PermissionReferralPublisher
  include Mongoid::Document
  field :account_manager_id, type: Integer
  field :publisher_id, type: Integer
  field :referral_token, type: String
  field :commision, type: Float
  field :status, type: String, default: 'inactive'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ publisher_id: 1, account_manager_id: 1, referral_token: 1, status: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  include ThePermissionReferralPublishers::PermissionReferralPublishersSearching

  # ------- RELATIONS --------
  def account_manager
    account_manager = AccountManager.find self.account_manager_id
  end

  def publisher
    publisher = Publisher.find self.publisher_id
  end

  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze

  STATUSES = [self::ACTIVE, self::INACTIVE].freeze
  
  def status?
    if self.status == "active"
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  private
    def generate_referral_token(number)
      charset = Array('0'..'9') + Array('a'..'z')
      Array.new(number) { charset.sample }.join
    end

    def before_create_service
      referral_token = generate_referral_token(30)
      self.referral_token = referral_token
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
