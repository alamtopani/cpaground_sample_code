class RequestLandingPage
  include Mongoid::Document
  field :code, type: String
  field :category_id, type: String
  field :campaign_id, type: String
  field :publisher_id, type: Integer
  field :layout_theme_id, type: String
  field :admin_id, type: Integer
  field :account_manager_id, type: Integer
  field :domain, type: String
  field :track_script, type: String
  field :link_campaign, type: String, default: "--"
  field :status, type: String, default: 'inactive'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ publisher_id: 1, layout_theme_id: 1, campaign_id: 1, code: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}

  include TheRequestLandingPages::RequestLandingPagesSearching

  belongs_to :campaign
  belongs_to :layout_theme

  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze
  REJECT = 'reject'.freeze

  STATUSES = [self::ACTIVE, self::INACTIVE, self::REJECT].freeze

  def link_campaign?
    self.link_campaign.gsub(/\s+/, "")
  end
  
  def category
    category = CategoryCampaign::CATEGORIES.select{|c| c[:id] == self.category_id.to_i}[0]
  end

  def category_name
    category = CategoryCampaign::CATEGORIES.select{|c| c[:id] == self.category_id.to_i}[0][:name]
  end

  def admin
    admin = Admin.find_by(id: self.admin_id)
  end

  def publisher
    publisher = Publisher.find_by(id: self.publisher_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.account_manager_id)
  end

  def activities
    activities = Activity.where(activitiable_id: self.id)
  end

  def is_active?
    self.status == RequestLandingPage::ACTIVE
  end

  def is_inactive?
    self.status == RequestLandingPage::INACTIVE
  end

  def is_reject?
    self.status == RequestLandingPage::REJECT
  end

  def is_campaign_complete?
    self.campaign.status == Campaign::COMPLETE
  end

  def status?
    if self.is_campaign_complete?
      return "<label class='label label-info'>Complete</label>".html_safe
    else
      if self.status == "active"
        return "<label class='label label-success'>Active</label>".html_safe
      elsif self.status == "reject"
        return "<label class='label label-danger'>Reject</label>".html_safe
      else
        return "<label class='label label-warning'>Inactive</label>".html_safe
      end
    end
  end

  private
    def before_create_service
      code = 'LP'+ 10.times.map{Random.rand(10)}.join
      self.code = code
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
  
end
