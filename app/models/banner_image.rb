class BannerImage
  include Mongoid::Document
  field :category_campaign_id, type: String
  field :name, type: String
  field :description, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ category_campaign_id: 1, name: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  belongs_to :category_campaign

  mount_uploader :name, ImageUploader

  CPA_IMAGES = CpaImage::IMAGES

  def filename
    self.name.url.split("/").last
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
