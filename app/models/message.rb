class Message
  include Mongoid::Document
  field :group_message_id, type: String
  field :from, type: Integer
  field :to, type: Integer
  field :message, type: String
  field :status, type: String, default: 'unread'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :sort_unread, ->{order(status: :desc)}
  scope :unread, ->{where(status: "unread")}
  scope :read, ->{where(status: "read")}

  index({ group_message_id: 1, from: 1,to: 1,status: 1})

  belongs_to :group_message

  before_create :before_create_service
  before_save :before_update_service

  include TheBlogs::BlogsSearching

  # ------- STATUS --------
  READ = 'read'.freeze
  UNREAD = 'unread'.freeze

  STATUSES = [self::READ, self::UNREAD].freeze

  def status?
    if self.status == "read"
      return "<label class='label label-success'>Read</label>".html_safe
    else
      return "<label class='label label-danger'>Unread</label>".html_safe
    end
  end

  def user
    user = User.find_by(id: self.from)
  end

  def user_to
    user_to = User.find_by(id: self.to)
  end

  def has_readed?
    if self.status == "read"
      return "<li style='background: none;'>".html_safe
    else
      return "<li>".html_safe
    end
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
