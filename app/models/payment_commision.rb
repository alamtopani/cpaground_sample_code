class PaymentCommision < ApplicationRecord
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :for_publisher, -> { where(status: [self::PROCESS, self::PAID]) }
  scope :pending, -> { where(status: self::PENDING) }
  scope :process, -> { where(status: self::PROCESS) }
  scope :paid, -> { where(status: self::PAID) }
  scope :rejects, -> { where(status: self::REJECT) }

  belongs_to :admin
  belongs_to :user
  belongs_to :publisher, foreign_key: "user_id"

  include ThePaymentCommisions::PaymentCommisionsSearching

  before_create :before_create_service


  # ============
  #   STATUSES
  # ============

  PENDING = 'pending'.freeze
  PROCESS = 'process'.freeze
  PAID = 'paid'.freeze
  REJECT = 'reject'.freeze

  STATUSES = [self::PENDING, self::PROCESS, self::PAID, self::REJECT].freeze

  def activities
    activities = Activity.where(activitiable_id: self.id, activitiable_type: self.class.name)
  end

  def status?
    if self.status == "process"
      return "<label class='label label-info'>Process</label>".html_safe
    elsif self.status == "paid"
      return "<label class='label label-success'>Paid</label>".html_safe
    elsif self.status == "reject"
      return "<label class='label label-danger'>Reject</label>".html_safe
    elsif self.status == "pending"
      return "<label class='label label-warning'>Pending</label>".html_safe
    end
  end

  def is_pending?
    self.status == PaymentSnapshot::PENDING
  end

  def is_process?
    self.status == PaymentSnapshot::PROCESS
  end

  def is_paid?
    self.status == PaymentSnapshot::PAID
  end

  def is_reject?
    self.status == PaymentSnapshot::REJECT
  end

  private
    def before_create_service
      self.code = 'PC'+ 10.times.map{Random.rand(10)}.join
    end
end
