class Feed
  include Mongoid::Document
  field :group_id, type: String
  field :user_id, type: Integer
  field :message, type: String
  field :attachment_image, type: String
  field :status, type: String, default: 'active'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ group_id: 1, user_id: 1})

  mount_uploader :attachment_image, PhotoUploader

  scope :activated, ->{where(status: 'active')}
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :group, ->{ne(group_id: nil)}
  scope :public, ->{where(group_id: nil)}

  before_create :before_create_service
  before_save :before_update_service
  before_destroy :destroy_feed_comments

  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze

  STATUSES = [self::ACTIVE, self::INACTIVE].freeze

  include TheForums::ForumsSearching

  def user
    user = User.find_by(id: self.user_id)
  end

  def feed_comments
    feed_comments = FeedComment.where(feedable_id: self.id, feedable_type: FeedComment::Feed)
  end

  def likes
    likes = Like.where(feed_id: self.id)
  end

  def group
    if self.group_id.present?
      group = Group.find_by(id: self.group_id)
    else
      return nil
    end
  end

  def status?
    if self.status == "active"
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end

    def destroy_feed_comments
     self.feed_comments.delete_all   
   end
end
