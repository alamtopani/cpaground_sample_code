class Owner < User
  default_scope {where(type: User::OWNER)}

  CEO = 5
  CTO = 6
  CMO = 7
  OP = 8

  OWNER_TYPES = [['CEO', Owner::CEO], ['CTO', Owner::CTO], ['CMO', Owner::CMO], ['OP', Owner::OP]]

  def status_owner?
    if self.role_id == Owner::CEO
      return 'CEO'
    elsif self.role_id == Owner::CTO
      return 'CTO'
    elsif self.role_id == Owner::CMO
      return 'CMO'
    elsif self.role_id == Owner::OP
      return 'OP'
    end
  end

  def fee
    if self.role_id == Owner::CEO
      return 0.375
    elsif self.role_id == Owner::CTO
      return 0.25
    elsif self.role_id == Owner::CMO
      return 0.125
    elsif self.role_id == Owner::OP
      return 0.25
    end
  end

  def report_campaign_snapshots
    report_campaign_snapshots = ReportCampaignSnapshot.latest
  end

  def invoice_owners
    invoice_owners = InvoiceOwner.where(owner_id: self.id).latest
  end
end