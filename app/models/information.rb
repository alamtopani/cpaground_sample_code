class Information
  include Mongoid::Document
  field :title, type: String
  field :description, type: String
  field :status, type: String, default: 'inactive'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ status: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :actived, ->{where(status: "active")}

  include TheInformations::InformationsSearching

  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze
  PROMOTE = 'promote'.freeze

  STATUSES = [self::ACTIVE, self::INACTIVE].freeze

  def status?
    if self.status == "active"
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  def send_email_blast
    AccountManager.verified.pluck(:email).each_slice(500).to_a.map{|m| 
      managers = m.join(", ")
      UserMailer.send_email_blast_information(managers, self, "Manager").deliver_now!
    }
    Publisher.verified.no_suspended.pluck(:email).each_slice(500).to_a.map{|p| 
      publishers = p.join(", ")
      UserMailer.send_email_blast_information(publishers, self, "Publisher").deliver_now!
    }
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
