class GroupMessage
  include Mongoid::Document
  field :publisher_id, type: Integer
  field :account_manager_id, type: Integer
  field :status, type: String, default: 'active'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :active, ->{where(status: "active")}
  scope :nonactive, ->{where(status: "nonactive")}

  index({ publisher_id: 1, account_manager_id: 1, status: 1})

  has_many :messages

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
