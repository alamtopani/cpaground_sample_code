class CategoryCampaign
	include Mongoid::Document
	field :code, type: String
	field :slug, type: String
	field :name, type: String
	field :link, type: String
	field :action, type: String
	field :category_id, type: String
	field :detail, type: String
	field :featured, type: Boolean, default: false
	field :sales, type: Integer, default: 0
	field :status, type: String, default: 'inactive'
	field :auto_approved, type: Boolean, default: true
	field :need_approval, type: Boolean, default: false
	field :expires, type: Date
	field :cover, type: String
	field :link_offer, type: String
	field :created_at, type: DateTime
	field :updated_at, type: DateTime

	index({ code: 1,category_id: 1, slug: 1})

	before_create :before_create_service
	before_save :before_update_service

	scope :updated, ->{order(updated_at: :desc)}
	scope :latest, ->{order(created_at: :desc)}
	scope :oldest, ->{order(created_at: :asc)}
	scope :active, ->{where(status: CategoryCampaign::ACTIVE)}
	scope :promo, ->{where(featured: true)}
	scope :code_blank, ->{where(code: nil)}
	scope :hot_offers, ->{order(sales: :desc)}
	scope :need_approval, ->{where(need_approval: true)}

	include TheCategoryCampaigns::CategoryCampaignsSearching

	mount_uploader :cover, ImageUploader

	# ------- RELATIONS --------
	has_many :campaigns
	has_many :convertion_reports
	has_many :request_landing_pages
	has_many :targetings, dependent: :destroy
	has_many :banner_images, dependent: :destroy
	accepts_nested_attributes_for :targetings, reject_if: :all_blank, allow_destroy: true
	accepts_nested_attributes_for :banner_images, reject_if: :all_blank, allow_destroy: true

	def category
		category = CategoryCampaign::CATEGORIES.select{|c| c[:id] == self.category_id.to_i}[0]
	end

	def category_name
		category = CategoryCampaign::CATEGORIES.select{|c| c[:id] == self.category_id.to_i}[0][:name]
	end

	def sales?
		campaign_ids = Campaign.where(category_campaign_id: self.id).pluck(:id)
		report_campaign_snapshots = ReportCampaignSnapshot.where(:sales.gte => 1).where(campaign_id: { :$in => campaign_ids }).sum(:sales) rescue 0
	end

	# ------- STATUS --------
	ACTIVE = 'active'.freeze
	INACTIVE = 'inactive'.freeze

	STATUSES = [self::ACTIVE, self::INACTIVE].freeze

	SHORTERS = [["Top Sales", "top sales"], ["Featured", "featured"], ["Latest", "latest"], ["Oldest", "oldest"]].freeze

	ACTIONS = ["CPA", "CPI", "CPS"].freeze

	AUTO_APPROVED = [["Auto", true], ["Manual", false]].freeze

	NEED_APPROVAL = [["Yes", true], ["No", false]].freeze

	CATEGORIES = CategoryName::NAMES

	CATEGORY_NAMES = CategoryCampaign::CATEGORIES.pluck(:name, :id).freeze

	DOMAIN_LINK_BASIC = 'https://tcground.com'
	HTTP_DOMAIN_LINK_OFFER = 'https://a.groundtc.com'
	ALL_ACTIVE_DOMAIN = ['tcground.com', 'groundtc.com']

	def auto_approvement?
		self.auto_approved == true
	end

	def approval?
		self.need_approval == true
	end

	def has_applied?(publisher_id)
		AccessOffer.where(publisher_id: publisher_id, category_campaign_id: self.id).present?
	end

	def banned_applied?(publisher_id)
		if self.has_applied?(publisher_id)
			access_offer = AccessOffer.where(publisher_id: publisher_id, category_campaign_id: self.id).last
			if access_offer.status == "banned"
				return true
			else
				return false
			end
		else
			return false
		end
	end

	def checked_applied?(publisher_id)
		access_offer = AccessOffer.find_by(publisher_id: publisher_id, category_campaign_id: self.id)
		if access_offer.checked == true
			return true
		else
			return false
		end
	end
	
	def status?
		if self.status == "active"
			return "<label class='label label-success'>Active</label>".html_safe
		else
			return "<label class='label label-danger'>Inactive</label>".html_safe
		end
	end

	def approvement?
		if self.auto_approved == true
			return "<label class='label label-default'>Auto</label>".html_safe
		else
			return "<label class='label label-primary'>Manual</label>".html_safe
		end
	end

	def need_approval?
		if self.need_approval == true
			return "<label class='label label-default'>Yes</label>".html_safe
		else
			return "<label class='label label-primary'>No</label>".html_safe
		end
	end

	def featured?
		if self.featured == true
			return "<label class='label label-warning'>Featured</label>".html_safe
		end
	end

	def activities
		activities = Activity.where(activitiable_id: self.id)
	end

	def access_offers
		access_offers = AccessOffer.where(category_campaign_id: self.id)
	end

	def request(account_manager_id)
		request = self.access_offers.where(account_manager_id: account_manager_id, checked: false)
	end

	def approved(account_manager_id)
		approved = self.access_offers.where(account_manager_id: account_manager_id, checked: true, status: "approve")
	end

	def banned(account_manager_id)
		banned = self.access_offers.where(account_manager_id: account_manager_id, status: "banned")
	end

	def is_cps?
		self.action == "CPS"
	end

	def is_cpa?
		self.action == "CPA"
	end

	def user_campaigns(user)
		user_campaigns = Campaign.where(category_campaign_id: self.id, publisher_id: user.id)
	end

	def user_sales(user)
		user_sales = ConvertionReport.where(category_campaign_id: self.id, publisher_id: user.id).have_sales_only
	end

  def self.import(params)
    spreadsheet = Roo::Spreadsheet.open(params[:file].path)

    header = spreadsheet.row(1)
    hash = Hash.new
    hash = (2..spreadsheet.last_row).map{|i| row = Hash[[header, spreadsheet.row(i)].transpose]}
    targetings = hash.group_by {|r| r['offer_id']}
    targetings.each do |key, values|
      if key.present?
        offer = CategoryCampaign.find_by(id: key)

        if offer.present?
          offer_id = offer.id
          values.each do |resource|
            targeting = Targeting.all_of({category_campaign_id: offer_id}, {country_code: resource['country_code'].to_s}).first
            targeting = targeting.blank? ? Targeting.new : targeting
            targeting.category_campaign_id = offer_id
            targeting.country_code = resource['country_code'].to_s
            targeting.payout_price = resource['payout_price'].to_f
            targeting.fee_publisher = resource['fee_publisher'].to_f
            targeting.fee_am = resource['fee_am'].to_f
            targeting.fee_company = resource['fee_company'].to_f
            targeting.save
          end
        end
      end
    end
  end

	def self.import_per_offer(params, offer)
		spreadsheet = Roo::Spreadsheet.open(params[:file].path)

		header = spreadsheet.row(1)
		hash = Hash.new
		hash = (2..spreadsheet.last_row).map{|i| row = Hash[[header, spreadsheet.row(i)].transpose]}
		targetings = hash
		targetings.each do |resource|
			targeting = Targeting.all_of({category_campaign_id: offer.id}, {country_code: resource['country_code'].to_s}).first
			targeting = targeting.blank? ? Targeting.new : targeting
			targeting.category_campaign_id = offer.id
			targeting.country_code = resource['country_code'].to_s
			targeting.payout_price = resource['payout_price'].to_f
			targeting.fee_publisher = resource['fee_publisher'].to_f
			targeting.fee_am = resource['fee_am'].to_f
			targeting.fee_company = resource['fee_company'].to_f
			targeting.save
		end
	end

	def send_message(campaign, message)
		group_message_id = GroupMessage.find_by(account_manager_id: campaign.account_manager_id, publisher_id: campaign.publisher_id)
		@message = Message.new
		@message.group_message_id = group_message_id
		@message.from = campaign.account_manager_id
		@message.to = campaign.publisher_id
		@message.message = message
		@message.save
	end

	def blast_message(message, subject, start_email_content, end_email_content)
		@publishers = Publisher.verified.no_suspended
		@publishers.pluck(:email).each_slice(500).to_a.map{|p| 
			publishers = p.join(", ")
			UserMailer.send_email_blasts_offer(publishers, self, subject, start_email_content, end_email_content).deliver_now!
		}
	end

	def inactive_campaign
		self.campaigns.each do |campaign|
			if campaign.status == Campaign::ACTIVE
				campaign.status = Campaign::COMPLETE
				campaign.save
				send_message(campaign, "Offer (#{self.name}) for your campaign (#{campaign.title}) has expired and no longer valid.")
			end
		end
		self.campaigns.group_by{|c| c.publisher_id}.values.map{|r| [Publisher.find(r.last.publisher_id).email]}.each_slice(500).to_a.map{|p| 
			publishers = p.join(", ")
			UserMailer.send_email_blasts_offer(publishers, self, "Offer (#{self.name}) has expired.", "With great regret we convey, the following offer paused by advertiser :", "Apart from the offer is smooth and still good to use.").deliver_now!
		}
	end

	def active_campaign
		self.campaigns.each do |campaign|
			if campaign.status == Campaign::COMPLETE
				campaign.status = Campaign::ACTIVE
				campaign.save
				send_message(campaign, "Offer (#{self.name}) for your campaign (#{campaign.title}) has been reactivated.")
			end
		end
	end

	private
		def before_create_service
			begin
				code = 'OF'+ 5.times.map{Random.rand(10)}.join
			end while CategoryCampaign.where(code: code).present?
			self.code = code
			self.created_at = Time.zone.now
			self.updated_at = Time.zone.now
			self.slug = self.name.parameterize #For create title to slug
		end

		def before_update_service
			self.updated_at = Time.zone.now
			self.slug = self.name.parameterize #For create title to slug
		end
end
