class Activity
  include Mongoid::Document
  field :user_id, type: Integer
  field :name, type: String
  field :description, type: String
  field :activitiable_id, type: String 
  field :activitiable_type, type: String
  field :status, type: String, default: "unread"
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ user_id: 1, activitiable_id: 1, activitiable_type: 1, status: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :unread, ->{where(status: "unread")}

  include TheActivities::ActivitiesSearching

  # ------- RELATIONS --------

  def user
    user = User.find_by(id: self.user_id)
  end

  def admin
    admin = Admin.find_by(id: self.user_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.user_id)
  end

  def publisher
    publisher = Publisher.find_by(id: self.user_id)
  end

  def campaign
    if self.activitiable_type == "Campaign"
      campaign = Campaign.find_by(id: self.activitiable_id)
    end
  end

  def report_campaign_snapshot
    if self.activitiable_type == "ReportCampaignSnapshot"
      report_campaign_snapshot = ReportCampaignSnapshot.find_by(id: self.activitiable_id)
    end
  end

  def category_campaign
    if self.activitiable_type == "CategoryCampaign"
      category_campaign = CategoryCampaign.find_by(id: self.activitiable_id)
    end
  end

  def payment_snapshot
    if self.activitiable_type == "PaymentSnapshot"
      payment_snapshot = PaymentSnapshot.find_by(id: self.activitiable_id)
    end
  end

  def contact
    if self.activitiable_type == "Contact"
      contact = Contact.find_by(id: self.activitiable_id)
    end
  end

  def subscriber
    if self.activitiable_type == "Subscriber"
      subscriber = Subscriber.find_by(id: self.activitiable_id)
    end
  end


  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
