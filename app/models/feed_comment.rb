class FeedComment
  include Mongoid::Document
  field :user_id, type: Integer
  field :feedable_id, type: String
  field :feedable_type, type: String
  field :message, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ user_id: 1, feedable_type: 1, feedable_id: 1})

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  before_create :before_create_service
  before_save :before_update_service

  BLOG = 'Blog'
  FEED = 'Feed'

  def user
    user = User.find_by(id: self.user_id)
  end

  def feedable
    if self.feedable_type == FeedComment::Blog
      feedable = Blog.find_by(id: self.feedable_id)
    elsif self.feedable_type == FeedComment::FEED
      feedable = Feed.find_by(id: self.feedable_id)
    end
  end

  def check_feed
    Feed.where(id: self.feedable_id)
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
