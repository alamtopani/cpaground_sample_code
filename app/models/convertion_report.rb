class ConvertionReport
  include Mongoid::Document
  field :click_id, type: String
  field :report_campaign_snapshot_id, type: String
  field :category_campaign_id, type: String
  field :campaign_id, type: String
  field :publisher_id, type: Integer
  field :account_manager_id, type: Integer
  field :admin_id, type: Integer
  field :ip, type: String
  field :country_code, type: String
  field :country_name, type: String
  field :region_code, type: String
  field :region_name, type: String
  field :city, type: String
  field :referrer, type: String
  field :latitude, type: String
  field :longitude, type: String
  field :sales, type: Integer, default: 0
  field :revenue, type: Float, default: 0
  field :fee_publisher, type: Float, default: 0
  field :fee_am, type: Float, default: 0
  field :fee_company, type: Float, default: 0
  field :convertion_date, type: Date
  field :convertion_time, type: DateTime
  field :device, type: String
  field :browser, type: String
  field :provider, type: String
  field :os, type: String
  field :user_agent, type: String
  field :source, type: String
  field :sub1, type: String
  field :sub2, type: String
  field :status, type: String, default: 'visited'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ click_id: 1, publisher_id: 1,account_manager_id: 1,campaign_id: 1,sales: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :approved, ->{where(status: ConvertionReport::APPROVED)}
  scope :reject, ->{where(status: ConvertionReport::REJECT)}
  scope :pending, ->{where(status: ConvertionReport::PENDING)}
  scope :visited, ->{where(status: ConvertionReport::VISITED)}
  scope :have_sales_only, ->{where(:sales.gte => 1)}
  scope :have_sales, ->{where(:status.in => [self::APPROVED, self::REJECT, self::PENDING])}

  belongs_to :campaign
  belongs_to :category_campaign
  has_one :commision, dependent: :destroy
  # belongs_to :report_campaign_snapshot
  has_many :report_items, dependent: :destroy
  accepts_nested_attributes_for :report_items, reject_if: :all_blank, allow_destroy: true

  include TheConvertionReports::ConvertionReportsSearching
  include TheConvertionReports::ConvertionReportsSearchingAnalytic

  validates_presence_of :click_id
  validates_uniqueness_of :click_id

  # ------- STATUS --------
  APPROVED = 'approved'.freeze
  REJECT = 'reject'.freeze
  PENDING = 'pending'.freeze
  VISITED = 'visited'.freeze
  CONVERTION = 'convertion'.freeze

  STATUSES = [self::CONVERTION, self::APPROVED, self::REJECT, self::PENDING, self::VISITED].freeze
  STATUSES2 = [self::CONVERTION, self::APPROVED, self::REJECT, self::PENDING].freeze
  STATUSES_WITHOUT_VISITED = [self::PENDING, self::APPROVED, self::REJECT].freeze
  STATUSES_FOR_LIST = [self::VISITED, self::APPROVED, self::PENDING, self::REJECT].freeze

  def status?
    if self.status == ConvertionReport::APPROVED
      return '<i class="pe-7s-check" data-toggle="tooltip" title="Approved"></i>'.html_safe
    elsif self.status == ConvertionReport::REJECT
      return '<i class="pe-7s-attention" data-toggle="tooltip" title="Reject"></i>'.html_safe
    elsif self.status == ConvertionReport::PENDING
      return '<i class="pe-7s-coffee" data-toggle="tooltip" title="Pending"></i>'.html_safe
    elsif self.status == ConvertionReport::VISITED
      return ''.html_safe
    end
  end

  def admin
    admin = Admin.find_by(id: self.admin_id)
  end

  def publisher
    publisher = Publisher.find_by(id: self.publisher_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.account_manager_id)
  end

  def report_campaign_snapshot
    report_campaign_snapshot = ReportCampaignSnapshot.find_by(id: self.report_campaign_snapshot_id)
  end

  def campaign
    campaign = Campaign.find_by(id: self.campaign_id)  
  end

  def activities
    activities = Activity.where(activitiable_id: self.id)
  end

  def convertion_revenue(user_type)
    if user_type == User::PUBLISHER || user_type == User::ACCOUNTMANAGER
      self.fee_publisher
    else
      self.revenue
    end
  end

  def convertion_revenue_am
    self.fee_am
  end

  def report_items_sales
    report_items_sales = ReportItem.where(convertion_report_id: self.id, status: ReportItem::APPROVED)
  end

  def report_items_sales_pending
    report_items_sales = ReportItem.where(convertion_report_id: self.id, status: ReportItem::PENDING)
  end

  def report_items_sales_reject
    report_items_sales = ReportItem.where(convertion_report_id: self.id, status: ReportItem::REJECT)
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
