class Admin < User
  default_scope {where(type: User::ADMIN)}

  has_many :payment_snapshots
  has_many :publishers

  include TheUsers::AdminsSearching

  def campaigns
    campaigns = Campaign.where(admin_id: self.id)
  end

  def report_campaign_snapshots
    report_campaign_snapshots = ReportCampaignSnapshot.where(admin_id: self.id)
  end

  def verification_snapshots
    verification_snapshots = VerificationSnapshot.where(admin_id: self.id)
  end

  def activities
    activities = Activity.where(user_id: self.id)
  end
end