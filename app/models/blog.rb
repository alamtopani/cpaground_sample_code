class Blog
  include Mongoid::Document
  include Mongoid::Slug
  field :slug, type: String
  field :user_id, type: Integer
  field :title, type: String
  field :description, type: String
  field :category, type: String
  field :attachment_image, type: String
  field :status, type: String, default: 'active'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :view_count, type: Integer, default: 0
  slug :title

  index({ user_id: 1, slug: 1})

  mount_uploader :attachment_image, PhotoUploader

  scope :activated, ->{where(status: "active")}
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  include TheBlogs::BlogsSearching

  before_create :before_create_service
  before_save :before_update_service
  before_destroy :destroy_feed_comments

  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze

  STATUSES = [self::ACTIVE, self::INACTIVE].freeze

  # ------- CATEGORIY --------
  HOWTO = 'how to'.freeze
  NEWS = 'news'.freeze
  EVENT = 'event'.freeze

  CATEGORIES = [self::HOWTO, self::NEWS, self::EVENT].freeze

  def status?
    if self.status == "active"
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  def is_inactive?
    self.status == Blog::INACTIVE
  end

  def user
    user = User.find_by(id: self.user_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.user_id)
  end

  def feed_comments
    feed_comments = FeedComment.where(feedable_id: self.id, feedable_type: FeedComment::Blog)
  end

  def activities
    activities = Activity.where(activitiable_id: self.id)
  end

  def current_user_blog(current_user_id)
    self.user_id == current_user_id && self.is_inactive?

  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end

    def destroy_feed_comments
     self.feed_comments.delete_all   
   end
end
