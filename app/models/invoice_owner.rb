class InvoiceOwner
  include Mongoid::Document

  field :owner_id, type: Integer
  field :code, type: String
  field :title, type: String
  field :description, type: String
  field :price, type: Float, default: 0
  field :transaction_date, type: Date
  field :from_acc, type: String
  field :to_acc, type: String
  field :attachment, type: String
  field :status, type: String, default: 'pending'
  field :payment_at, type: Date
  field :payment_end, type: Date
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ code: 1, status: 1})

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :pending, -> { where(status: self::PENDING) }
  scope :process, -> { where(status: self::PROCESS) }
  scope :paid, -> { where(status: self::PAID) }
  scope :rejects, -> { where(status: self::REJECT) }
  
  before_create :before_create_service
  before_save :before_update_service

  include TheInvoices::InvoicesSearching

  mount_uploader :attachment, PhotoUploader

  # ============
  #   STATUSES
  # ============

  PENDING = 'pending'.freeze
  PROCESS = 'process'.freeze
  PAID = 'paid'.freeze
  REJECT = 'reject'.freeze

  STATUSES = [self::PENDING, self::PROCESS, self::PAID, self::REJECT].freeze

  def owner
    owner = Owner.find_by(id: self.owner_id)
  end

  def status?
    if self.status == "process"
      return "<label class='label label-info'>Process</label>".html_safe
    elsif self.status == "paid"
      return "<label class='label label-success'>Paid</label>".html_safe
    elsif self.status == "reject"
      return "<label class='label label-danger'>Reject</label>".html_safe
    elsif self.status == "pending"
      return "<label class='label label-warning'>Pending</label>".html_safe
    end
  end

  def is_pending?
    self.status == Invoice::PENDING
  end

  def is_process?
    self.status == Invoice::PROCESS
  end

  def is_paid?
    self.status == Invoice::PAID
  end

  def is_reject?
    self.status == Invoice::REJECT
  end

  private
    def before_create_service
      code = 'IO'+ 10.times.map{Random.rand(10)}.join
      self.code = code
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
