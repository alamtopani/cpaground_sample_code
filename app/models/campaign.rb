class Campaign
  include Mongoid::Document
  field :code, type: String
  field :admin_id, type: Integer
  field :account_manager_id, type: Integer
  field :publisher_id, type: Integer
  field :category_campaign_id, type: String
  field :title, type: String 
  field :status, type: String, default: 'pending' #, deafult: "pending" # Given by staff admin
  field :banned, type: Boolean, default: false #, deafult: "false" # Given by staff admin
  field :link, type: String # Given by staff admin
  field :tracking_code, type: String # Given by staff admin
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ publisher_id: 1, account_manager_id: 1, category_campaign_id: 1, code: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :active, ->{where(status: Campaign::ACTIVE)}
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  # ------- RELATIONS --------
  has_many :report_campaign_snapshots
  has_many :convertion_reports
  belongs_to :category_campaign
  has_many :request_landing_pages

  include TheCampaigns::CampaignsSearching

  def admin
    admin = Admin.find_by(id: self.admin_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.account_manager_id)
  end

  def publisher
    publisher = Publisher.find_by(id: self.publisher_id)
  end

  def activities
    activities = Activity.where(activitiable_id: self.id)
  end

  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze
  PENDING = 'pending'.freeze
  COMPLETE = 'complete'.freeze

  STATUSES = [self::ACTIVE, self::COMPLETE, self::INACTIVE, self::PENDING].freeze
  STATUSES_WITHOUT_COMPLETE = [self::ACTIVE, self::INACTIVE, self::PENDING].freeze

  def tracking_code?
    self.tracking_code.gsub(/\s+/, "")
  end

  def status?
    if self.banned == true
      return "<label class='label label-danger'>Banned</label>".html_safe
    else
      if self.status == "complete"
        return "<label class='label label-info'>Complete</label>".html_safe
      elsif self.status == "active"
        return "<label class='label label-success'>Active</label>".html_safe
      elsif self.status == "pending"
        return "<label class='label label-warning'>Pending</label>".html_safe
      else
        return "<label class='label label-danger'>Inactive</label>".html_safe
      end
    end
  end

  def is_active?
    self.status == Campaign::ACTIVE
  end

  def is_pending?
    self.status == Campaign::PENDING
  end

  def is_complete?
    self.status == Campaign::COMPLETE
  end

  def is_banned?
    self.banned == true
  end

  def new_data?
    if self.admin.blank?
      return "<label class='label label-danger'>New</label>".html_safe
    else
      return ""
    end
  end

  def last_updated?
    if self.report_campaign_snapshots.present?
      return self.report_campaign_snapshots.last.created_at
    else
      return self.updated_at
    end
  end

  def check_approved?
    if self.category_campaign.auto_approved == true
      return true
    else
      return false
    end
  end

  def self.import(params)
    spreadsheet = Roo::Spreadsheet.open(params[:file].path)

    if params[:type] == 'network'
      header = spreadsheet.row(1)
      hash = Hash.new
      hash = (2..spreadsheet.last_row).map{|i| row = Hash[[header, spreadsheet.row(i)].transpose]}
      reports = hash.group_by {|r| r['s2']}
      reports.each do |key, value|
        campaign = Campaign.where(id: key).first

        if campaign.present?
          campaign_id = campaign.id
          campaign_date = Time.zone.now.to_date
          @report_campaign_snapshot = ReportCampaignSnapshot.where(campaign_date: campaign_date).where(campaign_id: campaign_id).first
          if @report_campaign_snapshot.present?
            @report_campaign_snapshot.sales = value.count
            @report_campaign_snapshot.revenue = value.map{|n| n['payout'].to_f}.sum
          else
            @report_campaign_snapshot = ReportCampaignSnapshot.new
            @report_campaign_snapshot.sales = value.count
            @report_campaign_snapshot.revenue = value.map{|n| n['payout'].to_f}.sum
          end
          @report_campaign_snapshot.campaign_id = campaign_id
          @report_campaign_snapshot.campaign_date = campaign_date
          @report_campaign_snapshot.admin_id = campaign.admin_id
          @report_campaign_snapshot.publisher_id = campaign.publisher_id
          @report_campaign_snapshot.account_manager_id = campaign.account_manager_id
          @report_campaign_snapshot.save
        end
      end
    elsif params[:type] == 'analytic'
      header = spreadsheet.row(7)
      hash = Hash.new
      hash = (8..spreadsheet.last_row).map{|i| row = Hash[[header, spreadsheet.row(i)].transpose]}
      hash = hash.map{|h| {page: h['Page'], visitors: h['Pageviews'], unique_visitors: h['Unique Pageviews']}}
      hash = hash.map{|h| {page: h[:page].to_s.include?('s2=') == true ? h[:page].split('s2=')[1] : nil, visitors: h[:visitors], unique_visitors: h[:unique_visitors]} }

      reports = hash.group_by {|r| r[:page]}
      
      reports.each do |key, value|
        campaign = Campaign.where(id: key).first

        if campaign.present?
          campaign_id = campaign.id
          campaign_date = Time.zone.now.to_date
          @report_campaign_snapshot = ReportCampaignSnapshot.where(campaign_date: campaign_date).where(campaign_id: campaign_id).first
          if @report_campaign_snapshot.present?
            @report_campaign_snapshot.visitors = value.map{|n| n[:visitors].to_f}.sum
            @report_campaign_snapshot.unique_visitors = value.map{|n| n[:unique_visitors].to_f}.sum
          else
            @report_campaign_snapshot = ReportCampaignSnapshot.new
            @report_campaign_snapshot.visitors = value.map{|n| n[:visitors].to_f}.sum
            @report_campaign_snapshot.unique_visitors = value.map{|n| n[:unique_visitors].to_f}.sum
          end
          @report_campaign_snapshot.campaign_id = campaign_id
          @report_campaign_snapshot.campaign_date = campaign_date
          @report_campaign_snapshot.admin_id = campaign.admin_id
          @report_campaign_snapshot.publisher_id = campaign.publisher_id
          @report_campaign_snapshot.account_manager_id = campaign.account_manager_id
          @report_campaign_snapshot.save
        end
      end
    end
  end 

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def sales?
    report_campaign_snapshots = ReportCampaignSnapshot.where(campaign_id: self.id).have_sales_only.sum(:sales) rescue 0
  end

  private
    def before_create_service
      self.code = 'cp'+self.id.to_s
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end

    def create_activity(resource, user_id, name, description)
      Activity.create(user_id: user_id, name: name, description: description, activitiable_id: resource.id, activitiable_type: resource.class.name)
    end
end
