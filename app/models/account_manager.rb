class AccountManager < User
  default_scope {where(type: User::ACCOUNTMANAGER)}
  scope :verified, ->{where(verified: true)}

  has_many :payment_snapshots, foreign_key: "user_id"
  has_one :profile_account_manager
  accepts_nested_attributes_for :profile_account_manager, allow_destroy: true
  
  include TheUsers::AccountManagersSearching

  before_create :before_create_service

  after_initialize :prepare_profile
  before_save :remove_blank_profile

  # ------- RELATIONS --------    
  has_many :publishers

  def campaigns
    campaigns = Campaign.where(account_manager_id: self.id)
  end

  def report_campaign_snapshots
    report_campaigns = ReportCampaignSnapshot.where(account_manager_id: self.id)
  end

  def verification_snapshots
    verification_snapshots = VerificationSnapshot.where(account_manager_id: self.id)
  end

  def activities
    activities = Activity.where(user_id: self.id)
  end

  def request_landing_pages
    request_landing_pages = RequestLandingPage.where(account_manager_id: self.id)
  end

  def convertion_reports
    convertion_reports = ConvertionReport.where(account_manager_id: self.id)
  end

  def referrals
    referrals = Publisher.where(referral: self.username)
  end
  
  def groups
    groups = Group.where(user_id: self.id)
  end

  def permission_referral_publishers
    permission_referral_publishers = PermissionReferralPublisher.where(account_manager_id: self.id)
  end

  def commisions
    commisions = Commision.where(account_manager_id: self.id)
  end

  def status?
    if self.verified?
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  # ------- ATTRIBUTES --------
  def get_address_lat
    self.profile_account_manager.present? && self.profile_account_manager.latitude.present? ? self.profile_account_manager.latitude : 54.92299878585206
  end

  def get_address_long
    self.profile_account_manager.present? && self.profile_account_manager.longitude.present? ? self.profile_account_manager.longitude : -2.9421016573905945
  end

  def the_name?
    if self.profile_account_manager.present? && self.profile_account_manager.first_name.present?
      self.profile_account_manager.full_name
    else
      self.username
    end
  end

  private
    def before_create_service
      self.role_id = User::AM_ROLE
    end

    def prepare_profile
      self.profile_account_manager = ProfileAccountManager.new unless self.profile_account_manager.present? 
    end

    def remove_blank_profile
      ProfileAccountManager.where(account_manager_id: nil).destroy_all
    end
end