class AccessOffer
  include Mongoid::Document
  field :publisher_id, type: Integer
  field :account_manager_id, type: Integer
  field :category_campaign_id, type: String
  field :status, type: String, default: 'approve'
  field :checked, type: Boolean, default: false
  field :message, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ publisher_id: 1, account_manager_id: 1, category_campaign_id: 1, status: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :approved, ->{where(status: "approve")}
  scope :banned, ->{where(status: "banned")}

  include TheAccessOffers::AccessOffersSearching

  # ------- STATUS --------
  APPROVE = 'approve'.freeze
  BANNED = 'banned'.freeze
  CHECKED = 'checked'.freeze

  STATUSES = [self::APPROVE, self::BANNED].freeze

  CHECKEDS = [["Yes", true], ["No", false]].freeze
  
  def status?
    if self.status == "approve"
      return "<label class='label label-success'>Approve</label>".html_safe
    else
      return "<label class='label label-danger'>Banned</label>".html_safe
    end
  end
  
  def label_checked?
    if self.checked == true
      return "<label class='label label-success'>Yes</label>".html_safe
    else
      return "<label class='label label-danger'>No</label>".html_safe
    end
  end

  def publisher
    publisher = Publisher.find_by(id: self.publisher_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.account_manager_id)
  end

  def offer
    offer = CategoryCampaign.find_by(id: self.category_campaign_id)
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
