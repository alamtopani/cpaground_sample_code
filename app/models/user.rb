class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]

  self.inheritance_column = nil

  devise  :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable,
          :validatable, :lockable, :confirmable, :omniauthable, omniauth_providers: [:facebook, :google_oauth2], :authentication_keys => [:login]

  mount_uploader :avatar, AvatarUploader
  mount_uploader :facebook_avatar, AvatarUploader
  mount_uploader :google_avatar, AvatarUploader
  mount_uploader :foto_id_card, CardUploader
  mount_uploader :selfi_id_card, CardUploader

  validates :email, uniqueness: { case_sensitive: false }
  validates :username, uniqueness: { case_sensitive: false }
  validates_uniqueness_of :id_card, conditions: -> { where.not(id_card: nil) }
  # validates :facebook_url, uniqueness: { case_sensitive: true }
  validates :phone_code,  :presence => {:message => 'number your entered is incorrect.'},
                     :numericality => true,
                     :length => { :minimum => 2, :maximum => 4 }
  validates :phone,  :presence => {:message => 'number your entered is incorrect.'},
                     :numericality => true,
                     :length => { :minimum => 4, :maximum => 15 }

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  has_many :payment_snapshots
  has_many :payment_commisions

  include UserModule::UserAuthenticate

  before_create :before_create_service
  before_validation :check_facebook_url

  # ------- TYPE BY ROLE --------
  PUBLISHER_ROLE = 0
  ADMIN_ROLE = 1
  STAFF_ROLE = 2
  FINANCE_ROLE = 3
  AM_ROLE = 4

  # ------- TYPE --------
  OWNER = 'owner'.freeze
  ADMIN = 'admin'.freeze
  ACCOUNTMANAGER = 'account_manager'.freeze
  PUBLISHER = 'publisher'.freeze
  FINANCE = 'finance'.freeze

  # ------- FEE USER --------
  FEE_PUBLISHER = 0.7
  FEE_AM = 0.1
  FEE_COMPANY = 0.2

  def is_owner?
    self.type == User::OWNER
  end

  def is_publisher?
    self.type == User::PUBLISHER 
  end

  def is_admin?
    self.type == User::ADMIN && self.role_id == User::ADMIN_ROLE
  end

  def is_staff?
    self.type == User::ADMIN && self.role_id == User::STAFF_ROLE
  end

  def is_finance?
    self.type == User::ADMIN && self.role_id == User::FINANCE_ROLE
  end

  def is_account_manager?
    self.type == User::ACCOUNTMANAGER
  end

  def is_subscriber?
    if Subscriber.where(user_id: self.id, status: Subscriber::ACTIVE).present?
      return true
    else
      return false
    end
  end

  def groups
    if self.is_publisher?
      groups = Group.all_of({:id.in => GroupCoverage.all_of({user_id: self.id}, {status: GroupCoverage::APPROVE}).pluck(:group_id)})
    elsif self.is_account_manager?
      groups = Group.where(user_id: self.id)
    else
      return nil
    end
  end

  def feed_in_your_group(feed)
    if self.is_account_manager? && feed.group.present? && feed.group.account_manager.id == self.id 
      return true
    end
  end

  def comment_in_your_group(comment)
    if self.is_account_manager? && comment.feedable.group.present? && comment.feedable.group.account_manager.id == self.id 
      return true
    end
  end

  def group_coverage(group_id)
    group_coverage = GroupCoverage.all_of({user_id: self.id}, {group_id: group_id}).last
  end

  def not_group_coverage?(group_id)
    if GroupCoverage.where(group_id: group_id, user_id: self.id).blank?
      return true
    else
      return false
    end
  end

  def is_group_admin?(group_id)
    if Group.where(id: group_id, user_id: self.id).present?
      return true
    end
  end

  def is_group_coverage?(group_id)
    if GroupCoverage.where(group_id: group_id, user_id: self.id, status: GroupCoverage::APPROVE).present?
      return true
    end
  end

  def type?
    if self.type == User::PUBLISHER
      User::PUBLISHER
    elsif self.type == User::ACCOUNTMANAGER
      User::ACCOUNTMANAGER
    elsif self.type == User::ADMIN
      User::ADMIN
    end
  end

  def status?
    if self.confirmed?
      if self.suspended?
          return "<label class='label label-danger label-user'>Suspended</label>".html_safe
      else
        if self.verified?
          return "<label class='label label-success label-user'>Active</label>".html_safe
        else
          return "<label class='label label-danger label-user'>Inactive</label>".html_safe
        end
      end
    else
      return "<label class='label label-danger label-user'>Email has not confirmed</label>".html_safe
    end
  end

  def the_name?
    if self.profile.present? && self.profile.first_name.present?
      self.profile.full_name
    else
      self.username
    end
  end

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def blogs
    blogs = Blog.where(user_id: self.id)
  end

  def get_phone_number
    if self.phone_code.present?
      self.phone_code + self.phone
    else
      self.phone
    end
  end

  def like_feed(feed_id)
    if Like.where(feed_id: feed_id, user_id: self.id).present?
      return true
    else
      return false
    end
  end

  def format_facebook_url
    if self.facebook_url.present?
      if self.facebook_url.include? "facebook.com/"
        facebook_url = self.facebook_url.split(".com/")[1]
        return "https://www.facebook.com/"+"#{facebook_url}"
      else
        return "https://www.facebook.com/"+"#{self.facebook_url}"
      end
    end
  end

  # ---------------- ACTIVITY CREATE ---------------------
  def self.prepare_activity(resource, user_id, name, description)
    Activity.create(user_id: user_id, name: name, description: description, activitiable_id: resource.id, activitiable_type: resource.class.name)
  end

  private
    def before_create_service
      self.code = 'PB'+ 10.times.map{Random.rand(10)}.join
    end

    def check_facebook_url
      unless self.is_admin? || self.is_account_manager?
        self.facebook_url = self.format_facebook_url
      end
    end
end
