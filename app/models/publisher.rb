class Publisher < User
  default_scope {where(type: User::PUBLISHER)}
  scope :no_suspended, ->{where(suspended: false)}
  scope :pending, ->{where(verified: false)}
  scope :verified, ->{where(verified: true)}
  scope :confirmed, ->{where.not(confirmed_at: nil)}
  scope :no_referral_publisher, ->{where(referral_publisher: nil, referral_token: nil)}

  # ------- RELATIONS --------
  has_one :profile
  accepts_nested_attributes_for :profile, allow_destroy: true
  has_many :payment_snapshots, foreign_key: "user_id"
  has_many :payment_commisions, foreign_key: "user_id"
  
  belongs_to :account_manager
  belongs_to :admin

  include TheUsers::PublishersSearching

  # after_initialize :prepare_profile
  before_save :remove_blank_profile


  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze
  SUSPENDED = 'suspended'.freeze

  STATUSES = [[self::ACTIVE, 1], [self::INACTIVE, 0]].freeze

  STATUS_SUSPENDED = [[self::ACTIVE, 0], [self::SUSPENDED, 1]].freeze

  VERIFY_EMAIL = [["confirmed", true], ["not confirmed", false]].freeze

  SEARCH_ORDER = [["oldest", "oldest"], ["latest", "latest"], ["have campaign", "have campaign"], ["have not campaign", "have not campaign"], ["have referrals", "have referrals"]].freeze
  
  def campaigns
    campaigns = Campaign.where(publisher_id: self.id)
  end

  def report_campaign_snapshots
    report_campaign_snapshots = ReportCampaignSnapshot.where(publisher_id: self.id)
  end

  def verification_snapshots
    verification_snapshots = VerificationSnapshot.where(publisher_id: self.id)
  end

  def activities
    activities = Activity.where(user_id: self.id)
  end

  def request_landing_pages
    request_landing_pages = RequestLandingPage.where(publisher_id: self.id)
  end

  def convertion_reports
    convertion_reports = ConvertionReport.where(publisher_id: self.id)
  end

  def access_offers
    access_offers = AccessOffer.where(publisher_id: self.id)
  end

  def is_verified?
    self.verified == true
  end

  def group_coverages
    group_coverages = GroupCoverage.where(user_id: self.id, status: GroupCoverage::APPROVE)
  end

  def groups
    if self.group_coverages.present?
      group_ids = self.group_coverages.pluck(:group_id).map{|group_id| {id: group_id}}
      groups = Group.any_of(group_ids)
      return groups
    else
      return nil
    end
  end

  def referral_am?
    referral_am = AccountManager.where(username: self.referral).present?
  end

  def waiting_to_active?
    if self.confirmed?
      unless self.verified?
        return true
      end
    else
      return false
    end
  end

  def referral_publishers
    referral_publishers = Publisher.where(referral_publisher: self.referral_token)
  end

  def referral_publisher_top
    referral_publisher_top = Publisher.find_by(referral_token: self.referral_publisher, referral_status: 'active')
  end

  def permission_referral_publisher
    permission_referral_publisher = PermissionReferralPublisher.find_by(publisher_id: self.id)
  end

  def commisions
    commisions = Commision.where(publisher_id: self.id)
  end

  def referral_status?
    if self.referral_status == "active"
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  def have_link_referral?
    self.referral_token != nil && self.referral_status == "active"
  end

  def acc_bank_present?
    if self.profile.acc_bank.present?
      return true
    end
  end

  # ------- ATTRIBUTES --------
  def get_address_lat
    self.profile.present? && self.profile.latitude.present? ? self.profile.latitude : 54.92299878585206
  end

  def get_address_long
    self.profile.present? && self.profile.longitude.present? ? self.profile.longitude : -2.9421016573905945
  end

  private
    def prepare_profile
      self.profile = Profile.new unless self.profile.present? 
    end

    def remove_blank_profile
      Profile.where(publisher_id: nil).destroy_all
    end
end