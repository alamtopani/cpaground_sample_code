class Profile < ApplicationRecord
  belongs_to :publisher, optional: true

  ACC_PAYPAL = 'Acc Paypal'.freeze
  ACC_PAYONEER = 'Acc Payoneer'.freeze
  ACC_BANK = 'Acc Bank'.freeze

  CHOICE_PAYMENT_TYPE = [[self::ACC_PAYPAL, "1"], [self::ACC_BANK, "3"]].freeze
  # CHOICE_PAYMENT_TYPE = [[self::ACC_PAYPAL, "1"], [self::ACC_PAYONEER, "2"], [self::ACC_BANK, "3"]].freeze

  # Select Bank Name
  BCA = 'BCA'.freeze
  BNI = 'BNI'.freeze
  MANDIRI = 'MANDIRI'.freeze

  CHOICE_BANK_NAME = [self::BCA, self::BNI, self::MANDIRI].freeze
  
  def get_payment_type
    type = self.choice_payment_type
    case type
    when "1" then type = Profile::ACC_PAYPAL
    when "2" then type = Profile::ACC_PAYONEER
    when "3" then type = Profile::ACC_BANK
    else
      return '--'
    end
  end

  def payment_acc
    type = self.choice_payment_type
    case type
    when "1" then type = self.acc_paypal
    when "2" then type = self.acc_payoneer
    when "3" then type = ["(#{self.bank_name})", "#{self.acc_name} -", self.acc_bank].select(&:'present?').join(' ')
    end
  end

  def payment_to
    [self.payment_acc, "(#{self.get_payment_type})"].select(&:'present?').join(' ')
  end

  def activities
    activities = Activity.where(activitiable_id: self.id)
  end

  def full_name
    [first_name, last_name].select(&:'present?').join(' ')
  end

  def sort_place_info
    [city, country_name].select(&:'present?').join(', ')
  end

  def place_info
    [address, city, country, zip_code].select(&:'present?').join(', ')
  end

  def country_name
    country = ISO3166::Country[self.country]
    if self.country.present?
      country.translations[I18n.locale.to_s] || country.name
    else
      self.country
    end
  end
end
