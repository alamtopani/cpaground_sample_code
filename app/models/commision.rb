class Commision
  include Mongoid::Document
  field :convertion_report_id, type: String
  field :account_manager_id, type: Integer
  field :publisher_id, type: Integer
  field :referral_publisher_id, type: Integer
  field :report_campaign_snapshot_id, type: String
  field :category_campaign_id, type: String
  field :campaign_id, type: String
  field :campaign_date, type: Date
  field :fee_am, type: Float
  field :fee_publisher, type: Float
  field :revenue, type: Float
  field :status, type: String, default: 'pending'
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ convertion_report_id: 1,report_campaign_snapshot_id: 1, campaign_id: 1, publisher_id: 1})

  before_create :before_create_service
  before_save :before_update_service

  belongs_to :convertion_report

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :approved, ->{where(status: "approved")}
  scope :pending, ->{where(status: "pending")}
  scope :rejected, ->{where(status: "reject")}

  include TheCommisions::CommisionsSearching

  # ------- RELATIONS --------
  def account_manager
    account_manager = AccountManager.find self.account_manager_id
  end

  def publisher
    publisher = Publisher.find self.publisher_id
  end

  def referral_publisher
    referral_publisher = Publisher.find self.referral_publisher_id
  end

  def convertion_report
    convertion_report = ConvertionReport.find self.convertion_report_id
  end

  def report_campaign_snapshot
    report_campaign_snapshot = ReportCampaignSnapshot.find self.report_campaign_snapshot_id
  end

  def category_campaign
    category_campaign = CategoryCampaign.find self.category_campaign_id
  end

  def campaign
    campaign = Campaign.find self.campaign_id
  end

  # ------- STATUS --------
  APPROVED = 'approved'.freeze
  PENDING = 'pending'.freeze
  REJECT = 'reject'.freeze

  STATUSES = [self::APPROVED, self::PENDING, self::REJECT].freeze

  GROUP_BY = ["publisher"].freeze
  
  def status?
    if self.status == "approved"
      return "<label class='label label-success'>Approved</label>".html_safe
    elsif self.status == "reject"
      return "<label class='label label-danger'>Reject</label>".html_safe
    else
      return "<label class='label label-warning'>Pending</label>".html_safe
    end
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
