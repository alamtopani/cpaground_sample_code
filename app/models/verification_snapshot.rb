class VerificationSnapshot # Snapshot automatic create after status active or inactive campaign
  include Mongoid::Document

  field :admin_id, type: Integer
  field :account_manager_id, type: Integer
  field :publisher_id, type: Integer
  field :status, type: String
  field :message, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ admin_id: 1, account_manager_id: 1, publisher_id: 1, status: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  include TheVerificationSnapshots::VerificationSnapshotsSearching

  # ------- RELATIONS --------
  def admin
    admin = Admin.find_by(id: self.admin_id)
  end

  def account_manager
    account_manager = AccountManager.find_by(id: self.account_manager_id)
  end

  def publisher
    publisher = Publisher.find_by(id: self.publisher_id)
  end

  def status?
    if self.status == "active"
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  # ------- STATUS --------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze

  STATUSES = [self::ACTIVE, self::INACTIVE].freeze

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
