class CpaImage < BannerImage
  IMAGES = [
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/250__50_Black_3Button_download_play-now.png', '250x50 Button download play now', '250x50'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/336__280_Black_2Button_download_play-now_EN.png', '336x280 Button download play now ', '336x280'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/336__280_Black_2btn_dld_fv_EN.png', '336x280 Button download full version', '336x280'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/336__280_Black_3Button_download_play-now_sign-up_free-trial_EN.png', '336x280 Button download play now sign up free trial', '336x280'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/336__280_Black_3btn_dld_fv_su_ft_EN.png', '336x280 Button download full version sign up free trial', '336x280'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/728__90_Black_2Button_download_play-now_EN.png', '728x90 Button download play-now', '728x90'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/728__90_Black_2btn_dld_fv_EN.png', '728x90 Button download full version', '728x90'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/728__90_Black_3Button_download_play-now_sign-up_free-trial_EN.png', '728x90 Button download play now sign up free trial', '728x90'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/728__90_Black_3btn_dld_fv_su_ft_EN.png', '728x90 Button download full version sign up free trial', '728x90'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/120__600-sept2014-Blue-skyscrapers_dl-WatchNow_EN.jpg', '120x600 Button download watch now sign', '120x600'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/120__600-sept2014-v1-covers.jpg', '120x600 Stream watch cover sign up', '120x600'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/120__600_Black_2Button_download_play-now_EN.png', '120x600 Button download play-now', '120x600'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/120__600_Black_2btn_dld_fv_EN.png', '120x600 Button download full version', '120x600'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/120__600_Black_3Button_download_play-now_sign-up_free-trial_EN.png', '120x600 Button download play now sign up free trial', '120x600'],
    ['https://s3-ap-southeast-1.amazonaws.com/cpaground/images/120__600_Black_3btn_dld_fv_su_ft_EN.png', '120x600 Button download full version sign up', '120x600']
  ]
end