class WebSetting
  include Mongoid::Document
  field :title, type: String
  field :description, type: String
  field :keywords, type: String
  field :header_tags, type: String
  field :footer_tags, type: String
  field :contact, type: String
  field :email, type: String
  field :skype, type: String
  field :favicon, type: String
  field :logo, type: String
  field :facebook, type: String
  field :twitter, type: String
  field :author, type: String
  field :longitude, type: String
  field :latitude, type: String
  field :address, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  mount_uploader :favicon, PhotoUploader
  mount_uploader :logo, PhotoUploader

  # ------- RELATIONS --------
  embeds_many :galleries
  accepts_nested_attributes_for :galleries

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
