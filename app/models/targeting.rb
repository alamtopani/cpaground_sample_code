class Targeting
  include Mongoid::Document
  field :category_campaign_id, type: String
  field :country_code, type: String
  field :payout_price, type: Float, default: 0
  field :fee_publisher, type: Float, default: 70
  field :fee_am, type: Float, default: 10
  field :fee_company, type: Float, default: 20
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ category_campaign_id: 1, country_code: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  belongs_to :category_campaign

  def fee_publisher?
    self.payout_price * (self.fee_publisher.to_f / 100)
  end

  def fee_am?
    self.payout_price * (self.fee_am.to_f / 100)
  end

  def fee_am_referral_publisher?(commision)
    commision = commision.to_f / 100
    fee_am = self.payout_price * (self.fee_am.to_f / 100)
    fee_am.to_f - (fee_am.to_f * commision)
  end

  def fee_referral_publisher?(commision)
    commision = commision.to_f / 100
    (self.payout_price * (self.fee_am.to_f / 100)) * commision
  end

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
