class Gallery
  include Mongoid::Document
  field :title, type: String
  field :description, type: String
  field :position, type: Integer
  # field :galleriable_id, type: String
  # field :galleriable_type, type: String
  field :image, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  mount_uploader :image, PhotoUploader

  private
    def before_create_service
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
    end

    def before_update_service
      self.updated_at = Time.zone.now
    end
end
