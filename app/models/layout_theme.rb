class LayoutTheme
  include Mongoid::Document
  field :code, type: String
  field :slug, type: String
  field :category_id, type: String
  field :image, type: String
  field :title, type: String
  field :link_preview, type: String
  field :status, type: String, default: "inactive"
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  index({ code: 1, slug: 1,category_id: 1,status: 1})

  before_create :before_create_service
  before_save :before_update_service

  scope :latest, -> {order(created_at: :desc)}
  scope :oldest, -> {order(created_at: :asc)}
  scope :active, ->{where(status: LayoutTheme::ACTIVE)}

  mount_uploader :image, PhotoUploader

  include TheLayoutThemes::LayoutThemesSearching

  has_many :request_landing_pages

  # -------- STATUS -----------
  ACTIVE = 'active'.freeze
  INACTIVE = 'inactive'.freeze
  PROMOTE = 'promote'.freeze

  STATUSES = [self::ACTIVE, self::INACTIVE].freeze

  def category
    category = CategoryCampaign::CATEGORIES.select{|c| c[:id] == self.category_id.to_i}[0]
  end

  def category_name
    category = CategoryCampaign::CATEGORIES.select{|c| c[:id] == self.category_id.to_i}[0][:name]
  end

  def status?
    if self.status == "active"
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Inactive</label>".html_safe
    end
  end

  def total_campaigns
    campaigns = RequestLandingPage.where(layout_theme_id: self.id).pluck(:campaign_id).uniq
  end

  def campaigns
    request_landing_pages = RequestLandingPage.where(layout_theme_id: self.id).pluck(:campaign_id).uniq
    campaigns = Campaign.where(:id.in => request_landing_pages)
  end

  def blast_email(subject, start_email_content, end_email_content)
    @publishers = Publisher.verified.no_suspended
    @publishers.pluck(:email).each_slice(500).to_a.map{|p| 
      publishers = p.join(", ")
      UserMailer.send_email_blasts_landing_page(publishers, self, subject, start_email_content, end_email_content).deliver_now!
    }
  end

  private
    def before_create_service
      code = 'LT'+ 5.times.map{Random.rand(10)}.join
      self.code = code
      self.created_at = Time.zone.now
      self.updated_at = Time.zone.now
      self.slug = self.title.parameterize
    end

    def before_update_service
      self.updated_at = Time.zone.now
      self.slug = self.title.parameterize
    end
  
end