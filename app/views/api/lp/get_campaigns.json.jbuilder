json.array! @landing_pages do |lp|
  json.id lp.code
  json.domain lp.domain
  json.track_script lp.track_script
  json.link_campaign lp.link_campaign
end