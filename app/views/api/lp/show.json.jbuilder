json.lp do
  json.cache! @lp, expires_in: 1.days do
    json.id @lp['id']
    json.domain @lp['domain']
    json.track_script @lp['track_script']
    json.link_campaign @lp['link_campaign']
  end
end