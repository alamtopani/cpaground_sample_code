class CardUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  if Rails.env.production? || Rails.env.staging?
    storage :aws
  else
    storage :file
  end

  def store_dir
    "uploads/user/card/#{mounted_as}/#{model.id}"
  end

  def default_url(*args)
    ActionController::Base.helpers.asset_path("ktp.jpg")
  end

  version :normal do
    process :resize_to_fit => [500, 500]
  end

  version :small do
    process :resize_to_fit => [300, 300]
  end

  version :thumb do
    process :resize_to_fit => [170, 170]
  end

  def extension_white_list
    %w(jpg jpeg png)
  end
end
