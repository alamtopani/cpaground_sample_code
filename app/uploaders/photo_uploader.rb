class PhotoUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  if Rails.env.production? || Rails.env.staging?
    storage :aws
  else
    storage :file
  end

  def store_dir
    "uploads/photo/#{mounted_as}/#{model.id}"
  end

  def default_url(*args)
    ActionController::Base.helpers.asset_path("icon-camera.png")
  end

  version :big do
    process :resize_to_fit => [1200, 1200]
  end

  version :normal do
    process :resize_to_fit => [500, 500]
  end

  version :small do
    process :resize_to_fit => [300, 300]
  end

  version :thumb do
    process :resize_to_fit => [170, 170]
  end

  def extension_white_list
    %w(jpg jpeg png gif)
  end
end
