class UserMailer < ApplicationMailer
  include ActionView::Helpers::NumberHelper
  include ApplicationHelper
  include DateHelper
  helper :application
  helper :date

  default from: 'noreply@cpaground.com'
  default to: 'noreply@cpaground.com'

  def after_create_campaign(campaign)
    @campaign = campaign
    subject = "Information Request Campaign (#{@campaign.title})"
    mail(to: @campaign.publisher.email, subject: subject, content_type: "text/html")
  end

  def after_create_campaign_admin(campaign)
    @campaign = campaign
    subject = "Request Campaign (#{@campaign.title})"
    mail(from: @campaign.publisher.email, subject: subject, content_type: "text/html")
  end

  def after_update_campaign(campaign)
    @campaign = campaign
    subject = "Information Activated Campaign (#{@campaign.title})"
    mail(to: @campaign.publisher.email, subject: subject, content_type: "text/html")
  end

  def after_payment_publisher_paid(payment, payment_type)
    @payment = payment
    @payment_type = payment_type
    subject = "Information #{@payment_type} (#{date_sort_time(@payment.payment_at)} - #{date_sort_time(@payment.payment_end)})"
    mail(to: @payment.user.email, subject: subject, content_type: "text/html")
  end

  def publisher_approved(publisher)
    @publisher = publisher
    subject = "Information Approved Account"
    mail(to: @publisher.email, subject: subject, content_type: "text/html")
  end

  def publisher_not_approved(publisher)
    @publisher = publisher
    subject = "Information Not Approved Account"
    mail(to: @publisher.email, subject: subject, content_type: "text/html")
  end

  def after_request_lp(request_landing_page)
    @request_landing_page = request_landing_page
    subject = "Information Request Landing Page"
    mail(to: @request_landing_page.publisher.email, subject: subject, content_type: "text/html")
  end

  def after_request_lp_admin(request_landing_page)
    @request_landing_page = request_landing_page
    subject = "Information Request Landing Page"
    mail(to: @request_landing_page.publisher.admin.email, subject: subject, content_type: "text/html")
  end

  def after_review_request_lp(request_landing_page)
    @request_landing_page = request_landing_page
    subject = "Information Review Request Landing Page"
    mail(to: @request_landing_page.publisher.email, subject: subject, content_type: "text/html")
  end

  def after_subscribe(subscriber)
    @subscriber = subscriber
    subject = "Welcome Subscriber"
    mail(to: @subscriber.email, subject: subject, content_type: "text/html")
  end

  def after_unsubscribe(subscriber)
    @subscriber = subscriber
    subject = "Information Unsubscribe"
    mail(to: @subscriber.email, subject: subject, content_type: "text/html")
  end

  def after_send_contact(contact)
    @contact = contact
    subject = "New Contact Message"
    mail(from: @contact.email, subject: subject, content_type: "text/html")
  end

  def publisher_suspended(publisher)
    @publisher = publisher
    subject = "Account Suspended"
    mail(to: @publisher.email, subject: subject, content_type: "text/html")
  end

  def publisher_activated(publisher)
    @publisher = publisher
    subject = "Account Activated"
    mail(to: @publisher.email, subject: subject, content_type: "text/html")
  end

  # Access Offer
  def apply_access_offer(access_offer)
    @access_offer = access_offer
    subject = "#{@access_offer.publisher.username} applied offer '#{@access_offer.offer.name}'"
    mail(to: @access_offer.account_manager.email, subject: subject, content_type: "text/html")
  end

  def approve_access_offer(access_offer)
    @access_offer = access_offer
    subject = "Your applied offer '#{@access_offer.offer.name}' has been approved"
    mail(to: @access_offer.publisher.email, subject: subject, content_type: "text/html")
  end

  def banned_access_offer(access_offer)
    @access_offer = access_offer
    subject = "Your applied offer '#{@access_offer.offer.name}' has been banned"
    mail(to: @access_offer.publisher.email, subject: subject, content_type: "text/html")
  end

  def replacing_manager(publisher, previous_manager)
    @publisher = publisher
    @previous_manager = previous_manager
    subject = "Your manager has been replaced" 
    mail(to: @publisher.email, subject: subject, content_type: "text/html")
  end

  def send_email_blasts_offer(publishers, offer, subject, start_email_content, end_email_content)
    @publishers = publishers
    @offer = offer
    @subject = subject
    @start_email_content = start_email_content
    @end_email_content = end_email_content
    mail(to: 'hi@cpaground.com', bcc: @publishers, subject: @subject, content_type: "text/html")
  end

  def send_email_blast_information(publishers, information, user)
    @user = user
    @publishers = publishers
    @information = information
    mail(to: 'hi@cpaground.com', bcc: @publishers, subject: @information.title, content_type: "text/html")
  end

  def send_email_blasts_landing_page(publishers, layout_theme, subject, start_email_content, end_email_content)
    @publishers = publishers
    @layout_theme = layout_theme
    @subject = subject
    @start_email_content = start_email_content
    @end_email_content = end_email_content
    mail(to: 'hi@cpaground.com', bcc: @publishers, subject: @subject, content_type: "text/html")
  end

  def test_mailer
    subject = "test mailer cron job"
    mail(to: "rayasa_01@yahoo.co.id", subject: subject, content_type: "text/html")
  end
end
