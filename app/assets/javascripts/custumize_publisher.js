function region(){
  $('.provinces_select').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/cities?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
        $('.city-container').html(result);
        $('.city-container .select2').select2();
      })
    }
  });
}

function selectCampaignLp(){
  $('.js-select_category_id').on('change', function(){
    if($(this).val() != '' && $(this).val() != 'undefined'){
      $.get('/xhrs/get_campaign?id='+$(this).val(), function(result){
        $('.js-content-lp').html(result);
        $('.select2').select2();
      })
    }
  });
}

function previewFile() {
  var preview = document.querySelector('.change-profile');
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = "";
  }
}

function alertShow(){
  $('.alert .close').click(function(){
    $('.section-alert').hide();
  });
  $('.section-alert').click(function(){
    $('.section-alert').hide();
  });
}

function copyClipboard(){
  $('.clipboard-btn').click(function(){
    var clipboard = new Clipboard($(this)[0]);
  });
}

function charFromBottom(){
  var charFromBottom = $('.chat-discussion');

  if(charFromBottom.length){
    charFromBottom[0].scrollTop = charFromBottom[0].scrollHeight;
  }
}

function feedMoreShow(){
  $(document).on('click', '.js-show-feed', function(){
    $(this).parents('.list-forum').find('.js-sort-feed').hide();
    $(this).parents('.list-forum').find('.js-completed-feed').show();
  });
  // $(document).on('click', '.js-hide-feed', function(){
  //   $(this).parents('.list-forum').find('.js-completed-feed').hide();
  //   $(this).parents('.list-forum').find('.js-sort-feed').show();
  // });
}



loadMoreFeed = function(){
  if ($('.forum-publisher .pagination')) {
    $(window).scroll(function() {
      url = $('.forum-publisher .pagination a').attr('href');
      if (url && $(window).scrollTop() > $(document).height() - $(window).height() - 150) {
        $('.forum-publisher .pagination').html("<p>LOADING MORE FEEDS ...</p><br><br>");
        $.getScript(url)
      }
    });
  }
}

loadMoreBlog = function(){
  if ($('.blog-publisher .pagination')) {
    $(window).scroll(function() {
      url = $('.blog-publisher .pagination a').attr('href');
      if (url && $(window).scrollTop() > $(document).height() - $(window).height() - 150) {
        $('.blog-publisher .pagination').html("<p>LOADING MORE ARTICLES ...</p><br><br>");
        $.getScript(url)
      }
    });
  }
}

loadMoreScroll = function(){
  if ($('.load-more-scroll .pagination')) {
    $(window).scroll(function() {
      url = $('.load-more-scroll .pagination a').attr('href');
      if (url && $(window).scrollTop() > $(document).height() - $(window).height() - 150) {
        $('.load-more-scroll .pagination').html("<p>LOADING MORE ...</p><br><br>");
        $.getScript(url)
      }
    });
  }
}


function submitCommentFeed(){
  $(document).on('submit', '.new-comment-feed', function(e) {
    $this_box = $(this);
    $.ajax({
      url: $(this).attr('data-url'),
      data: $(this).serialize(),
      type: "POST",
      dataType: "json",
      success: function(json, status, xhr) {
        html = '<div class="media responsive-media-comments"> <a class="pull-left"> <img src='+ json.user_avatar +'></a> <div class="media-body"> <small><b>'+ json.user_username + '</b> - ' + json.user_type +'</small> <small class="text-muted"> <i class="pe-7s-timer"></i> just now </small> <div class="social-content"><small>'+ json.data.message +'</small></div> </div> </div>';
        media = $this_box.parents('.forum-comments').find('.media-all');
        media.append(html);
        update_num = $this_box.parents('.list-forum').find('.js-num');
        total_num = parseInt(update_num.html()) + 1;
        update_num.html( " " + total_num );
        $('.comment-input').val('');
      },
      error: function(json, status, xhr) {
        alert('Error, to comment feed!');
      }
    });
    e.preventDefault();
  });
}

function submitLikeFeed(){
  $(document).on('submit', '.like-feed', function(e) {
    $this_box = $(this);
    $.ajax({
      url: $(this).attr('data-url'),
      data: $(this).serialize(),
      type: "POST",
      dataType: "json",
      success: function(json, status, xhr) {
        update_num = $this_box.parents('.list-forum').find('.js-num-like');
        total_num = parseInt(update_num.html()) + 1;
        update_num.html( " " + total_num );
        html = '<div class="like-feed"> <i class="fa fa-thumbs-up liked"></i> </div>';
        media = $this_box.parents('.feed-content').find('.forum-like');
        media.find('.like-feed').remove();
        media.append(html);
      },
      error: function(json, status, xhr) {
        alert('Error, to like feed!');
      }
    });
    e.preventDefault();
  });
}

function submitCommentBlog(){
  $(document).on('submit', '.new-comment-blog', function(e) {
    $this_box = $(this);
    $.ajax({
      url: $(this).attr('data-url'),
      data: $(this).serialize(),
      type: "POST",
      dataType: "json",
      success: function(json, status, xhr) {
        html = '<div class="social-talk"><div class="media social-profile clearfix"><div class="pull-left"><img src='+ json.user_avatar +'></div><div class="media-body"><b class="text-capital">'+ json.user_username + '</b> <small><i class="pe-7s-timer"></i> just now </small> <br><p> '+ json.data.message +' </p></div> </div> </div><hr>';
        media = $this_box.parents('.blog-comments').find('.new-comment');
        media.prepend(html);
        $('.comment-input').val('');
      },
      error: function(json, status, xhr) {
        alert('Error, to comment blog!');
      }
    });
    e.preventDefault();
  });
}

function updateNumber(this_box, parents, find, operations){
  update_num = this_box.parents(parents).find(find);
  if (operations == "plus") {
    total_num = parseInt(update_num.html()) + 1;
  } else {
    total_num = parseInt(update_num.html()) - 1;
  }
  update_num.html( total_num );
}

function submitStatusGroupCoverage(){
  $(document).on('submit', '.change-status-group-coverage', function(e) {
    $this_box = $(this);
    $status = $this_box.find('button.clicked').val();
    $.ajax({
      url: $(this).attr('data-url')+"?status="+$status,
      data: $(this).serialize(),
      type: "PUT",
      dataType: "json",
      success: function(json, status, xhr) {
        if($status == "approve"){
          updateNumber($this_box, 'body', '.total-members', 'plus');
          updateNumber($this_box, '.list-group-coverages', '.total-group-coverages-waiting', 'minus');
          updateNumber($this_box, '.list-group-coverages', '.total-group-coverages-approve', 'plus');

          html = '';
          btn = '<button name="status" type="sumbit" value="reject" class="btn btn-xs btn-default"><i class="fa fa-close"></i> Reject</button>';
        }else{
          updateNumber($this_box, 'body', '.total-members', 'minus');
          updateNumber($this_box, '.list-group-coverages', '.total-group-coverages-waiting', 'plus');
          updateNumber($this_box, '.list-group-coverages', '.total-group-coverages-approve', 'minus');

          html = '<div class="status"><label class="label label-danger">Reject</label></div>';
          btn = '<button name="status" type="sumbit" value="approve" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Approve</button>';
        }
        media = $this_box.parents('.list-group-coverages__info').find('.status-section');
        media.find('.status').remove();
        $this_box.find('.btn').remove();
        media.append(html);
        $this_box.append(btn);
      },
      error: function(json, status, xhr) {
        alert('Error, to change status member!');
      }
    });
    e.preventDefault();
  });
}

function submitStatusPublisher(){
  $(document).on('submit', '.change-status-publisher', function(e) {
    $this_box = $(this);
    $.ajax({
      url: $(this).attr('data-url'),
      data: $(this).serialize(),
      type: "PUT",
      dataType: "json",
      success: function(json, status, xhr) {
        updateNumber($this_box, '.list-group-coverages', '.total-publishers-waiting', 'minus');
        media = $this_box.parents('.list-group-coverages__info').find('.status-section');
        media.find('.status').remove();
        $this_box.find('.btn').remove();
      },
      error: function(json, status, xhr) {
        alert('Error, to change activation status publisher!');
      }
    });
    e.preventDefault();
  });
}

function submitSuspendedPublisher(){
  $(document).on('submit', '.suspended-publisher', function(e) {
    $this_box = $(this);
    $id = $(this).attr('data-url').split("/")[3];
    $status = $(this).serialize().split("suspended%5D=")[1];
    
    $.ajax({
      url: $(this).attr('data-url'),
      data: $(this).serialize(),
      type: "PUT",
      dataType: "json",
      success: function(json, status, xhr) {
        modal = $this_box.parents('.modal-forum');
        media = $this_box.parents('body').find('#'+$id);
        if ($status == "true"){
          html = '<div class="status"><label class="label label-danger label-user"><b>Suspend This Publisher</b></label></div>';
          btn = "<div class='button-action'><form class='suspended-publisher' data-url='/account_managers/publishers/"+$id+"/suspended' id='edit_publisher_"+$id+"' action='' accept-charset='UTF-8' method='post'><input value='false' type='hidden' name='publisher[suspended]' id='publisher_suspended'><button name='button' type='sumbit' class='btn btn-sm btn-default btn-block'><b><i class='fa fa-check'></i> Reactivate</b></button></form></div>";
        } else {
          html = '<div class="status"><label class="label label-success label-user">Reactivate</label></div>';
          btn = "<div class='button-action'><button class='btn btn-default btn-sm btn-block' data-target='#mySuspendedForm-"+$id+"' data-toggle='modal' type='button'><b><i class='pe-7s-attention'></i> Suspend This Publisher</b> </button></div>";
        }
        media.find('.button-action').remove();
        media.find('.form-action').append(btn);
        media.find('.status').remove();
        media.find('.status-box').append(html);
        modal.modal('hide');
      },
      error: function(json, status, xhr) {
        alert('Error, to change activation status publisher!');
      }
    });
    e.preventDefault();
  });
}

function submitApprovementCampaign(){
  $(document).on('submit', '.approvement-campaign', function(e) {
    $this_box = $(this);
    $id = $(this).attr('data-url').split("/")[3];
    $.ajax({
      url: $(this).attr('data-url'),
      data: $(this).serialize(),
      type: "PUT",
      dataType: "json",
      success: function(json, status, xhr) {
        media = $this_box.parents('.panel-footer');
        status = '<label class="label label-success">Active</label>';
        btn = "<a class='btn btn-primary detail-campaign pull-right desktop' href='/account_managers/campaigns/"+$id+"'>View More</a><a class='btn btn-primary btn-detail-campaign pull-right mobile' href='/account_managers/campaigns/"+$id+"'><i class='fa fa-eye'></i></a>";
        media.find('.label').remove();
        media.find('.approvement-campaign').remove();
        media.find('.status-campaign').append(status);
        media.find('.btn-approvement-campaign').append(btn);
      },
      error: function(json, status, xhr) {
        alert('Error, to approvement campaign!');
      }
    });
    e.preventDefault();
  });
}

function submitCheckedOffer(){
  $(document).on('submit', '.submit-checked-offer', function(e) {
    $this_box = $(this);
    $.ajax({
      url: $(this).attr('data-url'),
      data: $(this).serialize(),
      type: "PUT",
      dataType: "json",
      success: function(json, status, xhr) {
        media = $this_box.parents('.access-offer__info').find('.checked-section');
        media.remove();
      },
      error: function(json, status, xhr) {
        alert('Error, to checked access offer publisher!');
      }
    });
    e.preventDefault();
  });
}

function submitStatusOffer(){
  $(document).on('submit', '.submit-status-offer', function(e) {
    $this_box = $(this);
    $.ajax({
      url: $(this).attr('data-url'),
      data: $(this).serialize(),
      type: "PUT",
      dataType: "json",
      success: function(json, status, xhr) {
        media = $this_box.parents('.access-offer__info');
        media.remove();
      },
      error: function(json, status, xhr) {
        alert('Error, to change status access offer publisher!');
      }
    });
    e.preventDefault();
  });
}

function limitMoreComments(){
  $(document).on('click', '.js-more-comments', function(){
    $(this).parents('.forum-comments').find('.all-comments').show();
    $(this).parents('.forum-comments').find('.limit-comments').hide();
  });
}

function choiceCheckedLp(){
  $(document).on('click', '.template-lp', function(){
    $('.template-lp').removeClass('checked');
    $('.template-lp').addClass('opacity');
    if($(this).find('input').prop("checked", true)){
      $(this).addClass('checked');
      $(this).removeClass('opacity');
    }
  });
}
function feedImage() {  
  var toggle = false;
  $(document).on('click', '.js-image-feed', function(){
    var containerElement = $(this).parents('.list-forum').find(".cover-photo-feed");
    containerElement.addClass('skinny');
  });
}

$(document).ready(function() {
  feedImage();
  alertShow();
  region();
  copyClipboard();
  charFromBottom();
  feedMoreShow();
  loadMoreFeed();
  loadMoreBlog();
  loadMoreScroll();
  submitCommentFeed();
  submitLikeFeed();
  submitCommentBlog();
  limitMoreComments();
  selectCampaignLp();
  choiceCheckedLp();
  submitStatusGroupCoverage();
  submitStatusPublisher();
  submitSuspendedPublisher();
  submitApprovementCampaign();
  submitCheckedOffer();
  submitStatusOffer();
  $('.responsive-tabel').doubleScroll();
  $('.select2').select2();
  $('.real-time').tooltip();
  $("#to").datepicker({ format: 'yyyy-mm-dd' });
  $("#from").datepicker({ format: 'yyyy-mm-dd' }).bind("change",function(){
      var minValue = $(this).val();
      minValue = $.datepicker.parseDate("yyyy-mm-dd", minValue);
      minValue.setDate(minValue.getDate()+1);
      $("#to").datepicker( "option", "minDate", minValue );
  });
  $('.datepicker').datepicker({
    format: "yyyy-mm-dd"
  });
  var date = new Date(), y = date.getFullYear(), m = date.getMonth() - 1;
  var firstDay = new Date(y, m, 1);
  $('.datepicker-limited').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: firstDay,
    endDate: '+0d'
  });

  var sidebarTop = $('.js-sticky-sidebar');
  if(sidebarTop.length > 0){
    sidebarTop = sidebarTop.offset().top;
  }
  
  $(window).scroll(function(){
    if ($(this).scrollTop() > sidebarTop) {
      $('.js-sticky-sidebar').addClass('fixed');
    }else{
      $('.js-sticky-sidebar').removeClass('fixed');
    }
  });

  $(".to_tags").select2({ multiple: true });
  $('[data-toggle="tooltip"]').tooltip(); 

  $(document).on('click', '.lightboxgallery-gallery-item', function(event) {
    event.preventDefault();
    $(this).lightboxgallery({
      showCounter: true,
      showTitle: true,
      showDescription: true
    });
  });
  
  // $(".to_tags").select2({
  //   multiple: true,
  //   data: window.tags,
  //   createSearchChoice(term, data) {
  //     if ($(data).filter(function() {
  //       return this.text.localeCompare(term) === 0;
  //     }).length === 0) {
  //       return {
  //         id: `<<<${term}>>>`,
  //         text: term
  //       };
  //     }
  //   }
  // });
  
});

