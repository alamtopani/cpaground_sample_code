function alertShow(){
  $('.alert .close').click(function(){
    $('.section-alert').hide();
  });
  $('.section-alert').click(function(){
    $('.section-alert').hide();
  });
}

function scrollToFix(){
  if ($(window).scrollTop() >= 50) { 
    $('.navbar-transparant').removeClass('navbar-transparant');
  }else { 
    $('.js-navbar-base').addClass('navbar-transparant');
  } 
}

function resizeLineImage(){
  var cw = $('.line-image').width();
  $('.line-image').css({
      'height': cw + 'px'
  });
  var cw = $('.line-image-jpg').width();
  $('.line-image-jpg').css({
      'height': cw + 'px'
  });
}


$(window).scroll(function () { 
  scrollToFix();
});

$( window ).resize(function() {
  resizeLineImage();
});

$(document).ready(function(){
  scrollToFix();
  alertShow();
  resizeLineImage();
  $('.select2').select2();
});
