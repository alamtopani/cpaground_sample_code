// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require plugins/jquery_ui
//= require plugins/doubleScroll
//= require plugins/bootstrap.min
//= require plugins/homer
//= require plugins/metisMenu.min
//= require plugins/icheck.min
//= require plugins/jquery.peity.min
//= require plugins/sparkline
//= require plugins/clipboard.min
//= require select2
//= require plugins/bootstrap-datepicker.min
//= require plugins/highcharts
// require chartist/chartist
// require chartist/chartist-plugin-legend
//= require chartist/chartist-plugin-tooltip
//= require plugins/input-image
//= require plugins/input-image2
//= require plugins/jquery.blueimp-gallery.min
//= require plugins/lightboxgallery.min
//= require custumize_publisher
//= require chartkick