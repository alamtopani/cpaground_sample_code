// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require plugins/jquery_ui
//= require plugins/doubleScroll
//= require bootstrap
//= require bootstrap-datepicker
//= require plugins/moment
//= require plugins/bootstrap-datetimepicker.min
//= require app
//= require cocoon
//= require select2
//= require plugins/jquery.slimscroll.min
// require chartist/chartist
// require chartist/chartist-plugin-legend
//= require chartist/chartist-plugin-tooltip
//= require plugins/clipboard.min
//= require ckeditor/init
//= require ckeditor/config
//= require chartkick


function region(){
	$('.provinces_select').on('change', function(){
		if($(this).val() != '' && $(this).val() != 'undefined'){
			$.get('/xhrs/cities?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
				$('.city-container').html(result);
				// $('.city-container .select2').select2();
			})
		}
	});
}

function get_balances(){
	$('.publisher_select').on('change', function(){
		if($(this).val() != '' && $(this).val() != 'undefined'){
			$.get('/xhrs/get_balances?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
				$('.balance-container').html(result);
			})
		}
	});
}

function previewFile(input, imageHeader) {
	var preview = imageHeader[0];
	var file    = input.files[0];
	var reader  = new FileReader();

	reader.onloadend = function () {
		preview.src = reader.result;
	}

	if (file) {
		reader.readAsDataURL(file);
	} else {
		preview.src = "";
	}
}

function copyClipboard(){
	var clipboard = new Clipboard('.clipboard-btn');
}

function boxReportInList(){
	$(document).on('click', '.js-to-box-add-report.disabled', function() {
		var box_value = $(this).attr('value');
		$(this).removeClass('disabled');
		$(this).addClass('active');

		$.get('/xhrs/get_box_report?id='+box_value, function(result){
			$('.'+box_value).html(result);
			$('.'+box_value).show();
			$('.datepicker').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true
			});

			$('.datetimepicker1').datetimepicker({
			  defaultDate: new Date(),
		    format:'DD/MM/YYYY HH:mm'
			});

			$('.select2').select2();

			get_campaign_date_form();
		})
	});

	$(document).on('click', '.js-to-box-add-report.active', function() {
		var box_value = $(this).attr('value');
		$(this).removeClass('active');
		$(this).addClass('disabled');
		$('.'+box_value).hide();
	});
	
	$(document).on('submit', '.new-campaign-report-on-list', function(e) {
		var this_box = $(this)
		var box_value = this_box.parents('.js-box-add-report').attr('class').split(' ')[0];

		$.ajax({
				url: $(this).attr('data-url'),
				data: $(this).serialize(),
				type: "POST",
				dataType: "json",
				success: function(json, status, xhr) {
					alert(json.message);
					this_box.parents('.js-box-add-report').hide();
					$(".js-to-box-add-report[value='" + box_value +"']").removeClass('active');
					$(".js-to-box-add-report[value='" + box_value +"']").addClass('disabled');
				},
				error: function(json, status, xhr) {
					alert('Error, click the id that you input not from the conversion report of this campaign!');
					this_box.find('.btn-submit').prop('disabled', false);
					this_box.find('.btn-submit').val('Add');
				}
		});
		e.preventDefault();
	});
}

function get_campaign_date_form(){
  // $(document).on('change', '.js-campaign-date-select', function() {
  //  var jsBoxAddReport = $(this).parents('.js-box-add-report').find('.js-box-campaign-form');
  //  if($(this).val() != '' && $(this).val() != 'undefined'){
  //    $.get('/xhrs/get_campaign_date_form?id='+$(this).val()+'&node='+$(this).data("node"), function(result){
  //      jsBoxAddReport.html(result);
  //    })
  //  }
  // });

  $(document).on('click', '.js-click-convertion', function() {
    var jsBoxAddReport = $(this).parents('.js-box-add-report').find('.js-box-campaign-form');
    var click_id = $(this).parents('.js-box-add-report').find('.js-click-id-convertion-value').val();
    if(click_id != '' && click_id != 'undefined'){
      $.get('/xhrs/get_campaign_date_form?id='+click_id, function(result){
        jsBoxAddReport.html(result);
      })
    }
  });
}

function get_convertion_report(){
  $(document).on('click', '.js-click-convertion-report', function() {
    var jsBoxAddReport = $(this).parents('.js-convertion-box-add-report').find('.js-box-campaign-form');
    var click_id = $(this).parents('.js-convertion-box-add-report').find('.js-click-id-convertion-value').val();
    if(click_id != '' && click_id != 'undefined'){
      $.get('/xhrs/get_convertion_report?id='+click_id, function(result){
        jsBoxAddReport.html(result);
      })
    }
  });
}

function change_convertion_report(){
	$(document).on('click', '.js-click-convertion-report', function() {
		var jsBoxAddReport = $(this).parents('.js-convertion-box-add-report').find('.js-box-campaign-form-change');
		var click_id = $(this).parents('.js-convertion-box-add-report').find('.js-click-id-convertion-value').val();
		if(click_id != '' && click_id != 'undefined'){
			$.get('/xhrs/change_convertion_report?id='+click_id, function(result){
				jsBoxAddReport.html(result);
			})
		}
	});
}

function save_convertion_report() {
  $(document).on('submit', '.new-convertion-report-on-list', function(e) {
    $.ajax({
        url: $(this).attr('data-url'),
        data: $(this).serialize(),
        type: "POST",
        dataType: "json",
        success: function(json, status, xhr) {
          $('.new-convertion-report-on-list .form-control').val('');
          alert(json.message);
        },
        error: function(json, status, xhr) {
          alert('Error, country code for this data not present! please input the country code to the corresponding offer.');
        }
    });
    e.preventDefault();
  });
}

function submitCommentFeed(){
	$(document).on('submit', '.new-comment-feed', function(e) {
		$this_box = $(this);
		$.ajax({
			url: $(this).attr('data-url'),
			data: $(this).serialize(),
			type: "POST",
			dataType: "json",
			success: function(json, status, xhr) {
				html = '<div class="social-talk"><div class="media social-profile"> <a class="pull-left"> <img src='+ json.user_avatar +'></a> <div class="media-body"> <span class="font-bold">'+ json.user_username +'</span> <small class="text-muted"> <i class="pe-7s-timer"></i> just now </small> <div class="social-content">'+ json.data.message +'</div> </div> </div></div>';
				media = $this_box.parents('.forum-comments').find('.media-all');
				media.append(html);
				$('.comment-input').val('');
			},
			error: function(json, status, xhr) {
				alert('Error, to comment feed!');
			}
		});
		e.preventDefault();
	});
}

function feedMoreShow(){
	$(document).on('click', '.js-show-feed', function(){
		$(this).parents('.list-forum').find('.js-sort-feed').hide();
		$(this).parents('.list-forum').find('.js-completed-feed').show();
	});
	// $(document).on('click', '.js-hide-feed', function(){
	//   $(this).parents('.list-forum').find('.js-completed-feed').hide();
	//   $(this).parents('.list-forum').find('.js-sort-feed').show();
	// });
}

function getOfferCr(){
	$(document).on('click', '.btn-list-offer', function(){
		$('#list-offer-count').toggle();
		loadingData();
		if($('#list-offer-count').find('table').length == 0){
			$.get('/xhrs/get_offer_cr', function(result){
				$('#list-offer-count').html(result);
				$(".loading-data").fadeOut(1000);
			});
		}
	});
}

function loadingData(){
	var originalTextLoad = $(".loading-data .text").text(),
	i  = 0;
	setInterval(function() {
		$(".loading-data .text").append(" .");
		i++;
		if(i == 4)
		{
			$(".loading-data .text").html(originalTextLoad);
			i = 0;
		}
	}, 500);
}



function limitMoreComments(){
	$(document).on('click', '.js-more-comments', function(){
		$(this).parents('.forum-comments').find('.all-comments').show();
		$(this).parents('.forum-comments').find('.limit-comments').hide();
	});
}

loadMoreFeed = function(){
	if ($('.forum-publisher .pagination')) {
		$(window).scroll(function() {
			url = $('.forum-publisher .pagination a').attr('href');
			if (url && $(window).scrollTop() > $(document).height() - $(window).height() - 150) {
				$('.forum-publisher .pagination').html("<p>LOADING MORE FEEDS ...</p><br><br>");
				$.getScript(url)
			}
		});
	}
}

function get_payment_owners(){
	$(document).on('change', '.invoice_owners_payment', function() {
		$owner_id = document.getElementById("invoice_owner_owner_id").value;
		$payment_at = document.getElementById("invoice_owner_payment_at").value;
		$node = $owner_id+"+"+$payment_at;
		if($(this).val() != '' && $(this).val() != 'undefined'){
			$.get('/xhrs/get_payment_owners?id='+$(this).val()+'&node='+$node, function(result){
				$('.price-container').html(result);
			})
		}
	});
}


$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip(); 
  $('.responsive-tabel').doubleScroll();
	$('.select2').select2();
	$(".js-select2-tag").select2({
		tags: true,
		tokenSeparators: [',', ' ']
	});
	
	$(".photo-profile").find("input[type='file']").on('change', function(index){
		if($(this).length){
			previewFile(this, $('.photo-profile img'));
		}
	});

  $('#check_all').click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
  });

  $('#check_all1').click(function(){
    $('.tab1 input:checkbox').not(this).prop('checked', this.checked);
  });

	$('#check_all2').click(function(){
		$('.tab2 input:checkbox').not(this).prop('checked', this.checked);
	});

	region();
	get_balances();
	$('.datepicker').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true
	});

	$('.datetimepicker1').datetimepicker({
	  defaultDate: new Date(),
    format:'DD/MM/YYYY HH:mm'
	});


	boxReportInList();
	copyClipboard(); 
	submitCommentFeed();
	feedMoreShow();
	getOfferCr();
	limitMoreComments();
	loadMoreFeed();
	get_payment_owners();
  get_convertion_report();
  save_convertion_report();
  change_convertion_report();

  $(".to_tags").select2({ multiple: true });
  $('[data-toggle="tooltip"]').tooltip(); 
});